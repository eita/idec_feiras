<?php 

function idec_biblioteca_get_items() {
    global $biblioteca_items;

    if(get_object_vars($biblioteca_items)) {
        return $biblioteca_items;
    }

    $url = BIBLIOTECA_API_URL."items";
    $params = array(
        "public=1", 
        "tags=mapadefeiras"
    );

    $url .= "?".implode('&',$params);
    $json = file_get_contents($url);
    $data = json_decode($json);

    $itemsAr = array(
        "count"=>count($data),
        "data"=>array()
    );
    
    for($i=0;$i<count(($data));$i++) {
        $itemsAr['data'][] = idec_biblioteca_process_item_elements($data[$i], false);
    }
    $biblioteca_items = (object) $itemsAr;
    
    return $biblioteca_items;
}

function idec_biblioteca_get_filtered_items($search="", $item_type="", $collection="", $paged="") {
    
    $limit = get_option('posts_per_page');
    
    $items = clone idec_biblioteca_get_items();

    $data = $items->data;
    $items->data = array();
    
    if($search || $item_type || $collection) {
        // TODO: add $search. It's not being used yet, since searches are done in pure javascript.
        
        $dataRaw = $data;
        $data = array();
        foreach ($dataRaw as $item) {
            if (
                (!$item_type || (isset($item->item_type) && $item_type==$item->item_type))
                && (!$collection || (isset($item->collection) && $collection==$item->collection))
            ) {
                $data[] = $item;
            }
        }
    }

    // present more recent items first
    $data = array_reverse($data);

    $items->count = count($data);    
    $items->paged = $paged;
    $items->pages = ($limit) ? ceil($items->count/$limit) : 1;
    
    $ii = ($limit)
        ? ($paged-1)*$limit
        : 0;
    $limit = ($limit && ($limit*$paged)<$items->count)
        ? $limit*$paged
        : $items->count;

    for($i=$ii;$i<$limit;$i++) {
        $items->data[] = $data[$i];
    }

    return $items;
    
}

function idec_biblioteca_get_page_from_url() {
    $paged = explode('/',$_SERVER['REQUEST_URI']);
    return (is_numeric($paged[count($paged)-2])) ? $paged[count($paged)-2] : 1;
}

function idec_biblioteca_get_single_item($itemId=false) {
    global $biblioteca_item;
    if(!$itemId && isset($_REQUEST['id'])) {
        $biblioteca_item = idec_biblioteca_get_single_item($_REQUEST['id']);
        return $biblioteca_item;
    }
    if(!$itemId || $itemId == $biblioteca_item->id) {
        return (!$biblioteca_item->id) ? false : $biblioteca_item;
    }
    
    $url = BIBLIOTECA_API_URL."items/$itemId";
    $params = array();
    $json = file_get_contents($url);
    $item_elements = json_decode($json);
    if(!$item_elements || !get_object_vars($item_elements)) {
        $biblioteca_item = (object) array();
    } else {
        $biblioteca_item = idec_biblioteca_process_item_elements($item_elements, true); //TODO: Solve how to only do this once!
    }
    
    return $biblioteca_item;
}

function idec_biblioteca_get_single_collection($id=false) {
    global $biblioteca_collection;
    
    if(!$id && $biblioteca_collection->id) {
        return $biblioteca_collection;
    }
    
    if(!$id) {
        $id = $_REQUEST['collection'];
    }

    $collections = idec_biblioteca_get_collections();

    if (isset($collections[$id])) {
        $biblioteca_collection = $collections[$id];
    } else {
        $biblioteca_collection = false;
    }
        
    return $biblioteca_collection;
}

function idec_biblioteca_get_single_item_type($id=false) {
    global $biblioteca_item_type;

    if(!$id && $biblioteca_item_type->id) {
        return $biblioteca_item_type;
    }
    
    if(!$id) {
        $id = $_REQUEST['item_type'];
    }
    
    $item_types = idec_biblioteca_get_item_types();
        
    if (isset($item_types[$id])) {
        $biblioteca_item_type = $item_types[$id];
    } else {
        $biblioteca_item_type = false;
    }

    return $biblioteca_item_type;
}

function idec_biblioteca_get_single_mixed_archive($id=false) {
    global $biblioteca_mixed_archive;
    
    if ((!$id && $biblioteca_mixed_archive->id) || ($id && $biblioteca_mixed_archive->id==$id && $biblioteca_mixed_archive->paged==$paged)) {
        return $biblioteca_mixed_archive;
    }
    
    if (!$id) {
        $id = $_REQUEST['mixed_archive'];
    }
    list($collection_id, $item_type_id) = idec_split_mixed_archive_id($id);

    if(!$collection_id || !$item_type_id) {
        $biblioteca_mixed_archive = (object) array();
    } else {
        $collection = idec_biblioteca_get_single_collection($collection_id);
        $item_type = idec_biblioteca_get_single_item_type($item_type_id);

        $biblioteca_mixed_archive = (object) array();
        $biblioteca_mixed_archive->id = $id;
        $biblioteca_mixed_archive->count = 0;
        $biblioteca_mixed_archive->local_url = idec_get_page_type_homeurl()."?mixed_archive=".$id;
        $biblioteca_mixed_archive->title = 'Conteúdos do tipo "'.$item_type->title.'" no tema "'.$collection->title.'"';
        $biblioteca_mixed_archive->description = '';
        if($item_type->description) {
            $biblioteca_mixed_archive->description .= 'Tipo dos conteúdos: '.$item_type->description.'. ';
        }
        if($collection->description) {
            $biblioteca_mixed_archive->description .= 'Sobre o tema: '.$collection->description;
        }
        $items = idec_biblioteca_get_items();
        foreach($items->data as $item) {
            if ($item->collection==$collection->id && $item->item_type==$item_type->id) {
                $biblioteca_mixed_archive->count ++;
            }
        }
    }
    
    return $biblioteca_mixed_archive;
}

function idec_biblioteca_get_single_category($category, $id=false) {
    switch ($category) {
        case 'collection':
            return idec_biblioteca_get_single_collection($id);
        case 'item_type':
            return idec_biblioteca_get_single_item_type($id);
        case 'mixed_archive':
            return idec_biblioteca_get_single_mixed_archive($id);
    }
}

function idec_get_mixed_archive_id($collId, $itId) {
    return $collId . "," . $itId;
}
function idec_split_mixed_archive_id($maId) {
    return explode(",",$maId);
}

function idec_biblioteca_process_item_elements($item_elements, $get_files_urls = false) {
    $item = array();
    $item['id'] = $item_elements->id;
    $item['local_url'] = idec_get_page_type_homeurl()."?id=".$item_elements->id;
    $item['url'] = $item_elements->url;
    if(isset($item_elements->collection)) {
        $item['collection'] = $item_elements->collection->id;
    }
    foreach($item_elements->element_texts as $element) {
        // print_r($element)
        switch($element->element->name) {
            case 'Title':
                $item['title'] = $element->text;
                break;
            case 'Description':
                $item['description'] = $element->text;
                break;
            case 'Date':
                $item['date'] = $element->text;
                break;
            case 'Source':
                $item['source'] = $element->text;
                break;
            case 'Player':
                $item['video'] = $element->text;
                break;
            case 'URL':
                $item['link'] = strip_tags($element->text);
                break;
            case 'Creator':
                $item['creator'] = $element->text;
                break;
        }
    }
    $item_type = $item_elements->item_type->id;
    if ($item_type) {
        $item['item_type'] = $item_type;
    }
    if($get_files_urls) {
        $item['files'] = idec_biblioteca_get_file_from_url($item_elements->files->url);
        foreach($item['files'] as $file) {
            if ($file->file_urls->square_thumbnail) {
               $item['thumbnail'] = $file->file_urls->square_thumbnail;
               break;
            }
        }
    } else {
        $item['filesAPI'] = $item_elements->files->url;
    }
    return (object) $item;
}

function idec_biblioteca_get_collections($orderby = 'name') {
    global $biblioteca_collections;
    
    if($biblioteca_collections) {
        return $biblioteca_collections;
    }
    
    $url = BIBLIOTECA_API_URL."collections/?public=1";
    $json = file_get_contents($url);
    $data = json_decode($json);
    
    $collectionsRaw = array();
    
    foreach($data as $collection) {
        $t = array(
            'id'=>$collection->id,
            'url'=>$collection->url,
            'items_url'=>$collection->items->url,
            'local_url'=>idec_get_page_type_homeurl()."?collection=".$collection->id
        );
        foreach($collection->element_texts as $element) {
            switch($element->element->name) {
                case 'Title':
                    $t['title'] = $element->text;
                    break;
                case 'Description':
                    $t['description'] = $element->text;
                    break;
            }
        }
        $collectionsRaw[$collection->id] = (object) $t;
    }
    
    $items = idec_biblioteca_get_items();
    $collectionsUnordered = array();
    $orderReference = array();
    foreach($items->data as $item) {
        if ($item->collection && isset($collectionsRaw[$item->collection])) {
            if(!isset($collectionsUnordered[$item->collection])) {
                $collectionsUnordered[$item->collection] = $collectionsRaw[$item->collection];
            }
            $collectionsUnordered[$item->collection]->items_count++;
            if($orderby=='name') {
                $orderReference[$item->collection] = sanitize_title($collectionsUnordered[$item->collection]->title);
            } else {
                $orderReference[$item->collection] = $collectionsUnordered[$item->collection]->items_count;
            }
        }
    }
    if($orderby=='name') {
        asort($orderReference);
    } else {
        arsort($orderReference);
    }
    
    foreach($orderReference as $key=>$o) {
        $biblioteca_collections[$key] = $collectionsUnordered[$key];
    }

    return $biblioteca_collections;
}

function idec_biblioteca_get_item_types($orderby = 'name') {
    global $biblioteca_item_types;
    
    if($biblioteca_item_types) {
        return $biblioteca_item_types;
    }
    
    $url = BIBLIOTECA_API_URL."item_types";
    $json = file_get_contents($url);
    $data = json_decode($json);
    
    $item_typesRaw = array();
    
    foreach($data as $item_type) {
        $t = array(
            'id'=>$item_type->id,
            'url'=>$item_type->url,
            'title'=>$item_type->name,
            'description'=>$item_type->description,
            'items_count'=>0,
            'items_url'=>$item_type->items->url,
            'local_url'=>idec_get_page_type_homeurl()."?item_type=".$item_type->id
        );
        $item_typesRaw[$item_type->id] = (object) $t;
    }
    
    $items = idec_biblioteca_get_items();
    $item_typesUnordered = array();
    $orderReference = array();
    foreach($items->data as $item) {
        if ($item->item_type && isset($item_typesRaw[$item->item_type])) {
            if(!isset($item_typesUnordered[$item->item_type])) {
                $item_typesUnordered[$item->item_type] = $item_typesRaw[$item->item_type];
            }
            $item_typesUnordered[$item->item_type]->items_count++;
            if($orderby=='name') {
                $orderReference[$item->item_type] = sanitize_title($item_typesUnordered[$item->item_type]->title);
            } else {
                $orderReference[$item->item_type] = $item_typesUnordered[$item->item_type]->items_count;
            }
        }
    }
    if($orderby=='name') {
        asort($orderReference);
    } else {
        arsort($orderReference);
    }
    
    foreach($orderReference as $key=>$o) {
        $biblioteca_item_types[$key] = $item_typesUnordered[$key];
    }

    return $biblioteca_item_types;
}

function idec_biblioteca_show_item_categories($item, $type, $class="") {
    $term = ($type=='collection')
        ? idec_biblioteca_get_single_collection($item->collection)
        : idec_biblioteca_get_single_item_type($item->item_type);

    if (!$term) {
        return NULL;
    }

    $class = ($class) ? " class=\"$class\"" : "";
	$html = '
	    <div'.$class.'>
	        <a class="ajaxifythis" href="'.$term->local_url.'" data-item-type="'.$type.'" data-item-id="'.$term->id.'" data-item-is_overlay=0>
	            '.$term->title.'
	        </a>
        </div>
    ';
	return $html;
}

function idec_biblioteca_get_file_from_url($url="") {
    $json = file_get_contents($url);
    $data = json_decode($json);
    $files = array();
    foreach ($data as $file) {
        $t = array();
        if($file->file_urls) {
            $t['file_urls'] = $file->file_urls;
        }
        if($file->element_texts) {
            foreach ($file->element_texts as $element) {
                if($element->element_set->id==1 && $element->element->name='Type' && $element->text=='thumb') {
                    $t['thumb_only'] = true;
                    break;
                }
            }
        }
        $files[] = (object) $t;
    }
    return $files;
}

add_action('wp_ajax_nopriv_idec_biblioteca_ajax_get_item_thumbnail', 'idec_biblioteca_ajax_get_item_thumbnail' );
add_action('wp_ajax_idec_biblioteca_ajax_get_item_thumbnail', 'idec_biblioteca_ajax_get_item_thumbnail' );
function idec_biblioteca_ajax_get_item_thumbnail() {
    $id = $_POST['id'];
    if (!$id) {
        echo '{}';
        wp_die();
    }
    $thumbnail = '';
    $files = idec_biblioteca_get_file_from_url(BIBLIOTECA_API_URL."files?item=$id");
    foreach ($files as $file) {
        if($file->file_urls->square_thumbnail) {
            if (!$thumbnail || $file->thumb_only) {
                $thumbnail = json_encode(array('thumbnail'=>$file->file_urls->square_thumbnail, 'size800'=>$file->file_urls->fullsize));
            }
            if ($file->thumb_only) {
                break;
            }
        }
    }
    echo $thumbnail;
    wp_die();
}

function idec_get_collection_html($showCity=true, $id="{id}", $url="{url}", $type="{type}", $title="{title}", $city="{city}", $address="{address}") {
    $itemHTML = "
        <li class=\"with-separator\">
            <a class=\"ajaxifythis idec-list-item-content\" href=\"$local_url\" data-item-id=\"$id\" data-item-type=\"collection\" data-page_title=\"$page_title\" data-item-is_overlay=0>
                <div>
                    <div class=\"idec-list-item-icon\">$items_count</div>
                    <h1 class=\"idec-list-item-title\">$title</h1>
                </div>
            </a>
        </li>
    ";
    return str_replace(array("\n", "  "),"",$itemHTML);
}

?>
