    <?php idec_pageLocation('item', get_the_ID(), get_the_permalink(), idec_generate_page_headers('receita','item',get_the_ID())->title, '', '1', 'receita'); ?>

    <?php idec_receita_set_main_query_to_archive(); ?>
    
    <?php get_header(); ?>

    <?php get_template_part('archive', 'receita-content'); ?>

    <?php get_footer('receita'); ?>
