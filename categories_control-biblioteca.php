<?php $collections=idec_biblioteca_get_item_types('name'); ?>
<div id="categoriesControl-wrapper">
    <div class="categoriesControl-buttons">
        <a href="#" id="categoriesControl-menu" alt="Escolha o tipo de conteúdo" title="Escolha o tipo de conteúdo"><span class="mapafeiras_icon-filtro"></span></a>
    </div>
    <div id="filters">
      <form id="categoriesControl-form">
          <h1>Filtrar por tipo</h1>
          <ul class='ui-menu'>
              <?php foreach ($collections as $id=>$cat) { ?>
                  <li class='ui-menu-item'>
                      <a class="idec-list-item-content ajaxifythis" href="<?= $cat->local_url ?>" data-item-type='item_type' data-item-id='<?= $cat->id ?>' data-item-is_overlay=0>
                          <p class="idec-search-autocomplete-name">
                              <span class="m-label-name"><?= $cat->title ?></span>
                              <span class="m-label-count"><?= $cat->items_count ?></span>
                          </p>
                      </a>
                  </li>
              <?php } ?>
          </ul>
      </form>
    </div>
</div>
