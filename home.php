<?php $pL = idec_pageLocation(); ?>
<?php if (!$pL->type) { ?>
    <?php if (!COMIDADEVERDADE) { ?>
        <?php idec_pageLocation("global", "", get_bloginfo('url'), "", "", 0, 'map'); ?>
    <?php } else { ?>
        <?php $title = idec_generate_page_headers('page','page',COMIDADEVERDADE_SLUG)->title; ?>
        <?php idec_pageLocation("page", COMIDADEVERDADE_SLUG, get_bloginfo('url')."/".COMIDADEVERDADE_SLUG, $title, $title, "1", "page"); ?>
    <?php } ?>
<?php } ?>

<?php get_header(); ?>

    <div id="main_map" class="removeFilterOnClick"></div>

<?php get_footer('map'); ?>
