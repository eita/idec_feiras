        </div>
    </div>
    <?php get_template_part('rrssb', 'content'); ?>
    <div class="idec-nav-wrapper idec-nav-wrapper-sm" id="menu">        
        <div class="idec-toggle pure-show pure-md-hide">
            <div class="pure-menu">
                <a href="#" class="custom-toggle" id="toggle"><s class="bar"></s><s class="bar"></s></a>
            </div>
        </div>
        <div class="idec-nav-menus-wrapper idec-nav-menus-wrapper-md"> 
            <div class="pure-menu pure-menu-horizontal custom-can-transform idec-nav idec-nav-md pure-hide pure-md-show idec-nav-menu-inst idec-nav-menu-inst-md">
                <?php 
                    wp_nav_menu( array(
                        'theme_location' => 'idec_institutional_navigation',
                        'container' => '',
                        'menu_class'=> 'pure-menu-list'
                     ) );
                ?>
            </div>
            <div class="pure-menu pure-menu-horizontal custom-menu-3 custom-can-transform idec-nav idec-nav-md pure-hide pure-md-show idec-nav-menu-contact idec-nav-menu-contact-md">
                <ul class="pure-menu-list">
                    <li class="pure-menu-item"><a alt="Contato" title="Contato" class="ajaxifythispage" href="<?= get_permalink (3981); ?>"><i class="mapafeiras_icon-email pure-hide pure-md-show" aria-hidden="true"></i><span class="pure-md-hide">Contato</span></a></li>
                    <li class="pure-menu-item">
                        <span class="rrssb-trigger" alt="Compartilhe" title="Compartilhe">
                            <i class="mapafeiras_icon-espalhe pure-hide pure-md-show" aria-hidden="true"></i>
                            <span class="pure-md-hide">Compartilhe</span>
                        </span></li>
                </ul>
            </div>
        </div>
        <div class="pure-menu pure-menu-horizontal idec-silly-menu pure-md-hide">
            <ul class="pure-menu-list">
                <li class="pure-menu-item menu-o-que-e"><a class="ajaxifythispage" href="<?= get_permalink( get_page_by_path( ABOUT_SLUG ) ) ?>">O que é</a></li>
                <li class="pure-menu-item menu-adicionar-local"><a class="ajaxifythispage" href="<?= get_permalink( get_page_by_path( ADD_PLACE_SLUG ) ) ?>">Adicionar Local</a></li>
            </ul>            
        </div>
        <a href="https://www.idec.org.br" target="_blank">
            <div class="idec-logo">        
        </div>
        </a>
    </div>
    </div>
</div>
