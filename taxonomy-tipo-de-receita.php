<?php $term = get_term_by('slug', get_query_var('term'), 'tipo-de-receita'); ?>
<?php $headers = idec_generate_page_headers('receita', 'tipo-de-receita', $term->slug); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php $pL = idec_pageLocation('tipo-de-receita', $term->slug, get_term_link($term->slug, 'tipo-de-receita'), $headers->title, $term->name, '0', 'receita', $paged); ?>

<?php get_template_part('archive', 'receita'); ?>
