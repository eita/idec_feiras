<div class="idec-wrapper">
    <div id="item-ajax-content" class="removeFilterOnClick contents loading"><img class='loading' src="<?= BASE_URI ?>/images/logo_loading_em_verde_200x85.gif" /></div>
    <div id="idec-main-wrapper<?= (PAGE_TYPE=='map') ? '-fixed' : '' ?>" class="pure-g">
        <div id="sidebar" class="idec-menusearchlist-wrapper idec-menusearchlist-wrapper<?= (PAGE_TYPE=='map') ? '-fixed' : '' ?>-sm pure-u-md-2-5 pure-u-lg-7-24">
            <div class="idec-menusearch-wrapper idec-menusearch-wrapper-sm">
                <div class="pure-menu pure-menu-horizontal idec-menu-wrapper idec-menu-wrapper-sm">
                    <a href="/" class="pure-menu-heading pure-menu-heading-sm idec-feiras-logo idec-feiras-logo-sm">
                    </a>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'idec_main_navigation',
                            'container' => '',
                            'menu_class'=> 'pure-menu-list'
                         ) );
                    ?>
                </div>
                <?php /* <div class="idec-search-wrapper idec-search-wrapper-sm<?= (IS_SINGLE) ? " pure-hide pure-md-show" : "" ?>"> */ ?>
                    <div class="idec-search-wrapper idec-search-wrapper-sm">
                    <?php get_template_part( 'template-parts/search', PAGE_TYPE ); ?>
                </div>
            </div>
            <div class="removeFilterOnClick idec-list-wrapper idec-list-wrapper-sm pure-hide pure-md-show">

                <div class="idec-list-header pure-md-hide">
                    <div class="idec-list-header-mapa">Mapa</div>
                    <div class="idec-list-header-back">Voltar</div>
                </div>
                <div id="sidebarLoading"><img src="<?= BASE_URI ?>/images/loader3.gif"></div>
                <div id="locationStats" class="removeFilterOnClick">
                    <?php if(PAGE_TYPE!='map') { ?>
                        <?php get_template_part('sidebar', PAGE_TYPE); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div id="idec-content-wrapper" class="removeFilterOnClick pure-u-1 pure-u-md-3-5 pure-u-lg-17-24">
