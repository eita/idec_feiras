<?php $term = idec_get_this_locality_from_term('cidade'); ?>
<?php $headers = idec_generate_page_headers('map', 'cidade', $term->slug); ?>
<?php idec_pageLocation('cidade', $term->slug, get_term_link($term->slug, 'cidade'), $headers->title, $term->label, '0', 'map'); ?>
<?php get_header(); ?>

    <div id="main_map" class="removeFilterOnClick"></div>

<?php get_footer('map'); ?>
