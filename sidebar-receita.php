<?php $tiposDeReceita = idec_receita_get_non_empty('tipo-de-receita'); ?>
        <div class="locations_stats">
            <div class="locations_stats-body">
                <ul id="locationStatsList" class="items_list">
                <?php foreach($tiposDeReceita as $slug=>$tipoDeReceita) { ?>
                    <li class="with-separator">
                        <a class="ajaxifythis idec-list-item-content" href="<?= $tipoDeReceita->url ?>" data-item-id="<?= $tipoDeReceita->slug ?>" data-item-type="tipo-de-receita" data-page_title="<?= $tipoDeReceita->page_title ?>" data-item-is_overlay=0>
                            <div>
                                <div class="idec-list-item-icon"><?= $tipoDeReceita->count ?></div>
                                <h1 class="idec-list-item-title"><?= $tipoDeReceita->label ?></h1>
                            </div>
                        </a>
                    </li>
                <?php } ?>
                </ul>
            </div>
        </div>
