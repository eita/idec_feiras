<?php
require_once( ABSPATH . 'wp-admin/includes/image.php' );
require_once( ABSPATH . 'wp-admin/includes/file.php' );
require_once( ABSPATH . 'wp-admin/includes/media.php' );

add_action("wpcf7_before_send_mail", "wpcf7_do_something");

function wpcf7_do_something ($form) {

    //$form_id = $form->id; // gets current form id
	$form_title = $form->title;

	$submission = WPCF7_Submission::get_instance();
	$data = $submission->get_posted_data();
	$fotos = $submission->uploaded_files();

	if($form_title == "Adicionar Novo Local") {

	    $edit_id = $data['edit_id'];

		$author_name = $data['nome'];
		$author_email = $data['email'];
		$author_fone = $data['telefone'];
		$author_assoc_idec  = $data['assoc_idec'][0];

		//item
		$tipo = $data['tipo'];
		$titulo = $data['titulo'];
		$endereco = $data['endereco'];
		$uf = $data['uf'];
		$city = $data['cidade'];

		$selos = $data['selos'];
		$produtos_da_sociobio = $data['produtos-da-sociobiodiversidade'];

		$dias = $data['dias-funcionamento'];
		// $horario = $data['horario'];
		// $periodo = $data['periodo-funcionamento'];
		$produtos = $data['produtos'];
		$cafe = $data['cafe'][0];
		$referencia = $data['referencia'];
		if (($referencia) AND ((substr($referencia, 0 , 7) != 'http://') AND (substr($referencia, 0 , 8) != 'https://')))
			$referencia = "http://" . $referencia;
		// $foto = $fotos['foto'];
		$descricao = $data['descricao-produtos'];
		$contato = $data['contato'];

		//dados do autor
		$faixa_etaria = $data['faixa-etaria'];
		$sexo = $data['sexo'];
		$escolaridade = $data['escolaridade'];
		$regiao = $data['regiao'];

		$prof1 = $data['produtor-de-produtos-organicos'];
		$prof2 = $data['comerciante-de-produtos-organicos'];
		$prof3 = $data['profissional-da-saude'];
		$prof4 = $data['profissional-da-area-ambiental'];
		$prof5 = $data['gestor-de-politicas-publicas'];

		$participacao_na_feira = $data['participacao-na-feira'];
		$onde_ocorre_a_feira = $data['onde-ocorre-a-feira'];
		$outros_espacos = $data['outros-espacos'];

		$comercio_cond_descricao = $data['comercio-cond-descricao'];

		$outra_profissao = $data['outra-profissao'];

		$segunda_horario = $data['segunda_horario'];
		$terca_horario = $data['terca_horario'];
		$quarta_horario = $data['quarta_horario'];
		$quinta_horario = $data['quinta_horario'];
		$sexta_horario = $data['sexta_horario'];
		$sabado_horario = $data['sabado_horario'];
		$domingo_horario = $data['domingo_horario'];

		$periodo_segunda = $data['periodo-funcionamento-segunda'];
		$periodo_terca = $data['periodo-funcionamento-terca'];
		$periodo_quarta = $data['periodo-funcionamento-quarta'];
		$periodo_quinta = $data['periodo-funcionamento-quinta'];
		$periodo_sexta = $data['periodo-funcionamento-sexta'];
		$periodo_sabado = $data['periodo-funcionamento-sabado'];
		$periodo_domingo = $data['periodo-funcionamento-domingo'];

		$periodo_geral = array(null);

		$dias_periodos = array($periodo_segunda, $periodo_terca, $periodo_quarta, $periodo_quinta, $periodo_sexta, $periodo_sabado, $periodo_domingo);

		foreach ($dias_periodos as $k => $d) {

			if(is_array($d)) {

				$periodo_geral = array_merge($periodo_geral, $d);
			}

		}

		array_shift($periodo_geral);
		$periodo_geral = array_unique($periodo_geral);
		sort($periodo_geral);

		$observacoes_sobre_o_horario = $data['observacoes-sobre-o-horario'];

		//position
		$latlng = $data['latlng'];
		$address = $data['address'];

		$type = ($edit_id) ? 'revisao' : 'item';

		$post = array(
			'post_status'    => 'draft',
			'post_content'	 => $descricao,
			'post_title'     => $titulo,
			'post_type'      => $type
			);


		$newid = wp_insert_post( $post );


		//informações do author
		update_post_meta( $newid, 'wpcf-nome', $author_name );
		update_post_meta( $newid, 'wpcf-email', $author_email );
		update_post_meta( $newid, 'wpcf-telefone', $author_fone );
		update_post_meta( $newid, 'wpcf-associado-idec', $author_assoc_idec );

		update_post_meta( $newid, 'wpcf-faixa-etaria', $faixa_etaria);
		update_post_meta( $newid, 'wpcf-sexo', $sexo);
		update_post_meta( $newid, 'wpcf-escolaridade', $escolaridade);
		update_post_meta( $newid, 'wpcf-regiao', $regiao);
		update_post_meta( $newid, 'wpcf-participacao-na-feira', $participacao_na_feira);
		update_post_meta( $newid, 'wpcf-onde-ocorre-a-feira', $onde_ocorre_a_feira);
		update_post_meta( $newid, 'wpcf-outros-espacos', $outros_espacos);

		//profissao
		update_post_meta( $newid, 'wpcf-produtor-de-produtos-organicos', $prof1);
		update_post_meta( $newid, 'wpcf-comerciante-de-produtos-organicos', $prof2);
		update_post_meta( $newid, 'wpcf-profissional-da-saude', $prof3);
		update_post_meta( $newid, 'wpcf-profissional-da-area-ambiental', $prof4);
		update_post_meta( $newid, 'wpcf-gestor-de-politicas-publicas', $prof5);
		update_post_meta( $newid, 'wpcf-outra-profissao', $outra_profissao);

		//tipo
	    wp_set_object_terms( $newid, $tipo, 'tipo', true);

	    //condições para ponto de comercialização
	    update_post_meta( $newid, 'wpcf-comercio-cond-descricao', $comercio_cond_descricao );

	    //dados do local

		update_post_meta( $newid, 'wpcf-segunda_horario', $segunda_horario );
		update_post_meta( $newid, 'wpcf-terca_horario', $terca_horario );
		update_post_meta( $newid, 'wpcf-quarta_horario', $quarta_horario );
		update_post_meta( $newid, 'wpcf-quinta_horario', $quinta_horario );
		update_post_meta( $newid, 'wpcf-sexta_horario', $sexta_horario );
		update_post_meta( $newid, 'wpcf-sabado_horario', $sabado_horario );
		update_post_meta( $newid, 'wpcf-domingo_horario', $domingo_horario );

		update_post_meta( $newid, 'wpcf-periodo-segunda', is_array($periodo_segunda) ? implode(',', $periodo_segunda) : '');
		update_post_meta( $newid, 'wpcf-periodo-terca', is_array($periodo_terca) ? implode(',', $periodo_terca) : '');
		update_post_meta( $newid, 'wpcf-periodo-quarta', is_array($periodo_quarta) ? implode(',', $periodo_quarta) : '');
		update_post_meta( $newid, 'wpcf-periodo-quinta', is_array($periodo_quinta) ? implode(',', $periodo_quinta) : '');
		update_post_meta( $newid, 'wpcf-periodo-sexta', is_array($periodo_sexta) ? implode(',', $periodo_sexta) : '');
		update_post_meta( $newid, 'wpcf-periodo-sabado', is_array($periodo_sabado) ? implode(',', $periodo_sabado) : '');
		update_post_meta( $newid, 'wpcf-periodo-domingo', is_array($periodo_domingo) ? implode(',', $periodo_domingo) : '');

		update_post_meta( $newid, 'wpcf-observacoes-sobre-o-horario', $observacoes_sobre_o_horario);

		update_post_meta( $newid, 'wpcf-address-custom', $endereco );
		// update_post_meta( $newid, 'wpcf-horario', $horario );
		update_post_meta( $newid, 'wpcf-cafe-da-manha', $cafe );
		update_post_meta( $newid, 'wpcf-referencia', $referencia );
		update_post_meta( $newid, 'wpcf-contato', $contato );
	    wp_set_object_terms( $newid, $uf, 'uf', true);
		wp_set_object_terms( $newid, $city . " - " . $uf, 'cidade', true);
	    wp_set_object_terms( $newid, $dias, 'dia', true);
	    wp_set_object_terms( $newid, $periodo_geral, 'periodo', true);
	    wp_set_object_terms( $newid, $produtos, 'produto', true);
	    wp_set_object_terms( $newid, get_regiao($uf), 'regiao', true);

			wp_set_object_terms( $newid, $selos, 'selo', true);

	  //position
		update_post_meta( $newid, 'wpcf-address', $address );
		update_post_meta( $newid, 'wpcf-latlng', $latlng );

		//sociobio
		update_post_meta( $newid, 'wpcf-produtos-da-sociobiodiversidade', $produtos_da_sociobio );

		// set human readable times
		update_wpcf_horario ( $newid );

		if ($fotos) {
			$file_array = array(
			    'name' => basename( $fotos['foto'] ),
			    'tmp_name' => $fotos['foto']
			);

			add_filter( 'upload_mimes', 'idec_mime_types', 999 );

			$overrides['test_type'] = true;

			$attachment_id = media_handle_sideload( $file_array, $newid , pathinfo($fotos['foto'])['filename'], $overrides);

			if ( is_wp_error( $attachment_id ) ) {
			    @unlink( $file_array['tmp_name'] );
			    return $attachment_id;
			}

			set_post_thumbnail( $newid, $attachment_id );


	    }

   		if($edit_id) {

			update_post_meta($newid, '_wpcf_belongs_item_id', $edit_id );

			$msg_admin = "Nova revisão do item: ".get_the_title($edit_id).".";
			$msg_admin .= "<br />";
			$msg_admin .= "Visite ".admin_url( 'post.php?post='.$edit_id.'&action=edit')." para ver as revisões.";

			add_filter( 'wp_mail_content_type', 'set_html_content_type' );
			// wp_mail("iris.rosa@gmail.com", 'Nova Revisão', $msg_admin );
			// wp_mail("feirasorganicas@idec.org.br", 'Nova Revisão', $msg_admin );
			wp_mail(get_all_emails(), 'Nova Revisão', $msg_admin);
			remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

		} else {

			$msg_admin = "Novo item: ".get_the_title($newid).".";
			$msg_admin .= "<br />";
			$msg_admin .= "Visite ".admin_url( 'post.php?post='.$newid.'&action=edit')." para ver os detalhes.";

			// add_filter( 'wp_mail_content_type', 'set_html_content_type' );
			// wp_mail(get_all_emails(), 'Novo item', $msg_admin );
			// remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

		}

	}



}

function set_html_content_type() {

	return 'text/html';
}

function get_regiao($uf) {

	$regioes = array(
		'norte' => array('AM', 'RR', 'AP', 'PA', 'TO', 'RO', 'AC'),
		'nordeste' => array('MA', 'PI', 'CE', 'RN', 'PE', 'PB', 'SE', 'AL', 'BA'),
		'centro-oeste' => array('MT', 'MS', 'GO'),
		'sudeste' => array('SP', 'RJ', 'ES', 'MG'),
		'sul' => array('PR', 'RS', 'SC')

		);

	foreach ($regioes as $key => $r) {
		if(in_array($uf, $r)){ $ok=true; return $key; }
	}

	if(!$ok) { return false; }
}

function idec_mime_types( $mimes ) {

        // New allowed mime types.
        $mimes['png'] = 'image/png';
		$mimes['jpg'] = 'image/jpg';
        // Optional. Remove a mime type.
        unset( $mimes['exe'] );

	return $mimes;
}
