<div class="idec-content">
	<div class="idec-content-item">
		<div class="idec-content-head">
		    <div class="idec-content-head-back">
				<a href="#" id="closeit">
				    Voltar <i class="mapafeiras_icon-fechar" aria-hidden="true"></i>
			    </a>
			</div>
		</div>
		<div class="idec-ficha">
			<?php if (has_post_thumbnail()): ?>
				<div class="idec-receita-image idec-receita-image-sm">
				    <div class="cycle-slideshow idec-content-item-slideshow idec-content-item-slideshow-sm">
					    <div class="idec-content-item-image-slide image-slide" style="background-image:url(<?= get_the_post_thumbnail_url(null, 'large') ?>)"></div>
				    </div>
			    </div>
			<?php endif ?>
			<div class="idec-content-title-row idec-content-title-row-sm">
				<h1><?php the_title(); ?></h1>
			</div>
			<?php the_content(); ?>
		</div>
	</div>
</div>
