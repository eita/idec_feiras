<?php $term = get_term_by('slug', get_query_var('term'), 'alimento'); ?>
<?php $headers = idec_generate_page_headers('receita', 'alimento', $term->slug); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php $pL = idec_pageLocation('alimento', $term->slug, get_term_link($term->slug, 'alimento'), $headers->title, $term->name, '0', 'receita', $paged); ?>

<?php get_template_part('archive', 'receita'); ?>
