<?php
    $content_archive = "";
    global $wp_query;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $numpages = $wp_query->max_num_pages;
    if ( have_posts() ) : while ( have_posts() ) : the_post();
        $image_url = (has_post_thumbnail())
            ? get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' )
            : '/wp-content/themes/IDEC_feiras/images/icone_receitas.svg';
        $content_archive .= idec_get_template_model('general-archive', 'item-content', array('item_id'=>get_the_ID(), 'item_url'=>get_the_permalink(), 'image_url'=>$image_url, 'image_title'=>get_the_title(), 'extra_image_class'=>'', 'title'=>get_the_title()));
    endwhile; endif;
    $intro = idec_generate_page_headers('receita', 'archive', '')->description;
    $count = idec_generate_count_results_text('idec-receita-count', wp_count_posts('receita')->publish, 'receita', qtty(wp_count_posts('receita')->publish, 'disponível', false), $paged, $numpages);
    $pagination = idec_custom_pagination('', '', idec_get_page_type_homeurl());
?>

<?= idec_get_template_model('general-archive', 'content', array('closeit'=>'', 'image'=>'', 'title'=>'', 'intro'=>$intro, 'count'=>$count[0], 'whereami'=>$count[1], 'content_archive'=>$content_archive, 'pagination'=>$pagination)); ?>
