<?php 
    $pL = idec_pageLocation();
    $collection = idec_biblioteca_get_single_collection($pL->slug);
    $items = idec_biblioteca_get_filtered_items("", "", $collection->id, $pL->paged);
    $closeit = idec_get_template_model('general-closeit', 'content', array('see_all'=>'ver todos os conteúdos', 'homeurl'=>idec_get_page_type_homeurl()));
    $content_archive = "";
    foreach($items->data as $item) {
        $content_archive .= idec_get_template_model('general-archive', 'item-content', array('item_id'=>$item->id, 'item_url'=>idec_get_page_type_homeurl().'?id='.$item->id, 'image_url'=>BASE_URI."/images/loader3.gif", 'image_title'=>'loading', 'extra_image_class'=>' item_thumbnail', 'title'=>$item->title));
    }
    $count = idec_generate_count_results_text('idec-biblioteca-count', $items->count, 'conteúdo', 'no tema "'.$collection->title.'"', $items->paged, $items->pages);
    $pagination = idec_custom_pagination($items->pages, $items->paged, $pL->href);
?>

<?= idec_get_template_model('general-archive', 'content', array('closeit'=>$closeit, 'image'=>'', 'title'=>$collection->title, 'intro'=>'', 'count'=>$count[0], 'whereami'=>$count[1], 'content_archive'=>$content_archive, 'pagination'=>$pagination)); ?>
