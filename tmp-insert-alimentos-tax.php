<?php

$alimentos = array(

'Abricó' => 'É cultivado nos igapós e margens inundáveis de rios na região Amazônica, principalmente no estado do Pará. O fruto pode ser consumido in natura, em forma de salada, licores, compotas, geleias e sucos, ou processado.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Abiu' => 'Encontrada em estado cultivado no interior paraense, produzindo frutos de grande aceitação popular, utilizados em sua maioria para consumo in natura. A fruta pode ser conservada sob-refrigeração por até uma semana, mas também pode ser processada na forma de geleia. A polpa tem sabor doce e suave.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Açaí' => 'Na região Amazônica, o açaí exerce importante papel socioeconômico e cultural, pois a bebida obtida a partir de seus frutos tem consumo regional elevado, e a exportação tem aumentado muito nos últimos anos. O açaí é considerado um alimento de grande valor nutricional, pois apresenta em sua composição fibra alimentar, antocianinas, minerais, particularmente, cálcio e potássio e ácidos graxos essenciais. A polpa pode ser utilizada na preparação de sobremesas, sucos, vinhos, licores ou sorvetes. Os nativos extraem sua polpa, que é consumida pura ou acompanhada de farinha de mandioca ou tapioca.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Bacuri' => 'Essa árvore ocorre naturalmente desde a Ilha de Marajó, na foz do Rio Amazonas, até o Piauí, seguindo a costa do Pará e do Maranhão. O bacuri é uma das frutas mais populares da região Amazônica, sendo pouco maior que uma laranja. Contém polpa agridoce rica em potássio, fósforo e cálcio. O óleo extraído de suas sementes é usado como anti-inflamatório e cicatrizante na medicina popular e na indústria de cosméticos. A fruta é consumida diretamente ou utilizada na produção de doces, sorvetes, sucos, geleias, licores e outras iguarias.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Biribá' => 'Os frutos tipo cápsula abrem-se quando maduros, liberando entre uma e quatro sementes, que são comestíveis. É uma das frutas mais populares e apreciadas de toda a região Amazônica e também do Nordeste brasileiro. Sua polpa é suculenta e pouco fibrosa, de cor branca a creme, sabor agradável e doce. Quase sempre é consumido in natura, mas também pode ser apreciado na forma de sucos e sorvetes.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Cajarana' => 'Possui sabor pouco ácido se consumido ao natural. É utilizada na forma de sucos, sorvete e doces.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Castanha-do-Brasil' => 'É um dos principais produtos da nossa economia extrativista, com significativo valor no mercado de exportação. O fruto da castanheira, chamada de ouriço, constitui-se em uma resistente cápsula que não se abre espontaneamente, abrigando, em seu interior, entre 10 e 25 sementes. A amêndoa é rica em gordura e proteína. A castanha-do-brasil é consumida fresca ou assada e é ingrediente da composição de inúmeras receitas de doces e de salgados.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Cupuaçu' => 'É uma das frutas mais populares da Amazônia .Possui 30% de polpa e cerca de 35 sementes. A polpa é utilizada no preparo de sorvetes, sucos, geleias, doces, mousses, bombons, balas, biscoitos e iogurtes. As sementes, depois de secas, são utilizadas na fabricação de chocolate branco ou “cupulate”.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Ingá' => 'Possui polpa branca, fibrosa, que envolve sementes negras e brilhantes, de consistência macia e sabor adocicado. É consumido in natura.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Jambo' => 'O jambo possui forma de pera, é vermelho quando maduro, com aproximadamente 7 cm de comprimento, de casca fina e polpa branca, suculenta, crocante, comestível e levemente adocicada, dotado de uma única semente. O fruto contém vitaminas A, B1, B12, além de cálcio, ferro e fósforo. É consumido in natura ou sob a forma de sucos, molhos, compotas, geleias e doces em calda.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Murici' => 'Está distribuído por toda a Amazônia brasileira, atingindo os estados de Mato Grosso e Minas Gerais. Os frutos possuem coloração amarela quando maduros. É boa fonte de energia, pois apresenta altos teores de gordura. Possui sabor agridoce, é comestível in natura e usado para o preparo de doces, licores, sucos e sorvetes, refrescos, geleias, pudins e pavês.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Pupunha' => 'A pupunheira é nativa dos trópicos úmidos da Amazônia. Fruta de excelente valor energético e elevado teor de vitamina A, apresenta polpa carnuda, espessa e, às vezes, fibrosa. Tradicionalmente, o fruto da pupunha é consumido de uma única forma na maioria dos lugares onde ocorre: após separados do cacho, os frutos são cozidos em água com sal durante 30 a 60 minutos; em seguida, são descascados, partidos pelo comprimento, a semente extraída e estão prontos para o consumo, servidos no lanche ou com café acompanhados com mel, açúcar ou ao natural. Outro uso para os frutos cozidos é a preparação de diversas comidas caseiras, ou moídos para produção de farinha, que pode ser usada em uma variedade de receitas culinárias.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Jambu' => 'Na Amazônia, é encontrado em hortas domésticas e cultivado com finalidade comercial por pequenos agricultores. Suas folhas devem estar viçosas (sem amarelados ou murchas), sem marcas de insetos ou machucados. O uso das folhas e talos do jambu como hortaliça é indispensável na preparação de iguarias regionais como o pato e o tambaqui no tucupi e o tacacá, cujos ingredientes são o tucupi, a goma de mandioca, o jambu e o camarão seco. Além disso, a folhagem é utilizada em cozidos e sopas.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Farinha de Uarini' => 'Vem do município amazonense de Uarini, sendo a principal base da sua economia. Em seu processo de produção, a mandioca amarela é colocada em água até apodrecer. Seus grãos são duros e precisam ser hidratados para amolecerem, embora alguns amazonenses os comam in natura. Tradicionalmente usada na mesa de nativos, onde compõe pratos típicos como casquinha de caranguejo, pirarucu de casaca e farofa de jabá, vem sendo também empregada para incorporar receitas contemporâneas, criativas e de agradável sabor.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Cajá' => 'A cajazeira é encontrada principalmente nos Estados do Norte e Nordeste, porém ainda não é cultivada em escala comercial, sendo considerada planta em domesticação e de exploração extrativa. Os frutos possuem excelente sabor e aroma, além de rendimento acima de 60% em polpa e, por isso, são amplamente utilizados na confecção de suco, néctar, sorvetes, geleias, vinhos, licores etc. Devido a sua acidez, normalmente, não são consumidos ao natural.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Caju' => 'A castanha (fruto), por sua vez, tornou-se especiaria de luxo, indispensável na culinária nordestina e muito difundida em todo o mundo. A castanha-de-caju possui proteínas ricas em aminoácidos essenciais e alto teor de gorduras, característico das sementes oleaginosas. Para seu melhor aproveitamento in natura, o caju deve ser consumido no mesmo dia da compra e a casca deve ter cor firme, sem manchas ou machucados. Pode ficar na geladeira por, no máximo, dois dias.',
'Ciriguela' => 'Sob o ponto de vista alimentar, trata-se de um fruto extremamente rico em carboidratos, cálcio, fósforo, ferro e vitaminas A, B e C. Devido a sua excelente qualidade organoléptica, a ciriguela tem muita apreciação no Nordeste brasileiro, refletida pelo contínuo aumento do consumo do fruto in natura ou processado na forma de diversos produtos, normalmente disponibilizados no mercado, o que tem proporcionado crescente interesse para o cultivo comercial. É uma fruta saborosa que, além de consumida in natura, é utilizada para o preparo de sucos, sorvetes, geleias e compotas e também no preparo de bebidas fermentadas, vinhos e bebidas geladas.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Coco' => 'Da polpa madura extrai-se o óleo, que, por ser mais rico em gordura saturada, aproxima-se das características da gordura animal, e o leite de coco, que apresenta alto teor de gorduras, sais minerais (como potássio e fósforo) e proteínas. A polpa pode ser utilizada em diversos pratos da culinária, tanto em doces como em salgados. A água de coco apresenta sabor adocicado e refrescante, sendo excelente líquido hidratante oral. Contém glicose e sais minerais, como sódio, potássio e cloro.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Fruta-pão' => 'Fruto com alto conteúdo de carboidratos. A polpa, quando cozida ou assada, apresenta paladar bastante semelhante ao da batata-doce e macaxeira, tornando-se mais agradável quando consumida com mel ou melaço. Quando maduros, os frutos podem ser aproveitados para a confecção de doces. Com relação à fruta-pão com semente, a polpa não tem valor alimentício, limitando-se a sua importância ao consumo de sementes assadas, fervidas em água e sal ou torradas, como a castanha europeia, com as quais se parecem em gosto, sabor e forma. São também bastante valiosas por possibilitar a extração de uma farinha alimentícia muito nutritiva.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Graviola' => 'A graviola é um fruto de grande aceitação na agroindústria devido ao seu excelente sabor e ao aroma agradável de sua polpa, podendo ser processado na forma de suco concentrado, xarope, bebida (champola) e néctar. No entanto, grande parte de sua produção é consumida in natura, na forma de suco, refresco, sorvetes, saladas ou com açúcar.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Mamão' => 'O Brasil é o primeiro produtor mundial de mamão, situando-se entre os principais países exportadores, principalmente para o mercado europeu. O mamão apresenta-se como fonte de nutrientes, principalmente, ácido ascórbico e provitamina A. Esta fruta é consumida preferencialmente fresca. A polpa do fruto maduro é usada para produção de conservas, doces, geleias, sucos e néctares, combinados ou não com outras frutas tropicais, além de purê, pelo processo asséptico ou na forma congelada. Também é comum o uso dos frutos verdes, seja ralado ou em pedaços para fazer doces, seja para uso em pratos salgados.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Maracujá' => 'O maracujá é uma fruta de aroma e acidez acentuados. Pode ser consumido ao natural ou na forma de sucos, doces, geleia, sorvete e licor. Os princípios ativos maracujina, passiflorine e calmofilase conferem ao maracujazeiro propriedades calmantes, hipnóticas, analgésicas e anti-inflamatórias.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Pitanga' => 'A fruta possui casca dura, porém fácil de ser aberta, fina polpa suculenta e doce, além de um caroço que ocupa a maior parte do conteúdo. É rica em vitamina C. Pode ser consumida in natura ou beneficiada na fabricação de licores ou polpa.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Umbu' => 'Grande parte da sua composição é aquosa e possui consideráveis propriedades nutricionais, sendo rico em vitamina C. O umbu é consumido in natura, como fruta de mesa, preparado na forma de refresco, sorvete e como ingrediente da tradicional umbuzada, que é a mistura de leite com o suco da fruta. A fabricação caseira de doce de umbu e concentrado de suco, conhecido como “vinho” ou “vinagre”, é receita popular que proporciona agregação de valor ao produto para a venda local. Os frutos maduros duram no máximo dois ou três dias.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Abóbora' => 'Os frutos são ricos em vitamina A e também fornecem vitaminas do complexo B, cálcio e fósforo. A abóbora é consumida em saladas, cozidos, refogados, sopas, purês, pães, bolos, pudins e doces. As sementes podem ser torradas e consumidas como aperitivo. Elas podem conter até 50% de óleo e 35% de proteína, sendo consideradas como suplemento proteico. O óleo da semente de abóbora vem tendo ampla aceitação, não só como óleo comestível, mas como produto antioxidante.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Maxixe' => 'Os frutos são fonte de sais minerais, principalmente zinco, e têm poucas calorias. É tradicionalmente consumido cozido ou refogado, puro ou juntamente com carnes, abóbora, quiabo, feijão e temperos. Também se consome na forma crua, como salada, raspando os frutos, retirando uma casca fina. Os frutos devem ser consumidos verdes, pois, quando maduros, tornam-se fibrosos e amargos.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Vinagreira' => 'Na alimentação, destaca-se no Maranhão como base de pratos da culinária local, sendo o mais significativo o arroz de cuxá, feito com arroz, camarão seco, vinagreira e condimentos. Os frutos e cálices são matéria-prima para a fabricação de sucos, doces e geleias. As sépalas são desidratadas e utilizadas para o preparo do chá de hibisco.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Inhame' => 'As túberas do inhame são altamente energéticas, ricas em carboidratos, amido, vitaminas do complexo B e minerais, possuem baixo teor de gorduras e são reconhecidas pelas propriedades depurativas do sangue. Pode ser consumido cozido, assado, em pirão, sopas, cremes, pães, bolos, biscoitos, panquecas e tortas. Pode substituir a batata em vários pratos. Evite comprar inhames murchos ou brotados. Escolha aqueles firmes, sem partes mofadas ou amolecidas. O inhame se conserva bem por mais de 15 dias sem necessidade de refrigeração, desde que mantidos em locais frescos, secos, escuros e arejados.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015; EMBRAPA, Hortaliças na Web.',
'Mandioca' => 'Constitui um dos principais alimentos energéticos utilizados no Brasil, devido à sua rusticidade e ampla adaptabilidade. A melhor alternativa para o armazenamento doméstico e para a comercialização tem sido o congelamento da raiz descascada ou conservada por alguns dias imersa em água. Outros indicadores de boa qualidade são a polpa úmida e a casca que se solta com facilidade. Para a fabricação de farinhas, muito utilizadas principalmente nas regiões Norte e Nordeste, existem as casas de farinha, onde são produzidos, de forma artesanal, os diversos tipos de farinha consumidos no Brasil. Esse tipo de produção atualmente envolve o trabalho intrafamiliar ou até mesmo comunitário, garantindo o próprio consumo e a geração de renda.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Abacaxi-do-cerrado' => 'Os frutos inteiros maduros conservam-se por uma semana ou mais sem sofrer danos e, se estiverem semimaduros, podem durar por mais tempo. É consumido in natura, pois apresenta polpa doce e suculenta. O miolo pode ser triturado e aproveitado em sucos e geleias.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Araticum' => 'É um fruto comestível, doce, muito cheiroso. Sua folha forrageira é considerada venenosa pelo pantaneiro. O araticum pode ser considerado boa fonte de lipídios e de fibras. Os frutos podem ser consumidos ao natural e sua polpa pode ser utilizada em doces, sucos, geleias, iogurtes, licores, tortas e sorvetes.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Baru' => 'O baruzeiro, que é uma leguminosa, oferece um fruto de casca fina no qual se esconde uma amêndoa dura e comestível. O baru tem alto valor nutricional, superando os 26% de teor de proteínas encontrados no coco-da-bahia. A amêndoa é rica em cálcio, fósforo e manganês, contém 45% de óleos e o valor proteico e o gosto se assemelham aos do amendoim. As sementes ou amêndoas dessa espécie fornecem óleo de primeira qualidade. A composição dos ácidos graxos desse óleo revelam teor relativamente alto de ácido linoleico. . A amendôa pode ser comida crua ou torrada e, nesse último caso, substitui com equivalência a castanha-de-caju, servindo como ingrediente em receitas de pé de moleque, rapadura e paçoquinha. É utilizada também para enriquecer pães, bolos, sorvetes e acompanhar aperitivos.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Cagaita' => 'Os frutos tem sabor agradável a levemente ácido e são laxativos. A importância principal do aproveitamento da cagaiteira se dá pelo potencial alimentício de seus frutos. A cagaita é um fruto suculento, sendo considerado boa fonte de vitamina B2, cálcio, magnésio e ferro. O óleo da polpa da cagaita apresenta aproximadamente 28% de ácidos graxos saturados, 50% de ácidos graxos monoinsaturados e 22% de ácidos graxos de polinsaturados, principalmente ácido linolênico (12%), que é um ácido graxo essencial, isto é, não é sintetizado pelo organismo e precisa ser ingerido pela dieta. O óleo essencial das folhas apresenta atividade antifúngica.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Jatobá' => 'É fruto comestível, podendo ser consumido cru ou cozido com leite. Fornece farinha de ótimo valor nutritivo. Cerca de 60 vagens dão 1 kg de farinha. Seu valor proteico e utilização são iguais ao do fubá.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Jenipapo' => 'Fruto de sabor doce, que é utilizado no preparo de suco, jenipapada, passas, vinagre, doce e licor. A parte sólida é a polpa, utilizada em doces, e a líquida, em refrescos, vinhos e licores. Se o jenipapo for para compota ou doce cristalizado, não se deve macerar a polpa. A polpa pode ser congelada.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Marmelada-de-cachorro' => 'Os frutos novos de marmelada-de-cachorro possuem coloração verde; e os maduros, negro-azulada. A polpa, de sabor adocicado e coloração escura, pode ser consumida in natura e em forma de geleias e doces.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Pequi' => 'A “casca” do fruto do pequizeiro, processada em farinha, apresenta valores em lipídios, proteínas, carboidratos totais e fibra alimentar de, respectivamente, 1,54%, 5,76%, 50,94% e 39,97%. A polpa de pequi contém de 70,9 mg/100 g a 105 mg/100 g de vitamina C, valores acima dos da laranja, da goiaba, da banana-d’água e da maçã argentina, sendo o valor máximo superior ao suco de limão. A polpa e a amêndoa do pequi contêm 267,9 kcal/100 g e 317 kcal/100 g, respectivamente, constituindo uma fonte rica de calorias. O fruto é usado para fabricação de licor, e a polpa é consumida com arroz, feijão, galinha ou batida com leite e açúcar.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Pera-do-cerrado' => 'Os frutos maduros possuem coloração amarelo-esverdeada e apresentam casca fina e polpa mole, com certa adstringência. Pode ser usada para doce em compota e geleia.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Gueroba' => 'Diferentemente do palmito comum, a guariroba, ou gueroba, apresenta textura firme. Alimento substancial e de paladar amargo característico, seu palmito pode ser consumido frio, em saladas e conservas; ou quente, cozido puro ou refogado, servindo de base para vários pratos, como tortas salgadas, pastéis e empadões. Além da gueroba, o famoso empadão goiano leva frango, linguiça e batata. Da polpa extraída dos frutos podem-se preparar vitaminas, sorvetes e refrescos.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Milho-verde' => 'É rico em carboidratos, sendo assim um alimento energético. Também é fonte de óleo e fibras. No Brasil, o mesmo milho plantado para colheita de grão seco é colhido ainda verde para consumo. Cozido ou assado na espiga, na forma de pamonha, curau e mingau. Mas pode ser preparado também em pratos salgados, como sopas, cremes, suflês, pães, refogado com temperos, como recheio para qualquer prato, em bolinhos, farofas e saladas. Como sobremesa, pode ser usado em bolo, sorvete, cozido com mel, pudim e creme. O milho novinho, ainda em formação, ou seja, o sabuguinho, pode ser preparado com carne, em sopas e cozidos.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Abacate' => 'De sabor suave e gosto bom, nem doce nem amargo, o abacate é fruto macio e carnudo. Sua polpa cremosa assemelhasse a um creme amanteigado, sendo basicamente constituída por ácidos graxos não saturados e concentrando apenas 70% de água em sua composição, o que é pouco em comparação com a maioria das frutas existentes. Por suas qualidades e extrema suavidade ao paladar, é uma das frutas mais versáteis existentes, podendo ser utilizada em incontáveis e variadas receitas. Pelo sabor de sua polpa pouco açucarada, o abacate pode ser consumido como iguaria doce ou salgada, de acordo com os hábitos e a cultura dos povos das regiões em que é cultivado.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Caqui' => 'O fruto verde é rico em tanino – o maduro não apresenta acidez e é rico em amido, açúcares, sais minerais e vitaminas A e C. O caqui, além do consumo natural, pode ser usado tanto para preparo de passa, como para a elaboração de vinagre. A produção de caqui no Brasil se destina, na sua quase totalidade, ao consumo da fruta fresca. A passa de caqui é um produto altamente nutritivo, de sabor bastante agradável, cujo consumo, em nosso país, se restringe aos membros da colônia japonesa, talvez devido ao fato de ser produzida em pequenas quantidades.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Carambola' => 'A Fruta rica em sais minerais, vitaminas A, C e do complexo B, é, ainda, fonte natural de ácido oxálico. O fruto, quando cortado no sentido transversal, adquire a forma de uma perfeita estrela de cinco pontas, característica que lhe concedeu o nome mundial de star fruit. Pode ser consumida in natura. A polpa pode ser utilizada na preparação de sucos, sorvetes, vinhos, licores ou doces.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Jabuticaba' => 'Pode ser consumida ao natural, mas também na forma de doces, geleias, licor ou vinho. Deve ser imediatamente consumida, pois tem alto poder de fermentação. O fruto deve estar com consistência firme, brilhante e sem rachaduras.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Jaca' => 'O interior da jaca é formado por vários gomos, sendo que cada um contém um grande caroço recoberto por uma polpa cremosa, viscosa e muito aromática. A polpa, consumida in natura, possui boa quantidade de proteínas e vitamina A; se processada, compõe doces, compotas, polpas congeladas, refrescos, sucos e bebidas (licor). As sementes são ricas em amido, podem ser consumidas assadas e, quando moídas, produzem farinha utilizada no preparo de biscoitos e doces.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Manga' => 'A manga está entre as frutas tropicais de maior expressão nos mercados brasileiros e internacionais. É uma fruta polposa, de sabor e aroma muito agradáveis, além de ser grande fonte de carotenoides e carboidratos. Existe uma grande diversidade de tipos de frutos, com sabores e cores diferenciadas. É consumida preferencialmente in natura, mas também é processada pela indústria em sucos, compotas, geleias, sorvetes e chutney.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Abobrinha' => 'Os frutos são de fácil digestão, ricos em niacina e fonte de vitaminas do complexo B. Possui poucas calorias. A abobrinha pode ser consumida refogada, cozida, assada, em saladas frias, como suflê, recheadas ou como ingrediente em pastas, tortas, bolos, pizza, entre outros. Seu cozimento é rápido e não é necessário acrescentar muita água, pois a água da abobrinha é suficiente para cozinhá-la.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Berinjela' => 'A berinjela é rica em vitaminas A, B1, B2, B5, e C, potássio, fósforo, cloro, cálcio, sódio, ferro, magnésio e enxofre. A berinjela é normalmente consumida cozida, frita à milanesa, assada ou ensopada em pratos frios e quentes. Pode ser ingrediente no preparo de patês, antepasto, sucos, molhos e omeletes. Pode também ser recheada ou servida em conserva, associada a outras hortaliças, como pimentão e cebola.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Beldroega' => 'É comum encontrar beldroega em hortas caseiras, podendo-se aproveitar sua germinação espontânea, fazendo um manejo (raleio e transplante) de forma a obter plantas de melhor qualidade. Podem-se efetuar colheitas sucessivas ou uma única com a remoção de toda a planta. Na culinária, é usada quase que totalmente, com exceção das raízes. Pode ser consumida na forma de saladas cruas e em sucos, normalmente associada a frutas. O caule e as folhas são crocantes quando crus e, quando adicionados no preparo de sopas, caldos e ensopados, dão consistência cremosa.',
'Chuchu' => 'Atualmente, está entre as dez hortaliças mais consumidas no Brasil. O chuchu apresenta sabor suave, fácil digestibilidade, alto teor em fibras e possui baixa caloria. Destaca-se como fonte de potássio e vitaminas A, B1 e C. É consumido cozido, refogado, em sopas, suflês, tortas, frito à milanesa (em fatias e após breve cozimento) e em saladas frias. É também usado para dar ponto a alguns pratos salgados e doces, como o de goiaba e marmelo, devido à presença de pectina.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015; EMBRAPA, Hortaliças na Web.',
'Jiló' => 'O sabor amargo é característico do jiló e está presente em todos as variedades. É ao mesmo tempo a característica que atrai seus admiradores e afasta seus detratores. É rico em vitaminas A e B, cálcio, fósforo, ferro. O jiló é consumido ainda verde e cozido, na forma de refogados, saladas frias, farofas e recheios de tortas. É muito apreciado frito à milanesa, o que elimina ou reduz o amargor.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015; EMBRAPA, Hortaliças na Web.',
'Mostarda-de-folha' => 'As sementes também são consumidas na forma de pasta e têm valor agregado bem elevado no mercado de temperos. As folhas têm vida pós-colheita bastante curta e devem ser rapidamente utilizadas. A mostarda é fonte de vitaminas C e A, de cálcio, além de teores moderados de ferro, sódio, potássio e magnésio. Suas folhas podem ser consumidas cruas ou refogadas. As folhas novas são macias, tenras, têm o sabor mais suave e podem ser consumidas em saladas cruas e sanduíches. As folhas mais desenvolvidas têm o sabor mais picante e podem ser usadas em pratos que passem por cozimento como refogados, cozidos, farofas, tortas, pães.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Ora-pro-nóbis' => 'Apresenta característica mucilaginosa, aproveitada em sopas, refogados, mistos, mexidos e omeletes. Em algumas regiões, é comum usá-la misturada ao feijão. As folhas são utilizadas na culinária mineira, refogados, em substituição a outras hortaliças folhosas, ou combinando-as com aves, como o tradicional prato “frango com ora-pro-nóbis”, muito consumido em cidades históricas como Tiradentes e Sabará.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Rúcula' => 'É rica em sais minerais, como ferrro, cálcio e fósforo, e em vitaminas A e C. É apreciada pelo sabor picante e cheiro acentuado. Muito apreciada na forma de saladas, é também utilizada no preparo de tortas, quiches, pizzas e lasanhas. Pode substituir ou ser misturada ao agrião, alface e chicória. A mistura com folhas de sabor mais suave, como a alface, é especialmente indicada para as pessoas que consideram o sabor da rúcula muito acentuado. É vendida em maços ou já embalada e higienizada, pronta para o consumo.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Vagem' => 'A vagem é rica em fibras, tem apreciável quantidade de vitaminas B1 e B2, além de teores razoáveis de fósforo, flúor, potássio, cálcio, ferro e vitaminas A e C. Para preservar as vitaminas, a cor e o sabor, é importante cozinhar com pouca água somente pelo tempo necessário, preferencialmente no vapor, para deixá-las macias, porém crocantes. Vagens são muito saborosas em saladas, cozidos, empanados, tortas, sopas e farofas.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Banana' => 'Considerada por muitos a fruta perfeita, possui muitas qualidades: amadurece aos poucos, fora do pé, facilitando a colheita; é de fácil transporte; tem bom aproveitamento; e pode ser encontrada durante o ano inteiro. A maioria das bananas-verdes, é possível produzir farinha, que tem aplicações na alimentação, desde o preparo de mingaus até biscoitos. No processo de amadurecimento, a maior parte do amido contido nas bananas transforma-se em açúcar (glicose e sacarose)

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Figo' => 'O figo é consumido fresco ou industrializado na forma de doces em compota ou em pasta e figo rami, uma espécie de passa de figo. De acordo com a destinação futura, os frutos das figueiras devem ser colhidos em diferentes estágios de maturação. Os figos verdes se destinam basicamente à industrialização de doces em compotas; os inchados, à produção do figo rami; e os maduros, à produção de doces em pasta ou ao consumo in natura.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Maçã' => 'No Brasil, a produção de maçã se concentra em dois cultivares, gala e fuji, que representam em torno de 90% da área plantada. A maçã tem como principal destino o consumo fresco. Diversos tipos de processamento da fruta são possíveis, produzindo produtos como doces, geleias, compotas, sucos, bebidas e vinagre. O vinho de maçã, além do consumo direto, constitui a base para a sidra.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Morango' => 'A maior parte do morango produzido é consumida in natura pela população. É também bastante utilizado na fabricação de iogurtes, sorvetes, geleias, em recheios e coberturas para a indústria de panificação, além de sucos e néctares. O morango é rico em vitamina C, é boa fonte de fibras dietéticas, folato e potássio. Adicionalmente é pobre em calorias. Escolha frutos completamente maduros, mas firmes, com cor vermelha uniforme e brilhante. Os frutos menores geralmente têm sabor e aroma mais intenso do que os frutos graúdos.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015; EMBRAPA, Hortaliças na Web.',
'Nectarina' => 'Na composição da nectarina, são encontrados vários nutrientes importantes para a saúde humana. A fruta é rica em vitamina A, niacina (vitamina do complexo B) e, em menor quantidade, vitaminas C, K, B5, ferro e pectina, que ajuda a controlar os níveis de colesterol do sangue. A nectarina consumida in natura é valorizada no mercado como um tipo de fruta das mais finas. Também pode ser utilizada para elaborar doces e sorvetes.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Pêssego' => 'Os pêssegos são bastante apreciados no mundo inteiro pelo sabor, aparência e valor econômico, sendo que os frutos são consumidos frescos ou processados na forma de compotas e doces. Em função das características naturais, podem ter usos e destinos distintos, o consumo in natura ou processados. A vida útil do pêssego fresco é pequena, em torno de dois a cinco dias, quando submetido a condições naturais, sem refrigeração.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Tangerina' => 'As tangerinas constituem um grupo de frutas cítricas e ricas em vitamina C. São usualmente consumidas ao natural e também em forma de suco. A facilidade do descascamento e o aroma típico dessa fruta são os maiores atrativos para o consumo.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Uva' => 'A uva é consumida na sua forma in natura. Com ela são produzidos vinhos de mesa, vinhos finos, sucos, compotas, geleias e sorvetes. No Brasil, o Rio Grande do Sul é o principal produtor de uvas para processamento, a maior produção ocorre na região da serra gaúcha.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Almeirão' => 'O almeirão é uma hortaliça do tipo folha, de sabor amargo. Fornece vitaminas A, C e do complexo B, além de ser boa fonte de fósforo e ferro. Pode substituir hortaliças como a couve, o espinafre e a chicória no preparo de pratos quentes ou em saladas. Também pode ser preparado com feijão, arroz, grão-de-bico, soja, lentilha e como recheio de bolinhos, tortas e sanduíches.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Beterraba' => 'Hortaliça bastante consumida no Brasil, cuja parte tuberosa tem sabor adocicado. A raiz deve ser preferencialmente consumida crua e ralada, na forma de salada ou em sucos. Também pode ser consumida cozida, em sopas, em sucos e no preparo de bolos e suflês. As folhas também podem ser consumidas, refogadas como couve ou em sopas, omeletes e bolinhos. Destaca-se como uma das hortaliças mais ricas em ferro, tanto na raiz quanto nas folhas.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015; EMBRAPA, Hortaliças na Web.',
'Repolho' => 'Sua conservação é relativamente boa. Sob refrigeração, pode ser mantido por várias semanas. É consumido cru, em saladas, ou cozido, em sopas, refogados, acompanhando carnes e diversos pratos orientais. Pode também ser fermentado, para preparação do chucrute, um prato alemão. O repolho é uma hortaliça de cabeça formada pela sobreposição de folhas. Destaca-se como fonte de vitamina C. Também fornece vitaminas B1, B2, E e K, além de sais minerais sobretudo cálcio e fósforo. O repolho pode apresentar folhas lisas de cor verde ou roxa, ou folhas crespas de cor verde.

Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015; EMBRAPA, Hortaliças na Web.',
'Tomate' => 'É uma das hortaliças mais consumidas no mundo, sendo fonte de vitaminas A e C e de sais minerais, como o potássio. São consumidos crus, na salada, ou na preparação de molhos prontos e caseiros, de extrato (polpa concentrada) e, ainda, na forma de doces e sucos.


Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Batata doce' => 'Tem alto valor energético, ou seja, é rica em carboidratos. Também fornece quantidades razoáveis de vitaminas A e C e algumas do complexo B. É mais calórica que a batata-inglesa devido ao seu menor teor de água. As batatas de polpa alaranjada são excelentes fontes de vitamina A devido ao teor de carotenoides, que pode ser mais elevado que o da cenoura. São consumidas cozidas, assadas ou fritas. A polpa é utilizada em diversos pratos, como purês, doces, bolos e cremes. As folhas ou brotos podem ser consumidos refogados, empanados ou em sopas. O sabor lembra o espinafre. A batata-doce pode ser cozida também com casca.


Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',
'Batata inglesa' => 'É uma hortaliça do tipo tubérculo, considerada a terceira fonte de alimento para a humanidade, sendo superada apenas pelo arroz e trigo. A batata é pobre em gordura e rica em carboidratos. É fonte importante de fósforo, vitaminas do grupo B, e se destaca como fonte de vitamina C entre os alimentos básicos. Por ser uma hortaliça muito versátil, pode ser utilizada em uma infinidade de pratos, como acompanhamento de qualquer tipo de carne, ave ou peixe ou substituindo o arroz e o macarrão. Pode ser consumida cozida, assada e frita; mas esta última, apesar de ser a maneira mais popular, não é a opção mais saudável, em função da perda de nutrientes e do excesso de gordura associados a esse prato.


Fonte: Alimentos Regionais Brasileiros, Ministério da Saúde, 2015.',


);

foreach ($alimentos as $alimento => $desc) {
	wp_insert_term( $alimento , 'alimento', array('description' =>  $desc));
}