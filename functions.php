<?php
define('BASE_DIR', get_stylesheet_directory());
define('BASE_URI', get_stylesheet_directory_uri());
define('ADD_PLACE_SLUG', get_post_field( 'post_name', get_post(83) ));
define('ABOUT_SLUG', get_post_field( 'post_name', get_post(2) ));
define('NOT_FOUND_SLUG', get_post_field( 'post_name', get_post(6460) ));
define('CONTACT_SLUG', 'contato');
define('COMIDADEVERDADE', true); // activate comidadeverdade functionalities
define('COMIDADEVERDADE_SLUG', 'comidadeverdade');
define('DAYS_CACHED', 15);
define('ARCHIVE_DAYS_CACHED', 1);
define('VERSION_FOR_CACHE', '20200519001');

/*
if (COMIDADEVERDADE) {
    register_activation_hook( __FILE__, 'idec_activation' );
    add_action( 'idec_hourly_action', 'idec_update_comidadeverdade' );
    function idec_activation() {
    wp_schedule_event( time(), 'hourly', 'idec_hourly_event' );
    }
    function idec_update_comidadeverdade() {
    $sheet_id = "1UVcu0Z6BY5hrER3fTXMpM5fS5JiUAq2qug1_1XFFg2c";
    $url = "https://spreadsheets.google.com/feeds/cells/$sheet_id/2/public/full?alt=json";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$url);
    $dados=curl_exec($ch);
    curl_close($ch);
    file_put_contents(BASE_URI.'/js/idec_comidadeverdade.json', $dados);
    }
    register_deactivation_hook( __FILE__, 'idec_deactivation' );
    function my_deactivation() {
    wp_clear_scheduled_hook( 'idec_hourly_event' );
    }
}
*/
function idec_get_social_media() {
    return array(
        array(
            'class'=>'rrssb-facebook',
            'url'=>'https://www.facebook.com/sharer/sharer.php?u={url}'
        ),
        array(
            'class'=>'rrssb-whatsapp',
            'url'=>'whatsapp://send?text={title} - {url}'
        ),
        array(
            'class'=>'rrssb-twitter',
            'url'=>'https://twitter.com/intent/tweet?text={title} - {url}'
        ),
        array(
            'class'=>'rrssb-tumblr',
            'url'=>'https://tumblr.com/share/link?url={url}'
        ),
        array(
            'class'=>'rrssb-googleplus',
            'url'=>'https://plus.google.com/share?url={url}'
        )
    );
}

$biblioteca_item = (object) array();
$biblioteca_items = (object) array();
$biblioteca_collections = array();
$biblioteca_collection = (object) array();
$biblioteca_item_types = array();
$biblioteca_item_type = (object) array();
$biblioteca_mixed_archives = array();
$biblioteca_mixed_archive = (object) array();

$receita_items = array();
$receita_taxonomias = array('alimento'=>array(),'tipo-de-receita'=>array());

$this_page_headers = (object) array();
$this_page_url = "";
$page_type_homeurl = "";
$page_type_hometitle = "";
$pageLocation = (object) array();

$receita_taxonomy_query = "";

// include specific functions:
require("functions-map.php");
require("functions-receita.php");
require("functions-biblioteca.php");

add_action('after_setup_theme', 'idec_setup_theme');
add_action('wp', 'idec_init');
add_action('wp_enqueue_scripts', 'idec_enqueue_scripts');
add_action('admin_enqueue_scripts', 'idec_admin_scripts');
add_action('wp_ajax_get_markers', 'ajax_get_markers');
add_action('wp_ajax_nopriv_get_markers', 'ajax_get_markers');
add_action('wp_ajax_get_brazil_cities', 'fn_get_brazil_cities');
add_action('wp_ajax_nopriv_get_brazil_cities', 'fn_get_brazil_cities');
add_action('wp_ajax_show_item_data_in_form', 'show_item_data_in_form');
add_action('wp_ajax_nopriv_show_item_data_in_form', 'show_item_data_in_form');

add_filter( 'wpcf7_form_elements', 'mycustom_wpcf7_form_elements');
add_filter('comment_form_defaults','idec_comments_form_defaults');
add_filter('comment_form_default_fields','idec_comments_form_fields');

add_shortcode('checkbox_produtos', 'fn_checkbox_produtos');
add_shortcode('radio_faixaetaria', 'fn_radio_faixaetaria');
add_shortcode('radio_regiao', 'fn_radio_regiao');
add_shortcode('radio_sexo', 'fn_radio_sexo');
add_shortcode('radio_escolaridade', 'fn_radio_escolaridade');
add_shortcode('radio_onde_ocorre_a_feira', 'fn_radio_onde_ocorre_a_feira');
add_shortcode('radio_participacao_na_feira', 'fn_radio_participacao_na_feira');

add_shortcode('select_states', 'fn_select_states');
add_shortcode('opening_days', 'fn_opening_days');
add_shortcode('day', 'fn_opening_day');
add_shortcode('get_edit_id', 'fn_get_edit_id');
add_shortcode('get_url', 'fn_get_url');

add_shortcode('idec_parceiros', 'fn_idec_parceiros');

add_post_type_support('page', 'excerpt');

require_once('includes/handle-forms.php');

function idec_setup_theme(){
	register_nav_menu('idec_main_navigation', 'Navegação Principal');
	register_nav_menu('idec_institutional_navigation', 'Navegação Secundária');
	add_filter('show_admin_bar', '__return_false');
}

function idec_init() {
	// Set Global variables:
    define('IS_MAP', (is_front_page() || is_tax(array('uf', 'cidade')) || is_search() || is_singular('item')));
    define('IS_SINGLE', (is_singular(array('item', 'receita')) OR isset($_REQUEST['id'])));
    if (is_post_type_archive('receita') OR is_tax(array('alimento', 'tipo-de-receita')) OR is_singular('receita')) {
        $page_type = 'receita';
    } else if (is_page('biblioteca')) {
        $page_type = 'biblioteca';
    } else if (IS_MAP || is_page()) {
        $page_type = 'map';
    } else if (is_404()) {
        $page_type = 'map';
    } else {
        $page_type = false;
    }
    define('PAGE_TYPE', $page_type);
}

function idec_enqueue_scripts() {
	if(!is_admin()) {
        wp_enqueue_style( 'w3', BASE_URI.'/includes/w3.css', null, VERSION_FOR_CACHE);
	    wp_enqueue_style( 'pure', BASE_URI.'/purecss/pure-min.css');
		wp_enqueue_style( 'grids', BASE_URI.'/purecss/grids-responsive-min.css');
		wp_enqueue_style( 'rrssb', BASE_URI.'/includes/rrssb/css/rrssb.css');
		wp_enqueue_style( 'icones_mapa_feiras', BASE_URI.'/css/icones_mapa_feiras.css', null, VERSION_FOR_CACHE);
        wp_enqueue_style( 'awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
        wp_enqueue_style( 'comments', BASE_URI.'/css/comments.css');

		wp_enqueue_script('fancybox', BASE_URI.'/js/fancybox/jquery.fancybox.pack.js', array('jquery'), '', true );
		wp_enqueue_script('jqueryui', BASE_URI.'/js/jquery-ui-1.11.2.custom/jquery-ui.min.js', array('jquery'), '', true );
		wp_enqueue_script('jqueryui-touch', BASE_URI.'/js/jquery.ui.touch-punch.min.js', array('jquery', 'jqueryui'), '', true );
		wp_enqueue_script('selectboxit', BASE_URI.'/js/jquery.selectBoxIt.min.js', array('jquery', 'jqueryui'), '', true );
		wp_enqueue_script('idec', BASE_URI.'/js/idec.js', array('jquery', 'jqueryui'), VERSION_FOR_CACHE, true );
		wp_enqueue_script('rrssbJS', BASE_URI.'/includes/rrssb/js/rrssb.min.js', array('jquery'));

		if(PAGE_TYPE=='map') {
			wp_enqueue_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?key='.GOOGLEMAPS_API_KEY.'&libraries=places', '', '', false);
			//wp_enqueue_script('markerClusterer', BASE_URI.'/js/markerclusterer.js', array('jquery', 'googlemaps'), '', false);
            wp_enqueue_script('leaflet', BASE_URI.'/includes/leaflet/leaflet.js', array('jquery'), '', false);
            wp_enqueue_style('leaflet-css', BASE_URI.'/includes/leaflet/leaflet.css');
            wp_enqueue_script('leaflet-markercluster', BASE_URI. '/includes/leaflet-markercluster-1.4.1/leaflet.markercluster.js', array('jquery', 'leaflet'), '', false);
            wp_enqueue_style('leaflet-markercluster-css', BASE_URI. '/includes/leaflet-markercluster-1.4.1/MarkerCluster.css');
            wp_enqueue_style('leaflet-markercluster-default-css', BASE_URI. '/includes/leaflet-markercluster-1.4.1/MarkerCluster.IDEC.css');
            wp_enqueue_script('leaflet-geosearch', BASE_URI.'/includes/leaflet-geosearch.min.js', array('jquery'), '', false);
			wp_enqueue_script('cycle2', BASE_URI.'/js/jquery.cycle2.min.js', array('jquery'), '', true );
			wp_enqueue_script('idec-home', BASE_URI.'/js/idec_home.js', array('jquery','jqueryui', 'leaflet', 'selectboxit','cycle2'), VERSION_FOR_CACHE, true );
			wp_enqueue_script('ajaxcomments', BASE_URI.'/js/ajaxcomments.js', array('jquery'), VERSION_FOR_CACHE);
			wp_enqueue_script('comment-reply');
            wp_enqueue_script('chartjs', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js');
            if (COMIDADEVERDADE) {
                wp_enqueue_script('datatables', 'https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js');
                wp_enqueue_script('datatables-buttons', 'https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js');
                wp_enqueue_style( 'datatables-css', 'https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css');
                wp_enqueue_style( 'datatables-buttons-css', 'https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css');
                wp_enqueue_style( 'datatables-responsive-css', 'https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css');
            }
		}

		if(PAGE_TYPE=='map') {
			wp_enqueue_script('gmap3', BASE_URI.'/js/gmap3.min.js', array('jquery', 'leaflet'), '', true );
			wp_enqueue_script('idec_frontend_form_map', BASE_URI.'/js/idec_frontend_form_map.js', array('jquery', 'leaflet'), VERSION_FOR_CACHE );
			wp_enqueue_script('jquery_mask', BASE_URI.'/js/jquery.mask.min.js', array('jquery') );
			wp_enqueue_script('idec_form', BASE_URI.'/js/idec_form.js', array('jquery','leaflet','fancybox','jqueryui' ,'idec_frontend_form_map'), VERSION_FOR_CACHE, true );
            if (COMIDADEVERDADE) {
                wp_enqueue_script('idec_comidadeverdade', BASE_URI.'/js/idec_comidadeverdade.js', array('jquery'), VERSION_FOR_CACHE, true );
            }
		}

		if(PAGE_TYPE=='receita') {
			wp_enqueue_script('receita', BASE_URI.'/js/idec_receita.js', array('jquery'), VERSION_FOR_CACHE, true );
		}

		if(is_single()) {
			wp_enqueue_script('selectboxit', BASE_URI.'/js/jquery.selectBoxIt.min.js', array('jquery', 'jqueryui'), false, true );
		}
    wp_enqueue_style( 'style_eita', BASE_URI.'/style_eita.css', null, VERSION_FOR_CACHE);
	}
}

function idec_admin_scripts($hook) {
    if ( ! in_array( $hook, array( 'post-new.php', 'post.php' ) ) )	return;
	wp_enqueue_script('maps', 'https://maps.googleapis.com/maps/api/js?key='.GOOGLEMAPS_API_KEY, '', '', true);
	wp_enqueue_script('gmap3', BASE_URI.'/js/gmap3.min.js', array('jquery', 'maps'), '', true );
    wp_enqueue_script('idec_admin', BASE_URI.'/js/idec_admin.js', array('jquery', 'maps', 'gmap3'), VERSION_FOR_CACHE );
}

function idec_get_page_type_label($preposition = false, $isTitle = false) {
    switch (PAGE_TYPE) {
        case 'map':
            $ret = ($preposition) ? $preposition.'o mapa' : 'mapa';
            break;
        case 'receita':
            $ret = ($preposition) ? $preposition.'as receitas' : 'receitas';
            break;
        case 'biblioteca':
            $ret = ($preposition) ? $preposition.'a biblioteca' : 'biblioteca';
            break;
        case 'page':
        default:
            $ret = ($preposition) ? $preposition.'a página' : 'página';
            break;
    }
    if ($isTitle && !$preposition) {
        $ret = strtoupper(substr($ret, 0, 1)).substr($ret, 1);
    }
    return $ret;
}

function idec_generate_this_page_headers() {
    global $this_page_headers;
    if (get_object_vars($this_page_headers)) {
        return $this_page_headers;
    }
    $pL = idec_pageLocation();

    $this_page_headers = idec_generate_page_headers(($pL->page_type) ? $pL->page_type : PAGE_TYPE, $pL->type, $pL->slug);

    return $this_page_headers;
}

function idec_generate_page_headers($page_type=PAGE_TYPE, $item_type="", $idOrLabel) {
    if(!$page_type) {
        $title = "Página não encontrada";
        $description = "O conteúdo que você está buscando não foi encontrado. Será que é um link antigo?";
    } else {
        switch($page_type."_".$item_type) {
            case 'map_item':
                $title = get_the_title($idOrLabel) . " | " . get_bloginfo('title');
                $description = get_post($idOrLabel)->post_content;
                break;
            case 'map_':
            case 'map_global':
                $title = get_bloginfo('title');
                $description = get_bloginfo('description' );
                break;
            case 'map_cidade':
            case 'map_uf':
                if ($item_type=='uf') {
                    $extra = "no Estado ";
                    $localtxt = "do Estado ";
                    $do = false;
                } else {
                    $localtxt = "da cidade ";
                    $do = true;
                }
                $term = get_term_by('slug', $idOrLabel, $item_type);
                $localityData = idec_get_locality_from_term($item_type, $term);
                $title = "Feiras orgânicas $extra".idec_get_location_preposition($localityData->label, $do);
                $description = "Encontre as feiras orgânicas e demais iniciativas $localtxt".idec_get_location_preposition($localityData->label, false)."!! ".get_bloginfo('description');
                break;
            case 'biblioteca_archive':
            case 'biblioteca_':
                $title = 'Biblioteca | '.get_bloginfo('name');
                $description = 'A biblioteca do Mapa de Feiras Orgânicas reúne um acervo de conteúdo digital que aborda diferentes temas ligados à agroecologia, à produção orgânica e à alimentação saudável.</p><p>Esta seção é resultado de uma parceria com o <a target=\'_blank\' href=\'https://biblioteca.consumoresponsavel.org.br\'>Portal do Consumo Responsável</a>.</p>';
                break;
            case 'biblioteca_item':
                $items = idec_biblioteca_get_items();
                foreach($items->data as $item) {
                    if($item->id==$idOrLabel) {
                        $title = $item->title." | Biblioteca do ".get_bloginfo('name');
                        $description = $item->description;
                        break;
                    }
                }
                break;
            case 'biblioteca_collection':
                $collection = idec_biblioteca_get_single_collection($idOrLabel);
                $title = 'Tema '.$collection->title.' | Biblioteca do '.get_bloginfo('name');
                $description = $collection->description;
                if (!$description) {
                    $description = "Aprofunde seus conhecimentos sobre o tema '".idec_biblioteca_get_single_collection($idOrLabel)->title."' na Biblioteca do Mapa de Feiras Orgânicas do IDEC.";
                }
                break;
            case 'biblioteca_item_type':
                $item_type = idec_biblioteca_get_single_item_type($idOrLabel);
                $title = $item_type->title." | Biblioteca do ".get_bloginfo('name');
                $description = $item_type->description;
                if (!$description) {
                    $description = "Explore conteúdos do tipo '".idec_biblioteca_get_single_item_type($idOrLabel)->title."' na Biblioteca do Mapa de Feiras Orgânicas do IDEC.";
                }
                break;
            case 'biblioteca_mixed_archive':
                $mixed_archive = idec_biblioteca_get_single_mixed_archive($idOrLabel);
                $title = $mixed_archive->title.' | Biblioteca do '.get_bloginfo('name');
                $description = $mixed_archive->description;
                if (!$description) {
                    $description = "Aprofunde seus conhecimentos!";
                }
                break;
            case 'receita_item':
                $title = get_the_title($idOrLabel) . " | Receita do " . get_bloginfo('title');
                $description = get_post($idOrLabel)->post_content;
                if (!$description) {
                    $description = "Confira esta e outras deliciosas receitas elaboradas com alimentos orgânicos disponíveis em feiras e demais locais de comercialização do Mapa de Feiras Orgânicas do IDEC.";
                }
                break;
            case 'receita_alimento':
                $term = get_term_by('slug', $idOrLabel, 'alimento');
                $title = "Receitas com ".$term->name." | ".get_bloginfo('title');
                $description = $term->description;
                if (!$description) {
                    $description = "Descubra deliciosas receitas com ".$term->name.", disponíveis no Mapa de Feiras Orgânicas do IDEC. São inúmeras possibilidades de se aproveitar este alimento orgânico que você encontra nas feiras e demais pontos de comercialização do site.";
                }
                break;
            case 'receita_tipo-de-receita':
                $term = get_term_by('slug', $idOrLabel, 'tipo-de-receita');
                $title = $term->name." | Receitas do ".get_bloginfo('title');
                $description = $term->description;
                if(!$description) {
                    $description = "Descubra deliciosas receitas do tipo '".$term->name."' disponíveis no Mapa de Feiras Orgânicas do IDEC. São inúmeras possibilidades de se usar os produtos orgânicos que você encontra nas feiras e demais pontos de comercialização do site.";
                }
                break;
            case 'receita_archive':
            case 'receita_':
                $title = "Receitas | ".get_bloginfo('title');
                $description = "Confira as receitas e as inúmeras opções de pratos que podem ser feitos com os alimentos orgânicos adquiridos nas feiras e nos demais locais de comercialização destes produtos.";
                break;
            case 'page_page':
                $page = get_page_by_path($idOrLabel, OBJECT, 'page');
                $title = $page->post_title.' | '.get_bloginfo('title');
                $text = $page->post_content;
                $description = wp_trim_words($text, 55, '...');
                break;
        }
    }
    return (object) array(
        'title'=>html_entity_decode(str_replace('"',"'",$title), ENT_NOQUOTES, 'UTF-8'),
        'description'=>html_entity_decode(str_replace('"',"'",$description), ENT_NOQUOTES, 'UTF-8')
    );
}

function idec_generate_this_url() {
    global $this_page_url;
    if ($this_page_url) {
        return $this_page_url;
    }

    $this_page_url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    return $this_page_url;
}

function idec_get_page_type_homeurl() {
    global $page_type_homeurl;
    if ($page_type_homeurl) {
        return $page_type_homeurl;
    }
    switch(PAGE_TYPE) {
        case 'biblioteca':
        case 'receita':
            $page_type_homeurl = get_bloginfo('url').'/'.idec_get_page_type_label().'/';
            break;
        case 'map':
        default:
            $page_type_homeurl = get_bloginfo('url');
            break;
    }
    return $page_type_homeurl;
}

function idec_get_page_type_hometitle() {
    global $page_type_hometitle;
    if ($page_type_hometitle) {
        return $page_type_hometitle;
    }
    switch(PAGE_TYPE) {
        case 'biblioteca':
        case 'receita':
            $page_type_hometitle = idec_get_page_type_label("", true)." | ".get_bloginfo('name');
            break;
        case 'map':
        default:
            $page_type_hometitle = get_bloginfo('name');
            break;
    }
    return $page_type_hometitle;
}

add_action('wp_ajax_nopriv_idec_show_item_content', 'idec_show_item_content' );
add_action('wp_ajax_idec_show_item_content', 'idec_show_item_content' );
function idec_show_item_content() {
    global $pageLocation;
    define('PAGE_TYPE', $_POST['page_type']);
	$item_type = $_POST['item_type'];
	$item_id = $_POST['item_id'];
	$paged = (isset($_POST['paged'])) ? $_POST['paged'] : 1;

	ob_start();

	if (PAGE_TYPE=='page') {
	    global $post;
	    $post = get_page_by_path($item_id);
	    setup_postdata( $post );
	    get_template_part('content');
	    wp_reset_postdata();
	} else if (PAGE_TYPE=='biblioteca') {
	    switch($item_type) {
	        case 'item':
	            $item = idec_biblioteca_get_single_item($item_id);
	            if(!$item) {
	                get_template_part('archive', 'biblioteca-content');
	            } else {
	                $name = $item->title;
    	            get_template_part('single', 'biblioteca-content');
	            }
	            break;
            case 'collection':
            case 'item_type':
            case 'mixed_archive':
                $href = idec_get_page_type_homeurl().'?'.$item_type.'='.$item_id;
                $item = idec_biblioteca_get_single_category($item_type, $item_id);
	            $name = $item->title;
	            idec_pageLocation($item_type, $item_id, $href, '', $item->title, '0', PAGE_TYPE, $paged);
	            get_template_part('single', $item_type.'-content');
                break;
            case 'archive':
                idec_pageLocation($item_type, $item_id, idec_get_page_type_homeurl(), '', '', '0', PAGE_TYPE, $paged);
                get_template_part('archive', 'biblioteca-content');
                break;
	    }
	} elseif ($item_type=='item') {
	    global $post;
	    $post = get_post( $item_id );
	    $name = $post->post_title;
	    setup_postdata( $post );
	    $page_type_template = (PAGE_TYPE=='map') ? 'item' : PAGE_TYPE;
	    get_template_part('single', $page_type_template."-content");
	    wp_reset_postdata();
	} elseif ($item_type=='archive') {
	    idec_set_main_query_to_archive(PAGE_TYPE, $paged, get_option('posts_per_page'));
	    get_template_part('archive', PAGE_TYPE.'-content');
	    $name = idec_get_page_type_label(false, true);
	} else {
	    // This is a taxonomy:
	    $pL = idec_pageLocation($item_type, $item_id, get_term_link($item_id, $item_type), $term->name, $term->name, '0', PAGE_TYPE, $paged);
	    $term = get_term_by('slug', $item_id, $item_type);
	    $name = $term->name;
	    get_template_part('single', "$item_type-content");
	}
	$html = ob_get_contents();
	ob_end_clean();

	$title = idec_generate_page_headers(PAGE_TYPE, $item_type, $item_id)->title;

    $ret = array('html'=>$html, 'title'=>$title, 'name'=>$name);

    echo json_encode($ret);

	wp_die();
}

// Get or Set pageLocation:
function idec_pageLocation($item_type="", $slug="", $href="", $title="", $name="", $is_overlay="", $page_type=PAGE_TYPE, $paged = 1) {
    global $pageLocation;

    if($item_type) {
        $pageLocation = (object) array(
            'type'=>$item_type,
            'slug'=>$slug,
            'href'=>$href,
            'title'=>$title,
            'name'=>$name,
            'is_overlay'=>$is_overlay,
            'page_type'=>$page_type,
            'paged'=>$paged
        );
    }

    return $pageLocation;
}

// custom taxonomy permalinks
add_filter('post_link', 'cidade_permalink', 10, 3);
add_filter('post_type_link', 'cidade_permalink', 10, 3);
function cidade_permalink($permalink, $post_id, $leavename) {
        if (strpos($permalink, '%cidade%') === FALSE) return $permalink;

        // Get post
        $post = get_post($post_id);
        if (!$post) return $permalink;

        // Get taxonomy terms
        $terms = wp_get_object_terms($post->ID, 'cidade');
        if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $taxonomy_slug = $terms[0]->slug;
        else $taxonomy_slug = 'other';

        return str_replace('%cidade%', $taxonomy_slug, $permalink);
}

// custom active menu item
add_filter( 'nav_menu_css_class', 'idec_custom_active_menu_item', 10, 3 );
function idec_custom_active_menu_item($classes, $item, $args) {
    if($args->theme_location != 'idec_main_navigation') {
        return $classes;
    }
    $title = strip_tags($item->title);
    if(
        (PAGE_TYPE=='map' && $title=='Mapa') ||
        (PAGE_TYPE=='receita' && $title=='Receitas') ||
        (PAGE_TYPE=='biblioteca' && $title=='Biblioteca')
    ) {
        $classes[] = 'current-menu-item';
    }
    return array_unique($classes);
}

function idec_set_main_query_to_archive($page_type, $paged=1, $posts_per_page=-1) {
    global $wp_query, $post;
    $args = array(
        'post_type'=>$page_type,
        'paged'=>$paged,
        'posts_per_page'=>$posts_per_page
    );
    $wp_query = new WP_Query($args);
    setup_postdata( $post );
    return $wp_query;
}

function idec_title_filter( $where, &$wp_query ) {
    global $wpdb;
    if ( $search_term = $wp_query->get( 'title_filter' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
    }
    return $where;
}

function idec_save_template_to_var($template_name, $part_name) {
    ob_start();
    get_template_part($template_name, $part_name);
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}

function idec_get_template_model($template_name, $part_name, $data_parts=array()) {
    $html = idec_save_template_to_var('template-models/'.$template_name, $part_name);
    if($data_parts && is_array($data_parts)) {
        foreach ($data_parts as $key=>$value) {
            $html = str_replace('{'.$key.'}',$value,$html);
        }
    }
    return $html;
}

function idec_get_post_image($id='', $taxonomy='') {
    if ($id) {
        if ($taxonomy) {
            $term = get_term_by('slug', $id, $taxonomy);
            $image_id = get_term_meta( $term->term_id, 'image', true );
            $image = wp_get_attachment_image_src( $image_id, 'full', false );

            if ($image) {
                return $image;
            }
        } else {
            if ( has_post_thumbnail($id) ) {
                return wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'full', false );
            }
            // No post thumbnail, try attachments instead.
            $images = get_posts(
                array(
                    'post_type'      => 'attachment',
                    'post_mime_type' => 'image',
                    'post_parent'    => $id,
                    'posts_per_page' => 1
                )
            );
            if ( $images ) {
                return wp_get_attachment_image_src( $images[0]->ID, 'full' );
            }
        }
    }

    // If no image is found, return the default:
    return array(BASE_URI."/images/logo_feiras_1024x1024_fundo_verde.png",1024,1024);
}

function idec_custom_pagination($numpages = '', $paged='', $href='') {
    switch(PAGE_TYPE) {
        case 'receita':
            if ($numpages == '') {
                global $wp_query;
                $paged = get_query_var('paged');
                $numpages = $wp_query->max_num_pages;
                if(!$numpages) {
                    $numpages = 1;
                }
            }

            if (empty($paged)) {
                $paged = 1;
            }

            $base = ($href) ? $href : idec_get_page_type_homeurl();

            $pagination_args = array(
                'base'            => $base . '%_%',
                'format'          => 'page/%#%/',
                'total'           => $numpages,
                'current'         => $paged,
                'show_all'        => true,
            //  'end_size'        => 1,
            //  'mid_size'        => $pagerange,
                'prev_next'       => false,
            //  'prev_text'       => __( 'Previous page', 'twentysixteen' ),
	        //  'next_text'       => __( 'Next page', 'twentysixteen' ),
            //  'type'            => 'plain',
            //  'add_args'        => false,
            //  'add_fragment'    => ''
            );

            $paginate_links = paginate_links($pagination_args);
            if ($href) {
                $paginate_links = str_replace(admin_url("admin-ajax.php"),$href,$paginate_links);
            }
            break;
        case 'biblioteca':
            $paginate_links = '';
            if ($numpages>1) {
                $href = parse_url($href);
                if(substr($href['path'],-1)=='/') {
                    $href['path'] = substr($href['path'],0,-1);
                }
                $href['query'] = (isset($href['query']))
                    ? '?'.$href['query']
                    : '';
                for ($i=1; $i<$numpages+1; $i++) {
                    $current = ($i==$paged) ? ' current disabled' : '';
                    $paginate_links .= "<a class=\"page-numbers page-numbers-biblioteca".$current."\" href=\"".$href['scheme']."://".$href['host'].$href['path']."/page/".$i."/".$href['query']."\">".$i."</a>&nbsp;";
                }
            }
            break;
    }

    $html = "";
    if ($paginate_links) {
        $html = "
            <nav class=\"navigation pagination\" role=\"navigation\">
	    	    <h2 class=\"screen-reader-text\">Navegação por posts</h2>
	    	    <div class=\"nav-links\">
	    	        ".$paginate_links."
                </div>
	        </nav>";
    }
    return $html;
}

function idec_generate_count_results_text($class, $count, $item, $where, $paged, $pages) {
    $pageTxt = ($pages>1)
        ? ' (página '.$paged.' de '.$pages.')'
        : '';
    return array(
        '<span class="'.$class.'">'.qtty($count, $item).' '.$where.'</span>',
        $pageTxt
    );
}

function qtty($count, $word, $show_count=true) {
    $ret = ($show_count) ? $count.' ' : '';
    return ($count!=1) ? $ret.$word.'s' : $ret.$word;
}

function pR($t) {
    ?>
    <hr>
    <pre>
        <?php print_r($t); ?>
    </pre>
    <?php
}

add_action( 'draft_to_publish', 'send_publish_message' , 10 , 2);

function send_publish_message ( $post ) {

    if (get_post_type($post) !== 'item')
            return;

    global $wpdb;

    $title = "[A iniciativa que você cadastrou foi publicada] O Mapa de Feiras Orgânicas agradece!";

    $conteudo = "Caro (a) " . get_post_meta($post->ID)['wpcf-nome'][0] . ",<br />
        <br />
        Agradecemos sua colaboração!<br />
        <br />
        A iniciativa " . $post->post_title . ", que ocorre em " . wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"))[0] . ", foi publicada no Mapa de Feiras Orgânicas com sucesso. Confira e compartilhe: " . get_permalink( $post->ID ) . "<br />
        <br />
        Você está colaborando para aumentar o acesso dos consumidores aos alimentos orgânicos ou de base agroecológica diretamente do produtor.\n
        <br /><br />
        O Mapa de Feiras Orgânicas é uma ferramenta de busca colaborativa utilizada para melhorar o acesso dos consumidores aos produtos orgânicos e de base agroecológica.<br /><br />
        Gostaríamos de contar com você para futuras atualizações referentes a esta iniciativa, como endereço, horário e dias de funcionamento. Seu apoio é fundamental!<br />
        <br />
        Se souber de informações sobre outras feiras orgânicas de sua região, não deixe de nos contar.
        <br /><br />
        Atenciosamente,<br /><br />
        <img src='" . BASE_URI . "/images/rodape-idec.jpg' alt='Equipe Mapa de Feiras Orgânicas' title='Equipe Mapa de Feiras Orgânicas'/>
        ";
    $headers[] = 'Bcc: Feiras Orgânicas <feirasorganicas@idec.org.br>';
    $headers[] = 'Bcc: Feiras Orgânicas <alan@contraosagrotoxicos.org>';
    $headers[] = 'Content-Type: text/html; charset=UTF-8';

    wp_mail( get_post_meta($post->ID)['wpcf-email'],$title,$conteudo,$headers);
    return $post_ID;
}


function fn_idec_parceiros () {
    return $parceiros = '
        <div class="idec-parceiros2">

        <div style="background-image: url(\'/wp-content/uploads/2014/12/Kairós-300x136.png\')"><a href="http://www.institutokairos.net" target="_blank"></a></div>

        <div style="background-image: url(\'/wp-content/uploads/2017/08/Rede_Brasileira_GCR.jpg\')"><a href="http://institutokairos.net/2013/10/mapeamento-de-grupos-de-consumo-responsavel/" target="_blank"></a></div>

        <div style="background-image: url(\'/wp-content/uploads/2014/12/CSA_Brasil.png\')"><a href="http://www.csabrasil.org" target="_blank"></a></div>

        <div style="background-image: url(\'/wp-content/uploads/2014/12/SEAD.png\')"><a href="http://www.mda.gov.br/sitemda/tags/sead" target="_blank"></a></div>

        <div style="background-image: url(\'/wp-content/uploads/2014/12/MDS.png\')"><a href="http://mds.gov.br/" target="_blank"></a></div>

        <div style="background-image: url(\'/wp-content/uploads/2014/12/Consumo_Responsável-300x137.png\')"><a href="http://consumoresponsavel.org.br" target="_blank"></a></div>

        </div>
    ';
}


add_shortcode ( 'idec_saiba_mais' , 'fn_idec_saiba_mais');
function fn_idec_saiba_mais ( $atts , $content ) {

    $atts = shortcode_atts(
        array(
            'id' => '',
        ),
        $atts,
        'idec_saiba_mais'
    );

    return $saiba_mais = '
        <div class="idec-facaparte-saiba-mais" onclick="showSaibaMais(' . $atts['id'] .',this)">
                            Saiba mais
        </div>
        <div id="saiba-mais-content-' . $atts['id'] .'" style="display: none">
           ' . $content . '
        </div>
    ';
}

add_shortcode ( 'idec_comidadeverdade' , 'fn_idec_comidadeverdade');
function fn_idec_comidadeverdade ( $atts ) {
    $html = '<div id="idec_comidadeverdade_loading">Carregando dados <img src="'.BASE_URI.'/images/loader3.gif" /></div>';
    $html .= '<table id="comidadeverdade_table" class="stripe w3-text-black" width="100%"></table>';
    $html .= "<script>var idecPageExcerpt = '" . get_the_excerpt() . "'</script>";
    return $html;
}

function fix_custom_address (){

    global $wpdb;

    $result = $wpdb->get_results("SELECT * FROM `wp_postmeta` WHERE `meta_key` LIKE 'wpcf-address-custom' AND `meta_value` LIKE '%CEP:%';", OBJECT);

    foreach ($result as $r) {
        $address = $r->meta_value;
        $mod_address = preg_replace("/, CEP: \d{5}-\d{3}/i", "", $address);
        $mod_address = preg_replace("/, CEP: \d{5}/i", "", $mod_address);
        echo $mod_address . "<br>";
        echo "UPDATE `wp_postmeta` SET `meta_value` = $mod_address WHERE `meta_id` = " . $r->meta_id;


        $wpdb->get_results("UPDATE `wp_postmeta` SET `meta_value` = '$mod_address' WHERE `meta_id` = " . $r->meta_id, OBJECT);

    }
}

add_shortcode ( 'idec_estatisticas' , 'fn_idec_estatisticas');
function fn_idec_estatisticas ( $atts , $content ) {

    $atts = shortcode_atts(
        array(
            'id' => '',
        ),
        $atts,
        'idec_estatisticas'
    );

    # get stats
    # totais por categoria
    $tipos = array(
      'Feira Orgânica ou Agroecológica' => 'Feiras Orgânicas ou Agroecológicas',
      'Comércio Parceiro de Orgânicos' => 'Comércios Parceiros de Orgânicos',
      'Grupo de Consumo Responsável' => 'Grupos de Consumo Responsável'
    );

    $regioes = array(
      'Centro-Oeste' => 'Centro-Oeste',
      'Nordeste' => 'Nordeste',
    	'Norte' => 'Norte',
      'Sudeste' => 'Sudeste',
      'Sul' => 'Sul',
    );

    $estados = array(
      'AC' => 'Acre',
      'AL' => 'Alagoas',
      'AP' => 'Amapá',
      'AM' => 'Amazonas',
      'BA' => 'Bahia',
      'CE' => 'Ceará',
      'DF' => 'Distrito Federal',
      'ES' => 'Espírito Santo',
      'GO' => 'Goiás',
      'MA' => 'Maranhão',
      'MT' => 'Mato Grosso',
      'MS' => 'Mato Grosso do Sul',
      'MG' => 'Minas Gerais',
      'PA' => 'Pará',
      'PB' => 'Paraíba',
      'PR' => 'Paraná',
      'PE' => 'Pernambuco',
      'PI' => 'Piauí',
      'RJ' => 'Rio de Janeiro',
      'RN' => 'Rio Grande do Norte',
      'RS' => 'Rio Grande do Sul',
      'RO' => 'Rondônia',
      'RR' => 'Roraima',
      'SC' => 'Santa Catarina',
      'SP' => 'São Paulo',
      'SE' => 'Sergipe',
      'TO' => 'Tocantins',
    );

    $capitais = array(
      'Aracaju - SE' => 'Aracaju - SE',
      'Belém - PA' => 'Belém - PA',
      'Belo Horizonte - MG' => 'Belo Horizonte - MG',
      'Boa Vista - RR' => 'Boa Vista - RR',
      'Brasília - DF' => 'Brasília - DF',
      'Campo Grande - MS' => 'Campo Grande - MS',
      'Cuiabá - MT' => 'Cuiabá - MT',
      'Curitiba - PR' => 'Curitiba - PR',
      'Florianópolis - SC' => 'Florianópolis - SC',
      'Fortaleza - CE' => 'Fortaleza - CE',
      'Goiânia - GO' => 'Goiânia - GO',
      'João Pessoa - PB' => 'João Pessoa - PB',
      'Macapá - AP' => 'Macapá - AP',
      'Maceió - AL' => 'Maceió - AL',
      'Manaus - AM' => 'Manaus - AM',
      'Natal - RN' => 'Natal - RN',
      'Palmas - TO' => 'Palmas - TO',
      'Porto Alegre - RS' => 'Porto Alegre - RS',
      'Porto Velho - RO' => 'Porto Velho - RO',
      'Recife - PE' => 'Recife - PE',
      'Rio Branco - AC' => 'Rio Branco - AC',
      'Rio de Janeiro - RJ' => 'Rio de Janeiro - RJ',
      'Salvador - BA' => 'Salvador - BA',
      'São Luís - MA' => 'São Luís - MA',
      'São Paulo - SP' => 'São Paulo - SP',
      'Teresina - PI' => 'Teresina - PI',
      'Vitória - ES' => 'Vitória - ES',
    );

    $report = gen_report_table('Geral', $tipos, 'tipo');
    $report .= gen_report_table_2d('Por regiões', $regioes, 'regiao', $tipos, 'tipo');
    $report .= gen_report_table_2d('Por estados', $estados, 'uf', $tipos, 'tipo');
    $report .= gen_report_table_2d('Por capitais', $capitais, 'cidade', $tipos, 'tipo');
    $report .= gen_time_graph("Evolução dos registros");

    return "
        <div class=''>
          $report
        </div>
    ";
}

function gen_report_table($title, $data, $tax){

  $report = "<h2>$title</h2>";
  $report .= "<table class='report'>";

  # table content
  $report .= "<tr>";
  foreach ($data as $name => $d) {
    $args = array(
        'post_type'=>'item',
        'posts_per_page'=>-1,
        'post_status' => 'publish',
        'tax_query'=>array(
            array(
              'relation' => 'AND',
              array(
                  'taxonomy' => $tax,
                  'field' => 'name',
                  'terms' => $name
              ),
            )
        )
    );
    $r = new WP_Query($args);
    $v = ($r->post_count > 0) ? $r->post_count : "-";
    $report .= "<td>$v</td>";
  }
  $report .= "</tr>";

  $report .= "<tr>";
  foreach ($data as $d) {
    $report .= "<th>$d</th>";
  }
  $report .= "</tr>";
  $report .= "</table>";
  return $report;
}

function gen_report_table_2d($title, $data1, $tax1, $data2, $tax2){

  # table header
  $report = "<h2>$title</h2>";
  $report .= "<table class='report_2d'><tr><th></th>";
  foreach ($data2 as $d2) {
    $report .= "<th>$d2</th>";
  }
  $report .= "</tr>";

  # table content
  foreach ($data1 as $name1 => $d1) {
    $report .= "<tr><td>$d1</td>";
    foreach ($data2 as $name2 => $d2) {
      $args = array(
          'post_type'=>'item',
          'posts_per_page'=>-1,
          'post_status' => 'publish',
          'tax_query'=>array(
              array(
                'relation' => 'AND',
                array(
                    'taxonomy' => $tax1,
                    'field' => 'name',
                    'terms' => $name1
                ),
                array(
                    'taxonomy' => $tax2,
                    'field' => 'name',
                    'terms' => $name2
                ),
              )
          )
      );
      $r = new WP_Query($args);
      $v = ($r->post_count > 0) ? $r->post_count : "-";
      $report .= "<td>$v</td>";
    }
    $report .= "</tr>";
  }
  $report .= "</table>";
  return $report;
}

function gen_time_graph( $title ){

  global $wpdb;

  $result = $wpdb->get_results("SELECT YEAR(post_date) as 'ano', MONTH(post_date) as 'mes', COUNT(*) as 'total' FROM `wp_posts` WHERE `post_type`='item' and `post_status` = 'publish' GROUP BY YEAR(post_date), MONTH(post_date)");

  $data = NULL;
  $label = NULL;
  $i = 0;
  foreach ($result as $r) {
    $data[$i] = $r->total;
    if ($i > 0) $data[$i] += $data[$i-1];
    $label[] = "'$r->mes/$r->ano'";
    $i++;
  }
  $data = implode(",",$data);
  $label = implode(",",$label);

  $report = "
  <h2>$title</h2>
  <canvas id='myChart' width='400' height='400'></canvas>
  <script>
  var ctx = document.getElementById('myChart').getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: [ $label ],
          datasets: [{
              label: 'Itens registrados',
              data: [ $data ]
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });
  </script>
  ";
  return $report;
}

add_action( 'admin_notices', 'my_error_message' );

add_action( 'pre_post_update', 'custom_post_site_save', 10, 2);
function custom_post_site_save( $post_id , $post_data) {
  # If this is just a revision, don't do anything.
  if (wp_is_post_revision($post_id))
      return;

  if ($post_data['post_type'] == 'item') {
      $latlng = $_POST['wpcf']['latlng'];
      preg_match('/[a-zA-Z]/', $latlng, $output_array);
      if ($output_array != array()) {
          # Add a notification
          update_option('my_notifications', json_encode(array('error', 'O campo Latitude/Longitude precisa estar em formato de coordenadas. Exemplo: (-23.5116272,-47.46440129999996)')));
          # And redirect
          header('Location: '.get_edit_post_link($post_id, 'redirect'));
          exit;
      }
  }
}

function my_error_message () {
  $notifications = get_option('my_notifications');

  if (!empty($notifications)) {
      $notifications = json_decode($notifications);
      #notifications[0] = (string) Type of notification: error, updated or update-nag
      #notifications[1] = (string) Message
      #notifications[2] = (boolean) is_dismissible?
      switch ($notifications[0]) {
          case 'error': # red
          case 'updated': # green
          case 'update-nag': # ?
              $class = $notifications[0];
              break;
          default:
              # Defaults to error just in case
              $class = 'error';
              break;
      }

      $is_dismissable = '';
      if (isset($notifications[2]) && $notifications[2] == true)
          $is_dismissable = 'is_dismissable';

      echo '<div class="'.$class.' notice '.$is_dismissable.'">';
         echo '<p>'.$notifications[1].'</p>';
      echo '</div>';

      # Let's reset the notification
      update_option('my_notifications', false);
  }
}
