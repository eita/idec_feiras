
//////////////////functions

function getMarkersInViewPort(){
	var markersInViewPort = {
	    'items': [],
	    'cities': {},
	    'states': {},
	    'regions': {},
	    'citiesCount': 0,
	    'statesCount': 0
	};
	var bounds=map.getBounds();
	var checkedCategories = getCheckedCategories();
	jQuery.each(markers, function(i, marker) {
		if ((typeof bounds == 'undefined') ||
    (bounds.contains(marker.getLatLng()) &&
    checkedCategories.indexOf(marker.options.type)>-1)) {
			markersInViewPort.items.push(markersRaw[marker.options.mRawPos]);
			if(typeof markersInViewPort.cities[markersRaw[marker.options.mRawPos].city_slug] == 'undefined') {
			    markersInViewPort.cities[markersRaw[marker.options.mRawPos].city_slug] = cities[markersRaw[marker.options.mRawPos].city_slug];
			    markersInViewPort.cities[markersRaw[marker.options.mRawPos].city_slug].countVisibleItems = 0;
			    markersInViewPort.citiesCount++;
			}
			markersInViewPort.cities[markersRaw[marker.options.mRawPos].city_slug].countVisibleItems ++;
			if(typeof markersInViewPort.states[markersRaw[marker.options.mRawPos].uf_slug] == 'undefined') {
			    markersInViewPort.states[markersRaw[marker.options.mRawPos].uf_slug] = states[markersRaw[marker.options.mRawPos].uf_slug];
			    markersInViewPort.states[markersRaw[marker.options.mRawPos].uf_slug].countVisibleItems = 0;
			    markersInViewPort.statesCount++;
			}
			markersInViewPort.states[markersRaw[marker.options.mRawPos].uf_slug].countVisibleItems ++;
			if(typeof markersInViewPort.regions[markersRaw[marker.options.mRawPos].region_slug] == 'undefined') {
			    markersInViewPort.regions[markersRaw[marker.options.mRawPos].region_slug] = regions[markersRaw[marker.options.mRawPos].region_slug];
			    markersInViewPort.regions[markersRaw[marker.options.mRawPos].region_slug].countVisibleItems = 0;
			}
			markersInViewPort.regions[markersRaw[marker.options.mRawPos].region_slug].countVisibleItems ++;
		}
	});
	return markersInViewPort;
}

function updateSidebarWithSearchResults(items, s) {
    var title = 'Busca por "<b>'+s+'</b>"';
    var page_title = 'Resultados da busca por "<b>'+s+'</b>"';
    jQuery("#ajax_search_value").autocomplete( "close" );
    jQuery('#locationStats').html(basicLocationStatsHTML).show();
    jQuery('.locations_stats-header').addClass('lowcase').html(title);
    jQuery('#locationStatsList').html("");
    var itemsCount = 0;
    if (items.length > 0) {
        var bounds = L.latLngBounds();
        for(var i=0;i<items.length;i++) {
            if(items[i].type!='header') {
                itemsCount ++;
            }
            build_search_result_item(searchResults.items[i], 's').appendTo(jQuery('#locationStatsList'), false);
            addItemToBounds(items[i], bounds);
        }
    } else {
        var bounds = globalBounds;
    }
    jQuery('.locations_stats-subheader').html(qtty(itemsCount, 'resultado'));
    change_page_state(home_url, page_title, s, 'search', s, 0, 'map');
    isAutoZooming = true;
    map.fitBounds(bounds);
}

function showVisibleItemsInSidebar(items, isLocation) {
  isLocation = (typeof isLocation != 'undefined' && isLocation);
	//console.log('Entered showVisibleItemsInSidebar');console.log(isLocation);console.log(pageLocation.type);console.log('-------------');
  jQuery('#locationStats').html(basicLocationStatsHTML).show();
  if(isLocation) {
    var label = (typeof cities[pageLocation.slug] == 'undefined')
      ? pageLocation.name
      : cities[pageLocation.slug].label;
    jQuery('#ajax_search_value').val(label);
  } else {
    jQuery('.locations_stats-header').text('O que você vê');
  }

  jQuery('#locationStatsList').html("");
  var n = 0;
	jQuery.each(items, function(i, item){
    if(!isLocation || pageLocation.slug==item.city_slug) {
      n++;
      var html = itemHTML
                  .replace(/{id}/g,item.id)
                  .replace(/{url}/g,item.link)
                  .replace(/{type}/g,item.type)
                  .replace(/{title}/g,item.title)
                  .replace(/{city}/g,cities[item.city_slug].label)
      jQuery('#locationStatsList').append(html);
  	}
	});
	jQuery('.locations_stats-subheader').html(qtty(n,'iniciativa'));
}

function showVisibleItemsStatsInSidebar(markersInViewPort, isLocation) {
  isLocation = (typeof isLocation != 'undefined' && isLocation);
	//console.log('Entered showVisibleItemsStatInSidebar');console.log(isLocation);console.log(pageLocation.type);console.log('---------');
  var title = 'O que você vê';
  if (isLocation && pageLocation.type=='cidade') {
    // Show items in city
    showVisibleItemsInSidebar(markersInViewPort.items, pageLocation.slug);
  } else if (
    (!isLocation && markersInViewPort.citiesCount<=maxSidebarCities) ||
    (isLocation && pageLocation.type=='uf')
  ) {
    jQuery('#locationStats').html(basicLocationStatsHTML).show();

    // Show cities
    var keys = Object.keys(markersInViewPort.cities);
    keys.sort();
    var n = 0;
    for (var i=0;i<keys.length;i++) {
      var citySlug = keys[i];
      var city = markersInViewPort.cities[citySlug];
      if (!isLocation || city.uf_slug==pageLocation.slug) {
          var html = locationHTML
              .replace(/{type}/g,'cidade')
              .replace(/{url}/g,city.url)
              .replace(/{slug}/g,city.slug)
              .replace(/{page_title}/g,city.page_title)
              .replace(/{name}/g,city.label)
              .replace(/{count}/g,city.countVisibleItems);
          jQuery('#locationStatsList').append(html);
          n += city.countVisibleItems;
      }
    }

    if(isLocation) {
      jQuery('#ajax_search_value').val(states[pageLocation.slug].label);
      jQuery('.locations_stats-subheader').text(qtty(n,'iniciativa')+' no estado');
    } else {
      jQuery('.locations_stats-header').text(title);
      jQuery('.locations_stats-subheader').html(
        qtty(markersInViewPort.items.length,'iniciativa')+
        ' em '+markersInViewPort.citiesCount+' cidades '+
        ' de '+markersInViewPort.statesCount+' estados<br />'+
        'CIDADES:'
      );
    }
  } else if (
    (!isLocation && markersInViewPort.statesCount<=maxSidebarStates) ||
    (isLocation && pageLocation.type=='region')
  ) {
    jQuery('#locationStats').html(basicLocationStatsHTML).show();

    // Show states
    var keys = Object.keys(markersInViewPort.states).sort();
  	keys.sort();
  	var n = 0;
    for (var i=0;i<keys.length;i++) {
      var stateSlug = keys[i];
      var state = markersInViewPort.states[stateSlug];
      if (!isLocation || state.region_slug==pageLocation.slug) {
        var html = locationHTML
          .replace(/{type}/g,'uf')
          .replace(/{url}/g,state.url)
          .replace(/{slug}/g,state.slug)
          .replace(/{page_title}/g,state.page_title)
          .replace(/{name}/g,state.label)
          .replace(/{count}/g,state.countVisibleItems);
      	jQuery('#locationStatsList').append(html);
      	n += state.countVisibleItems;
    	}
  	}

    if(isLocation) {
      jQuery('#ajax_search_value').val(regions[pageLocation.slug].page_title);
      jQuery('.locations_stats-subheader').text(qtty(n,'iniciativa')+' na região');
    } else {
      jQuery('.locations_stats-header').text(title);
      jQuery('.locations_stats-subheader').html(
        qtty(markersInViewPort.items.length,'iniciativa')+
        ' em '+markersInViewPort.citiesCount+' cidades '+
        'de '+markersInViewPort.statesCount+' estados<br />'+
        'ESTADOS:'
      );
    }
  } else {
    change_page_state(home_url, "", "", "global", "", 0, 'map');
    jQuery('#locationStats').html(basicLocationStatsHTML).show();
  	jQuery('.locations_stats-header').text('Zoom em sua região');
    jQuery('.locations_stats-subheader').text(qtty(markers.length,'iniciativa'));
    jQuery('#locationStatsList').html("");
    // Show regions
    var keys = Object.keys(markersInViewPort.regions).sort();
  	keys.sort();
    for (var i=0;i<keys.length;i++) {
      var regionSlug = keys[i];
      var region = markersInViewPort.regions[regionSlug];
      var html = locationHTML
        .replace(/{type}/g,'region')
        .replace(/{url}/g,"")
        .replace(/{slug}/g,regionSlug)
        .replace(/{page_title}/g,region.page_title)
        .replace(/{name}/g,region.name)
        .replace(/{count}/g,region.countVisibleItems);
      jQuery('#locationStatsList').append(html);
    }
  }
}

function goToThisLocation(el) {
    $el = jQuery(el);
    goToLocation($el.attr('href'), $el.attr('data-page_title'), "", $el.attr('data-type'), $el.attr('data-slug'), "");
}
function goToLocation(href, page_title, name, item_type, slug, searchLabel) {
    jQuery('#ajax_search_value').val(searchLabel);
    change_page_state(href, page_title, name, item_type, slug, 0, 'map');
    clearLocalityBoundaries();
    var target;
    switch(item_type) {
        case 'cidade':
            target = cities[slug];
            break;
        case 'uf':
            target = states[slug];
            break;
        case 'region':
            target = regions[slug];
            break;
    }

    isAutoZooming = true;
    if (typeof target != 'undefined') {
        zoomToCityOrState(target.src, target.address);
    } else {
        zoomToCityOrState("", name);
    }
}

//**
// Shows city or state boundaries and also zoom the viewport to city or state
//**
function zoomToCityOrState(src, address) {
    //show_loading(true);
    if(!src) {
        zoomToAddress(address);
        return;
    }

    jQuery.get(src)
        .done(function() {
            zoomToGeoJson(src);
        }).fail(function() {
            // Didn't find the city layer in geojson! Let's get it from Geocoder:
            zoomToAddress(address);
        })
}

function zoomToGeoJson(src) {
	jQuery.ajax({
		dataType: "json",
		url: src,
		success: function(data) {
			locationPolygon = L.geoJson(data, {
				style: function (feature) {
					return {
						interactive: false,
		        color: "#a40000",
		        weight: 1,
						fill: false,
		        fillColor: "#a40000",
		        fillOpacity: 0.4
					};
				}
			});
			locationPolygon.addTo(map);
			isAutoZooming = true;
	    map.fitBounds(locationPolygon.getBounds());
		}
	}).error(function() {});
}

function zoomToAddress(address) {
	geoProvider
  	.search({ query: address })
  	.then(function(result) {
    	//console.log(result);
			isAutoZooming = true;
			map.fitBounds(result[0].bounds);
  	});
}

function addItemToBounds(item, bounds) {
    if (typeof bounds == 'undefined' || typeof item.type == 'undefined' || item.type == 'header') {
        return;
    }
    if (item.type == 'uf') {
        var stateBounds = states[item.slug].bounds;
        bounds.extend(L.latLng(stateBounds.sw.lat, stateBounds.sw.lng));
        bounds.extend(L.latLng(stateBounds.ne.lat, stateBounds.ne.lng));
    } else if (cities[item.slug]) {
        if (item.type == 'cidade' && (typeof cities[item.slug].bounds!='undefined')) {
            var cityBounds = cities[item.slug].bounds;
            bounds.extend(L.latLng(cityBounds.sw.lat, cityBounds.sw.lng));
            bounds.extend(L.latLng(cityBounds.ne.lat, cityBounds.ne.lng));
        }
    } else if (item.type == 'item') {
        bounds.extend(L.latLng(item.lat, item.lng));
    }
}

function processPoints(geometry, callback, thisArg) {
		// TODO: Não entendi esta função!!! Qual a diferença entre um map.LatLng e um Data.Point?
    if (geometry instanceof google.maps.LatLng) {
        callback.call(thisArg, geometry);
    } else if (geometry instanceof google.maps.Data.Point) {
        callback.call(thisArg, geometry.get());
    } else {
        geometry.getArray().forEach(function(g) {
            processPoints(g, callback, thisArg);
        });
    }
}

function toggle_sidebar_loading(action) {
    if(action=='show') {
        jQuery('#sidebarLoading').show();
        jQuery('#locationStats').hide();
    } else {
        jQuery('#sidebarLoading').hide();
    }
}

function panToMarker(id) {
    var marker = getMarkerFromItemId(id);
    if(!marker) {
        return false;
    }
    map.setView(marker.getLatLng(), 16);
    openInfoWindow(marker);
    return true;
}

function getMarkerFromItemId(id) {
    var retMarker = false;
    jQuery.each(markers, function(i, marker) {
	    if(id==marker.options.id) {
	        retMarker = marker;
		    return false;
	    }
    });
    return retMarker;
}

function openInfoWindow(marker) {
    var content = generateInfoWindowContent(markersRaw[marker.options.mRawPos]);
    infowindow.setContent(content);
		infowindow.setLatLng(marker.getLatLng());
    map.openPopup(infowindow);
}

function generateInfoWindowContent(markerRaw) {
    return '<div class="idec_outer_marker_infos"> \
                <div class="idec_marker_infos" id="idec_marker_infos_'+markerRaw.id+'"> \
                    <div class="idec-info-text"> \
                        <h1>'+markerRaw.title+'</h1> \
                        <div class="address">'+markerRaw.address+'</div> \
                        <div class="city-uf">'+cities[markerRaw.city_slug]['label']+'</div> \
                        <div class="datetime">'+markerRaw.datetime+'</div> \
                    </div> \
                    <div class="idec-info-buttons"> \
                        <a href="https://maps.google.com/maps?daddr='+markerRaw.lat+','+markerRaw.lng+'" target="_blank" class="idec-info-route">Como Chegar</a> \
                        <a href="'+markerRaw.link+'" data-item-type="item" data-item-id="'+markerRaw.id+'" data-item-is_overlay=1 class="ajaxifythis more idec-info-more">Ver mais</a> \
                    </div> \
                </div> \
            </div>';
}

function clearLocalityBoundaries() {
	if (locationPolygon && map.hasLayer(locationPolygon)) {
		map.removeLayer(locationPolygon);
	}
	map.closePopup(infowindow);
}

function toggleLocalityBoundary() {
    clearLocalityBoundaries();
    jQuery('#ajax_search_value').val('');
    change_page_state(home_url, false, '', '', '', 0, 'map');
    updateSidebar();
}

function build_search_result_item(item, classPreposition, inAutocomplete) {
    var label = item.label;
    var $a = jQuery("<a></a>").addClass('idec-list-item-content');
    if (item.type=='header') {
        $h2 = jQuery("<h2></h2>").addClass('idec-search-autocomplete-section').text(item.label);
        return jQuery("<li></li>").addClass('header').append($h2);
    } else if (item.type=='item') {
        if(!inAutocomplete) {
            $a.addClass('ajaxifythis')
                .attr({
                    'href': item.url,
                    'data-item-type': 'item',
                    'data-item-id': item.id,
                    'data-empty-search': 1,
                    'data-item-is_overlay': 1
                });
        }
        var $div = jQuery("<div></div>");
        var $title = jQuery("<h1></h1>").addClass('idec-list-item-title').text(label).appendTo($div);
        var $address = jQuery("<p></p>").addClass('idec-list-item-address').text(item.address+', '+item.city).appendTo($div);
        $div.appendTo($a);
    } else if (item.type=='cidade') {
        if (!inAutocomplete) {
            $a.attr({
                'href': item.url,
                'data-type': 'cidade',
                'data-slug': item.slug,
                'data-page_title': item.title
            }).on('click', function(e) {
                e.preventDefault();
                jQuery('#ajax_search_value').val("");
                goToThisLocation(this);
            });
        }
        var $p = jQuery("<p></p>").addClass('idec-search-autocomplete-name').html("<span class='"+classPreposition+"-label-name'>"+label+"</span> <span class='"+classPreposition+"-label-count'>"+item.count+"</span>");
        $p.appendTo($a);
    } else if (item.type=='uf') {
        if (!inAutocomplete) {
            $a.attr({
                'href': item.url,
                'data-type': 'uf',
                'data-slug': item.slug,
                'data-page_title': item.title
            }).on('click', function(e) {
                e.preventDefault();
                jQuery('#ajax_search_value').val("");
                goToThisLocation(this);
            });
        }
        var $p = jQuery("<p></p>").addClass('idec-search-autocomplete-name').html("<span class='"+classPreposition+"-label-name'>"+label+"</span> <span class='"+classPreposition+"-label-count'>"+item.count+"</span>");
        $p.appendTo($a);
    } else if (item.type=='region') {
        if (!inAutocomplete) {
            $a.attr({
                'href': item.url,
                'data-type': 'region',
                'data-slug': item.slug,
                'data-page_title': item.page_title
            }).on('click', function(e) {
                e.preventDefault();
                jQuery('#ajax_search_value').val("");
                goToThisLocation(this);
            });
        }
        var $p = jQuery("<p></p>").addClass('idec-search-autocomplete-name').html("<span class='"+classPreposition+"-label-name'>"+label+"</span> <span class='"+classPreposition+"-label-count'>"+item.count+"</span>");
        $p.appendTo($a);
    }
    return jQuery("<li></li>").addClass(classPreposition+'-'+item.type).append($a);
}

//////////////////end of functions

(function( $ ) {
    $(function() {

        $(document).on("mouseenter", "#locationStatsList li", function(e){
            var id = $(this).attr('data-item-id');
            if(typeof id=='undefined') {
                return;
            }
            var marker = getMarkerFromItemId(id);
            if (!marker) {
                return false;
            }
            marker.setIcon(L.icon({iconUrl: baseIconDir+marker.options.type+'_ativo.png'}));
        });
        $(document).on("mouseleave", "#locationStatsList li", function(e){
            var id = $(this).attr('data-item-id');
            if(typeof id=='undefined') {
                return;
            }
            var marker = getMarkerFromItemId(id);
            if (!marker) {
                return false;
            }
            //circle.setMap(null);
            marker.setIcon(L.icon({iconUrl: baseIconDir+marker.options.type+'.png'}));
        });
        /*$(document).on("click", ".idec-form-send-photo label", function(e) {
            $("#button-idec-form-send-photo").show();
        });*/
        $(document).on("change", "#input-idec-form-send-photo", function(e) {
            var count = $(this).get(0).files.length;
            if (count==0) {
                $("#button-idec-form-send-photo").hide();
                $("#count-idec-form-send-photo").text("");
            } else {
                var text = (count==1)
                    ? "Enviar a foto selecionada"
                    : "Enviar as "+count+" fotos selecionadas";
                $("#button-idec-form-send-photo").show().val(text);
                $("#count-idec-form-send-photo").text("("+count+")");
                // $("#button-idec-form-send-photo").before('<i class="mapafeiras_icon-pag_upload" aria-hidden="true"></i>');
            }
        });
    });

})(jQuery);
