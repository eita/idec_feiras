function initForm() {

    //-----------------------------------------------------------------------------------FORM
    jQuery(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });

    //----------------------------------------------DEFINE QUAIS ITENS DEVEM APARECER
    var variable_itens = ['produtos', 'cafe', 'contato', 'descricao-produtos', 'ha-venda'];

    var show_form_itens = [];
    show_form_itens['feira'] = ['produtos', 'cafe', 'descricao-produtos'];
    show_form_itens['grupo'] = ['contato'];
    show_form_itens['produtor'] = ['produtos', 'descricao-produtos'];
    show_form_itens['cooperativa'] = ['ha-venda'];
    show_form_itens['associacao'] = ['ha-venda'];

    jQuery(document).on("change", ".radio-item-tipo input", function(e){
      var selected = jQuery('input[name=tipo]:checked').val();
      jQuery.each(variable_itens, function(i, v){ jQuery(".form-item."+v).hide() });
      jQuery.each(show_form_itens[selected], function(i, v){ jQuery(".form-item."+v).show() });
    });

    jQuery.each(variable_itens, function(i, v){ jQuery(".form-item."+v).hide() });
    jQuery.each(show_form_itens['feira'], function(i, v){ jQuery(".form-item."+v).show() });

    jQuery(document).on("change", ".ha-venda input", function(e){
      var this_value = jQuery(this).val();
      if(this_value == 'Sim') {
        jQuery('.form-item.produtos, .form-item.descricao-produtos').show();
      } else {
        jQuery('.form-item.produtos, .form-item.descricao-produtos').hide();
        // jQuery('.form-item.produtos, .form-item.descricao-produtos').toggle();
      }
    });

    jQuery(document).on("change", ".dias-funcionamento-opt > span > label > input", function(e){
      /*console.log(jQuery(this).val());
      console.log(jQuery(this).attr('checked'));*/

      var name = removeDiacritics(jQuery(this).val().toLowerCase());

      if(jQuery(this).attr('checked')) {

        var horario = '<input type="text" name="'+name+'_horario" placeholder="08h30 às 22h00" pattern="\([0-9]{2}\)[\s][0-9]{4,5}-[0-9]{4}" />';
        //var periodo = '<input type="checkbox" name="periodo-funcionamento-'+name+'[]" value="Manhã"> Manhã <input type="checkbox" name="periodo-funcionamento-'+name+'[]" value="Tarde"> Tarde <input type="checkbox" name="periodo-funcionamento-'+name+'[]" value="Noite"> Noite';
        var day_options = jQuery('<div />').addClass(name+'_options').addClass('day_options').css({'display':'none'});
        day_options.append(horario);
        //day_options.append(periodo);

        jQuery(this).parent().append(day_options);
        jQuery('.day_options input[type=text]')
    	    .mask('00h00 às 00h00');
        day_options.slideDown();

      } else {
        jQuery('.'+name+'_options').hide();
        jQuery('.'+name+'_options').find('input').val('').attr('checked', false);

      }

    });
    //----------------------------------------------VALIDAÇÃO
    // <span role="alert" class="wpcf7-not-valid-tip">Por favor preencha este campo obrigatório.</span>

    /*jQuery('.idec-form .wpcf7-submit').on('click', function(e){

      var selected = jQuery('input[name=tipo]:checked').val();
      if(selected == 'feira') {

        // if(jQuery('input.nome').val() == '') {
        //   alert('Defina o nome.');
        //   return false;
        // } else {
        //   return true;
        // }

      } else if(selected == 'grupo') {

      } else if(selected == 'produtor') {

      } else if(selected == 'cooperativa' || selected == 'associacao') {

      }

    }); */


    // jQuery('select#estado, select#cidade').on('focus', function(e){
    //   e.preventDefault();
    //   alert('Utilize o campo de endereço e o mapa para localizar corretamente o item.');
    //   jQuery(this).trigger('blur');
    //   jQuery('input#localizacao').trigger('focus');
    //   setTimeout(function() {

    //     jQuery('input#localizacao').trigger('click');

    //   }, 10);
    // });

    //POPULAR O FORMULÁRIO COM OS DADOS CASO SE TRATE DE UMA EDIÇÃO
    var edit_id = jQuery('.edit_id > input').val();
    if(edit_id) {

      var data = {
        'action': 'show_item_data_in_form',
        'edit_id': edit_id
      };

      jQuery.post(ajaxurl, data, function(response) {

        var items = jQuery.parseJSON(response);
        //console.log(items);

        jQuery('.radio-item-tipo input[value='+items['tipo']+']').click();

        jQuery('.titulo > input').val(items['titulo']);
        jQuery('.endereco > input').val(items['address-custom']);
        jQuery('.address > input').val(items['address']);
        jQuery('.latlng > input').val(items['latlng']);

        if(items['uf']) {
          jQuery("span.uf > input").val(items['uf']);
        }

        if(items['cidade']['slug']) {
          jQuery('span.cidade > input').val(items['cidade']['name']);
        }

        if(items['latlng']) {
            latlng = items['latlng'].replace('(', '');
            latlng = latlng.replace(')', '');
            latlng = latlng.replace(' ', '');
            latlng = latlng.split(',');
            setMapLatlng(latlng);
        } else if(items['address']) {
          setMapAddress(items['address']);
        }

        if(items['dias']) {
          jQuery.each(items['dias'], function(i, it){ jQuery('.dias-funcionamento input[value="'+it['name']+'"]').attr('checked', true);  });
        }
        if(items['periodos']) {
          jQuery.each(items['periodos'], function(i, it){ jQuery('.periodo-funcionamento input[value="'+it['name']+'"]').attr('checked', true);  });
        }

        if(items['produtos']) {
          jQuery('.ha-venda input[value="Sim"]').attr('checked', true);
          jQuery('.form-item.produtos, .form-item.descricao-produtos').show();
          jQuery.each(items['produtos'], function(i, it){ jQuery('.produtos input[value="'+it['slug']+'"]').attr('checked', true);  });
        } else {
          jQuery('.ha-venda input[value="Não"]').attr('checked', true);
        }

        jQuery('.horario > input').val(items['horario']);
        jQuery('.referencia > input').val(items['referencia']);
        jQuery('.contato > input').val(items['contato']);
        jQuery('.descricao-produtos > textarea').val(items['descricao']);

        if(items['cafe']=='s') { jQuery('.cafe input').attr('checked', true); }

      });


    }
    //end edit_id populate form

    jQuery('.wpcf7-form-control.wpcf7-file').attr('data-value', 'Foto Máx. 2Mb');
    jQuery('.wpcf7-form-control.wpcf7-file').on('change', function(e){

         //console.log(jQuery(this).val().split('\\').pop());
     jQuery(this).attr('data-value', jQuery(this).val().split('\\').pop());

    });


    jQuery("#produtos_da_sociobio").hide();
    jQuery('input[value="Produtos da Sociobiodiversidade"]').click(function () {
        console.log(jQuery('input[id="sb"]').is(":checked"));
        if (jQuery('input[value="Produtos da Sociobiodiversidade"]').is(":checked")) {
            jQuery("#produtos_da_sociobio").show();
        } else {
            jQuery("#produtos_da_sociobio").hide();
        }
    });


    //Hide the field initially
    jQuery("#comercio-condicoes").hide();
    jQuery('input[name="comercio-cond1[]"]').attr('checked', true);
    jQuery('input[name="comercio-cond2[]"]').attr('checked', true);
    jQuery('input[name="comercio-cond3[]"]').attr('checked', true);
    jQuery('input[name="comercio-cond4[]"]').attr('checked', true);
    jQuery('input[name="comercio-cond5[]"]').attr('checked', true);
    jQuery('input[name="comercio-cond6[]"]').attr('checked', true);
    jQuery('input[name="comercio-cond7[]"]').attr('checked', true);
    jQuery('textarea[name="comercio-cond-descricao"]').val(' ')

    jQuery('input[name=tipo]').change(function () {
        //Show conditions for comércio only if comércio is checked
        if (jQuery('input[name="tipo"]:checked').val() == "Comércio Parceiro de Orgânicos") {
            jQuery("#comercio-condicoes").show();
            jQuery('input[name="comercio-cond1[]"]').attr('checked', false);
            jQuery('input[name="comercio-cond2[]"]').attr('checked', false);
            jQuery('input[name="comercio-cond3[]"]').attr('checked', false);
            jQuery('input[name="comercio-cond4[]"]').attr('checked', false);
            jQuery('input[name="comercio-cond5[]"]').attr('checked', false);
            jQuery('input[name="comercio-cond6[]"]').attr('checked', false);
            jQuery('input[name="comercio-cond7[]"]').attr('checked', false);
            jQuery('textarea[name="comercio-cond-descricao"]').val('')
        } else {
            jQuery("#comercio-condicoes").hide();
            jQuery('input[name="comercio-cond1[]"]').attr('checked', true);
            jQuery('input[name="comercio-cond2[]"]').attr('checked', true);
            jQuery('input[name="comercio-cond3[]"]').attr('checked', true);
            jQuery('input[name="comercio-cond4[]"]').attr('checked', true);
            jQuery('input[name="comercio-cond5[]"]').attr('checked', true);
            jQuery('input[name="comercio-cond6[]"]').attr('checked', true);
            jQuery('input[name="comercio-cond7[]"]').attr('checked', true);
            jQuery('textarea[name="comercio-cond-descricao"]').val(' ')
        }
    });

    jQuery('input[name=tipo]').change(function () {
      // campo dias de funcionamento opcional para GCR
      if ( jQuery('input[name="tipo"]:checked').val() != "Grupo de Consumo Responsável" ) {
        jQuery(".dias-func .asterisk").html("*");
      }
      else {
        jQuery(".dias-func .asterisk").html("");
      }
      // campo contato opcional para feiras, obrigatório para os outros
      if ( jQuery('input[name="tipo"]:checked').val() != "Feira Orgânica ou Agroecológica" ) {
        jQuery(".contato .asterisk").html("*");
      }
      else {
        jQuery(".contato .asterisk").html("");
      }

    });

    jQuery(".idec-adicionar-local-tipo .wpcf7-list-item:nth-child(2) .wpcf7-list-item-label").append('<a class="box button" href="#popup1">?</a>');
    jQuery(".idec-adicionar-local-tipo .wpcf7-list-item:nth-child(3) .wpcf7-list-item-label").append('<a class="box button" href="#popup2">?</a>');

    jQuery(".selos .wpcf7-list-item:nth-child(1) .wpcf7-list-item-label").append('<a class="box button" href="#modal_organico_aud">?</a>');
    jQuery(".selos .wpcf7-list-item:nth-child(2) .wpcf7-list-item-label").append('<a class="box button" href="#modal_organico_part">?</a>');
    jQuery(".selos .wpcf7-list-item:nth-child(3) .wpcf7-list-item-label").append('<a class="box button" href="#modal_organico_ocs">?</a>');
    jQuery(".selos .wpcf7-list-item:nth-child(4) .wpcf7-list-item-label").append('<a class="box button" href="#modal_sociobio">?</a>');


}
