var $ = jQuery.noConflict();
$(document).ready(function() {

	//set address to readonly
	$('input[data-wpt-id=wpcf-address]').attr('readonly','readonly');

	//creates the map
	$('div#wpcf-group-localizacao').append('<div id="thismap" style="width:100%; height:300px;"></div>');	
	
	//changes address field and map based on the given coordinates

	$('input[data-wpt-id=wpcf-latlng]').on('blur', function(e){ 

		latlng = $(this).val();
		latlng = latlng.replace('(', '');
		latlng = latlng.replace(')', '');
		latlng = latlng.replace(' ', '');
		latlng = latlng.split(',');
		latlng = [parseFloat(latlng[0]) , parseFloat(latlng[1])];

		//update de map
		setMap(latlng);

		//update address field
		$('input[data-wpt-id=wpcf-address]').val(
			$('#thismap').gmap3({
				 getaddress:{
							latLng:latlng,
							callback: function(results){
								try {
									var endereco = results[0].formatted_address;
								} catch (err) {
									console.log ("address not found: " + err);
								}

								$('input[data-wpt-id=wpcf-address]').val(endereco);
							}
						} 
			})
			)

	});
	

	var latlng = $('input[data-wpt-id=wpcf-latlng]').val(),
		localizacao = $('input[data-wpt-id=wpcf-address]').val();

	if(latlng!='' && typeof(latlng)!="undefined") {
		//if editing post, show the current map

		latlng = latlng.replace('(', '');
		latlng = latlng.replace(')', '');
		latlng = latlng.replace(' ', '');
		latlng = latlng.split(',');

		setMap(latlng);
	} else {
		//if new post show blank map
		setIniMap();
	}

});

function setIniMap(){
	$('#thismap').gmap3({
		map: {
			options: {
				zoom:15,
				mapTypeControl: false,
				zoomControl: true,
				draggable: true
			}
		}
	});
}

function setNomap() {	$('#thismap').gmap3(); }
function destroyMap() { $('#thismap').gmap3('destroy'); }


function setMap(latlng) {

	destroyMap();

	$('#thismap').gmap3({
		map: {
			options: {
				center : latlng,
				zoom: 15

			} 
		},
		marker:{
			latLng:latlng,
			options:{
				draggable: true
			},
			events:{
				dragend: function(marker) {
					$(this).gmap3({
						 getaddress:{
							latLng:marker.getPosition(),
							callback: function(results){
								if (!results) {
									return;
								}
								var endereco = results[0].formatted_address;								
								var latlng = '('+results[0].geometry.location.lat()+','+results[0].geometry.location.lng()+')';
								$('input[data-wpt-id=wpcf-address]').val(endereco);
								$('input[data-wpt-id=wpcf-latlng]').val(latlng);
							}
						} 
					});
				}
			}

		}
		
	});


}

function pegaEnderecoDoGoogle(addressComponents, formattedAddress) {
    var endereco = {};
	var i;
	for(i=0;i<addressComponents.length;i++) {
	    var types = addressComponents[i].types;
	    var j;
	    for (j=0;j<types.length;j++) {
	        if (types[j]=='route') {
	            endereco.rua = addressComponents[i].long_name;
	        } else if (types[j]=='street_number') {
	            endereco.numero = addressComponents[i].long_name;
	        } else if (types[j]=='sublocality') {
	            endereco.bairro = addressComponents[i].long_name;
	        } else if (types[j]=='locality' || types[j]=='administrative_area_level_2') {
	            endereco.cidade = addressComponents[i].long_name;
	        } else if (types[j]=='administrative_area_level_1') {
	            endereco.estado = addressComponents[i].short_name;
	        } else if (types[j]=='postal_code') {
	            endereco.CEP = addressComponents[i].long_name;
	        }
	    }
	}
	if (endereco.rua && endereco.numero) {
	    endereco.enderecoCompleto = endereco.rua+', '+endereco.numero;
	    if (endereco.bairro) {
	        endereco.enderecoCompleto += ', '+endereco.bairro;
	    }
	    if (endereco.CEP) {
	        endereco.enderecoCompleto += ', CEP: '+endereco.CEP;
	    }
	} else {
	    endereco.enderecoCompleto = formattedAddress;
	}
	return endereco;
}