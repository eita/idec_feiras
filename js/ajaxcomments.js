/** Author InkThemes ***/
jQuery('document').ready(function($){

    var list ;
    $(document).on('click', 'a.comment-reply-link', function(e){
        e.preventDefault();
    	list = $(this).parent().parent().parent().attr('id');
    });

    $(document).on('click', '#attachmentForm input#submit', function(e){
	    e.preventDefault();
	    var commentform=$('#attachmentForm'); // find the comment form

	    commentform.prepend('<div id="comment-status" ></div>'); // add info panel before the form to provide feedback or errors

	    var statusdiv=$('#comment-status'); // define the info panel
	    var formdata=commentform.serialize();
	    statusdiv.html('<p>Processando...</p>');
	    var formurl=commentform.attr('action');

	    //Post Form with data
	    $.ajax({
		    type: 'post',
		    url: formurl,
		    data: formdata,
		    error: function(XMLHttpRequest, textStatus, errorThrown)
		    {
			    statusdiv.html('<p class="ajax-error" >Ocorreu um erro ao enviar o comentário.</p>');
		    },
		    success: function(data, textStatus){
			    if(data == "success" || textStatus == "success"){

			     	var error = data.indexOf("Erro");
			     	console.log(error);

			     	var comentario = $('textarea#comment').val();

			     	if(error > 0 || comentario=='')  {
					    statusdiv.html('<p class="ajax-error" >Ocorreu um erro. Confira se você preencheu todos os campos obrigatórios.</p>');
			     	} else {
					    statusdiv.html('<p class="ajax-success" >Obrigado. Seu comentário foi enviado e pode aguardar moderação.</p>');
			     	}



				    if($("#commentsbox").has("ol.commentlist").length > 0){
					    if(list != null){
						    $('div.rounded').prepend(data);
					    } else {
						    $('ol.commentlist').append(data);
					    }
				    } else {
					    $("#commentsbox").find('div.post-info').prepend('<ol class="commentlist"> </ol>');
					    $('ol.commentlist').html(data);
				    }
			
			    } else {
				    // statusdiv.html('<p class="ajax-error" >Please wait a while before posting your next comment</p>');
				    statusdiv.html(data);
				    commentform.find('textarea[name=comment]').val('');
			    }
		    }
	    });
	    return false;

    });

});

