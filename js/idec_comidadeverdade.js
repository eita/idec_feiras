function initIdecComidadeverdadeTable() {
    function format ( d, CAMPOS ) {
        // `d` is the original data object for the row
        //var html = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
        var html = '<ul class="w3-ul w3-small w3-pale-green">';
        for (var i = 1; i < CAMPOS.length; i++) {
            if ((CAMPOS[i].child || CAMPOS[i].mobileHide) && d[i]) {
                html += (CAMPOS[i].child)
                    ? '<li><span class="w3-text-dark-gray">' + CAMPOS[i].title + ':</span> ' + d[i] + '</li>'
                    : '<li class="w3-hide-medium w3-hide-large"><span class="w3-text-dark-gray">' + CAMPOS[i].title + ':</span> ' + d[i] + '</li>';
            }
        }
        html += '</ul>';
        return html;
    }

    if (jQuery('#comidadeverdade_table')) {
        const ESTADOS = {
            "Acre": "AC",
            "Alagoas": "AL",
            "Amapá": "AP",
            "Amazonas": "AM",
            "Bahia": "BA",
            "Ceará": "CE",
            "Distrito Federal": "DF",
            "Espírito Santo": "ES",
            "Goiás": "GO",
            "Maranhão": "MA",
            "Mato Grosso": "MT",
            "Mato Grosso do Sul": "MS",
            "Minas Gerais": "MG",
            "Pará": "PA",
            "Paraíba": "PB",
            "Paraná": "PR",
            "Pernambuco": "PE",
            "Piauí": "PI",
            "Rio de Janeiro": "RJ",
            "Rio Grande do Norte": "RN",
            "Rio Grande do Sul": "RS",
            "Rondônia": "RO",
            "Roraima": "RR",
            "Santa Catarina": "SC",
            "São Paulo": "SP",
            "Sergipe": "SE",
            "Tocantins": "TO"
        };
        var defaultColor = "yellow";
        const CAMPOS = [
            {
                "className":      'details-control mapafeiras_icon-ver w3-text-black',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { title: "Carimbo de data/hora", visible: false },
            { title: "Endereço de e-mail", visible: false, email: true },
            { title: "Iniciativa", className: "w3-small", bold: true, alternativas: [{offset: 27}, {offset: 50, attachOffset: 51}] },
            { title: "Região", className: "w3-small w3-hide-small", mobileHide: true, alternativas: [{offset: 27}, {offset: 51}] },
            { title: "UF", className: "w3-small", UF: true, alternativas: [{offset: 27}, {offset: 51}] },
            { title: "Cidade", className: "w3-small", alternativas: [{offset: 27}, {offset: 51}] },
            { title: "Modalidade", className: "w3-small w3-hide-small", mobileHide: true, list: true, alternativas: [{offset: 34}, {offset: 60}] },
            { title: "Característica", className: "w3-small", visible: false, child: true, list: true },
            { title: "Tipo de certificação ou característica dos produtos", visible: false, child: true },
            { title: "Alimentos oferecidos", visible: false, child: true, list: false, alternativas: [{offset: 34}, {offset: 60}], suppress: [
                " (folhosas, verduras, legumes, etc..)",
                " (feijões, lentilha, grão de bico, etc)",
                " (pães, bolos, tortas, etc)"
                ]
            },
            { title: "Funcionamento", visible: false, child: true },
            { title: "Dias de funcionamento", visible: false, alternativas: [{offset: 30}, {offset: 56}] },
            { title: "Horários", className: "w3-small", visible: false, child: true, alternativas: [{offset: 30}, {offset: 56}], attach: {attached: 12, list: true, labels: ['', '']} },
            { title: "Endereço", visible: false, child: true, alternativas: [{offset: 32}, {offset: 58}] },
            { title: "CEP", visible: false, child: true },
            { title: "Ponto de referência", visible: false, child: true, alternativas: [{offset: 31}, {offset: 57}] },
            { title: "Região de entrega a domicílio", visible: false, child: true, alternativas: [{offset: 28}, {offset: 54}] },
            { title: "Responsável pela iniciativa", visible: false },
            { title: "Telefone do responsável com DDD", visible: false },
            { title: "Email", email: true, visible: false },
            { title: "Telefone de contato para entregas", visible: false, child: true, alternativas: [{offset: 28}, {offset: 54}] },
            { title: "Rede social da iniciativa", http: true, visible: false, child: true, alternativas: [{offset: 15}, {offset: 39}] },
            { title: "Você conhece o Mapa de Feiras Orgânicas do Idec?", visible: false },
            { title: "Sua iniciativa já está registrada no Mapa do Idec?", visible: false },
            { title: "Tem interesse em registrar no Mapa de Feiras?", visible: false },
            { title: "Você tem fotos da produção?", visible: false },
            { title: "Sentiu falta de algo neste formulário?", visible: false }
        ];
        var sheet_id = "1UVcu0Z6BY5hrER3fTXMpM5fS5JiUAq2qug1_1XFFg2c";
        var sheet_page = 4;
        var url = 'https://spreadsheets.google.com/feeds/cells/' + sheet_id + '/' + sheet_page + '/public/full?alt=json';
        var data = {
        };
        jQuery.get(url, data, function(response) {
            var cels = response.feed.entry;
            var row = 0;
            var col = 0;
            var htmlValue = "";
            var tableData = [];
            var tableDataRow = [""];
            for (var i = 0; i < cels.length; i++) {
                var dataCel = cels[i].gs$cell;
                if (dataCel.row < 2) {
                    continue;
                }
                if (dataCel.row > row) {
                    if (row == 0) {
                        row = dataCel.row;
                    } else {
                        while (tableDataRow.length < CAMPOS.length) {
                            tableDataRow.push("");
                        }
                        if (tableDataRow[1] != "") {
                            tableData.push(tableDataRow);
                        }
                        tableDataRow = [""];
                        row++;
                        col = 0;
                    }
                }
                if (dataCel.col > (col + 1)) {
                    for (var j = 0; j < dataCel.col - col - 1; j++) {
                        tableDataRow.push("");
                    }
                    col = parseInt(dataCel.col);
                } else {
                    col++;
                }
                if (col >= CAMPOS.length) {
                    continue;
                }
                var inputValue = dataCel.$t;
                var campoAtual = CAMPOS[col];
                if (inputValue == '§') {
                    inputValue = '';
                }
                if (inputValue == '' && campoAtual.hasOwnProperty('alternativas')) {
                    for (var k = 0; k < campoAtual.alternativas.length; k++) {
                        var alt = campoAtual.alternativas[k];
                        var dataCelOffset = cels[i+alt.offset].gs$cell;
                        if (dataCelOffset.$t != '' && dataCelOffset.$t != '§') {
                            inputValue = dataCelOffset.$t;
                            if (alt.hasOwnProperty('attachOffset')) {
                                inputValue += ' (' + cels[i+alt.attachOffset].gs$cell.$t + ')';
                            }
                            break;
                        }
                    }
                }
                if (inputValue == '') {
                    tableDataRow.push('');
                    continue;
                }
                htmlValue = '';
                if (campoAtual.UF && ESTADOS.hasOwnProperty(inputValue)) {
                    inputValue = ESTADOS[inputValue];
                }
                if (campoAtual.suppress) {
                    for (var j = 0; j < campoAtual.suppress.length; j++) {
                        inputValue = inputValue.replace(campoAtual.suppress[j], '');
                    }
                }
                if (campoAtual.tags) {
                    var values = inputValue.split(", ");
                    htmlValue += '<ul class="w3-ul">'
                    for (var j = 0; j < values.length; j++) {
                        var color = (campoAtual.tags.hasOwnProperty(values[j]))
                            ? campoAtual.tags[values[j]].color
                            : defaultColor;
                        htmlValue += '<li><span class="w3-tag w3-small w3-' + color + '">' + values[j] + '</span></li>';
                    }
                    htmlValue += '</ul>';
                } else if (campoAtual.list) {
                    var values = inputValue.split(", ");
                    var style = (values.length > 1) ? ' style="list-style: disc"' : '';
                    htmlValue += '<ul' + style + '>';
                    for (var j = 0; j < values.length; j++) {
                        htmlValue += '<li>' + values[j] + '</li>';
                    }
                    htmlValue += '</ul>';
                } else if (campoAtual.attach) {
                    if (campoAtual.attach.list) {
                        htmlValue = '<ul class="w3-ul w3-small">';
                        htmlValue += '<li><span class="w3-text-dark-gray">' + campoAtual.attach.labels[0] + '</span> ' + inputValue + '</li>';
                        htmlValue += '<li><span class="w3-text-dark-gray">' + campoAtual.attach.labels[1] + '</span> ' + tableDataRow[campoAtual.attach.attached] + '</li>';
                        htmlValue += '</ul>';
                    } else {
                        htmlValue = '<span class="w3-small">' + inputValue + campoAtual.attach.separator + tableDataRow[campoAtual.attach.attached] + '</span>';
                    }
                } else if (campoAtual.http) {
                    htmlValue = '<span class="w3-small"><a href="' + inputValue + '">' + inputValue + '</a></span>';
                } else if (campoAtual.email) {
                    htmlValue = '<span class="w3-small"><a href="mailto:' + inputValue + '">' + inputValue + '</a></span>';
                } else {
                    htmlValue = '<span class="w3-small">' + inputValue + '</span>';
                }
                if (campoAtual.bold) {
                    htmlValue = '<span class=""><b>' + htmlValue + '</b></span>';
                }
                tableDataRow.push(htmlValue);
            }
            // Add last row:
            while (tableDataRow.length < CAMPOS.length) {
                tableDataRow.push("");
            }
            if (tableDataRow[1] != "") {
                tableData.push(tableDataRow);
            }

            var comidadeverdadeTable = jQuery('#comidadeverdade_table').DataTable( {
                data: tableData,
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: 'Como funciona',
                        action: function ( e, dt, node, config ) {
                            alert( idecPageExcerpt );
                        }
                    },
                    {
                        text: 'Adicionar iniciativa',
                        action: function ( e, dt, node, config ) {
                            jQuery('<a href="https://forms.gle/VUeJ9ZX4DdKJ5ckU8" target="blank"></a>')[0].click();
                        }
                    }
                ],
                responsive: true,
                compact: true,
                hover: true,
                stripe: true,
                language: {
                    "sEmptyTable": "Nenhuma iniciativa encontrada",
                    "sInfo": "Mostrando de _START_ a _END_ de _TOTAL_ iniciativas",
                    "sInfoEmpty": "Mostrando 0 a 0 de 0 iniciativas",
                    "sInfoFiltered": "(Filtradas de _MAX_ iniciativas)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ iniciativas por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhuma iniciativa encontrada",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    },
                    "select": {
                        "rows": {
                            "_": "Selecionado %d linhas",
                            "0": "Nenhuma linha selecionada",
                            "1": "Selecionado 1 linha"
                        }
                    }
                },
                columns: CAMPOS,
                initComplete: function(settings, json) {
                    jQuery('#comidadeverdade_table th').removeClass('mapafeiras_icon-ver');
                    jQuery('#idec_comidadeverdade_loading').hide();
                    jQuery('#comidadeverdade_table tbody').on('click', 'tr.odd, tr.even', function () {
                        var tr = jQuery(this);
                        var row = comidadeverdadeTable.row( tr );
                        var tdControl = tr.children('.details-control');
                        if ( row.child.isShown() ) {
                            row.child.hide();
                            tdControl.removeClass('mapafeiras_icon-fechar');
                            tdControl.addClass('mapafeiras_icon-ver');
                        }
                        else {
                            row.child( format(row.data(), CAMPOS) ).show();
                            tdControl.removeClass('mapafeiras_icon-ver');
                            tdControl.addClass('mapafeiras_icon-fechar');
                        }
                    });
                }
            });
        });
    }
}
