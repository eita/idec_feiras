
    var localizacao, latlng, input, autocomplete;

	function runAddNewInitiative() {
	    localizacao = jQuery('input#localizacao').val();
    	latlng = jQuery('span.latlng input').val();
    	
    	jQuery('.wpcf7-tel')
    	    .attr('pattern', "\([0-9]{2}\)[\s][0-9]{4,5}-[0-9]{4}")
    	    .mask("(00) 0000-00009");

	    //autocomplete
	    input = document.getElementById('localizacao');
	    options = {
		    types: ['address'],
		    componentRestrictions: {country: 'br'}
	    };

	    autocomplete = new google.maps.places.Autocomplete(input, options);
	    
	    google.maps.event.addListener(autocomplete, 'place_changed', function() {
		    fillInAddress();
	    });

	    jQuery('a.search-address').on('click', function(e){
		    e.preventDefault();
	    });

	    if(latlng!='') {

		    latlng = latlng.replace('(', '');
		    latlng = latlng.replace(')', '');
		    latlng = latlng.replace(' ', '');
		    latlng = latlng.split(',');

		    setMapLatlng(latlng);

	    } else if(localizacao!='') {
		    //if editing post, show the current map
		    setMapAddress(localizacao);
	    } else {
		    //if new post show blank map
		    setIniMap();
	    }

	    // if no lat lng was set, clear address field to force validation error
	    jQuery('input[type=submit]').on('click', function(e){
		    if (!jQuery('input[name=latlng]').val()){
		    	jQuery('#localizacao').val('');
		    }
	    });

		jQuery('#localizacao').on('blur', function(e){
			setTimeout(function(){
				if (!jQuery('input[name=latlng]').val()){
					if(!jQuery('#latlng-alert').length) {
						var alert = "<span id='latlng-alert' role='alert' class='wpcf7-not-valid-tip'>Escolha uma das opções de localização no mapa.</span>";
						jQuery('.endereco').append(alert);
					}
					jQuery('#localizacao').val('');
					jQuery('#localizacao').focus();
				} else {
					jQuery('#latlng-alert').hide();
				}					
			}, 1000);
		});
	}

	function fillInAddress() {

	  var place = autocomplete.getPlace();

	  var endereco = pegaEnderecoDoGoogle(place.address_components, place.formatted_address);
	  

	  jQuery('input#localizacao, span.address > input').val(endereco.enderecoCompleto);
	  jQuery('span.cidade > input').val(endereco.cidade);
	  jQuery('span.uf > input').val(endereco.estado);
	  // jQuery('.latlng input').val('('+place.geometry.location['k']+','+place.geometry.location['D']+')');
	  // jQuery('.latlng input').val('('+place.geometry.location['A']+','+place.geometry.location['F']+')');
	  jQuery('.latlng input').val('('+place.geometry.location.lat()+','+place.geometry.location.lng()+')');


	  setMapLatlng(place.geometry.location);

	}



var setIniMap = function(){
	console.log("set ini");
	jQuery('#map').html('<br/><br/><br/><br/>Digite o endereço no campo acima, e escolha entre uma das opções oferecidas. <br><br> Se necessário, após a escolha movimente o marcador para selecionar o local exato. <br><br> Depois disso, ainda é possível editar o endereço caso deseje.');
}

var setNomap = function() {	jQuery('#map').gmap3(); }
var destroyMap = function() { jQuery('#map').gmap3('destroy'); }
var setMapLatlng = function(latlng) {

		destroyMap();
		jQuery('#map').gmap3({
		map: {
			options: {
				center : latlng,
				zoom: 15

			} 
		},
		marker:{
			latLng:latlng,
			options:{
				draggable: true
			},
			events:{
				dragend: function(marker) {
					jQuery(this).gmap3({
						 getaddress:{
							latLng:marker.getPosition(),
							callback: function(results){

								var endereco = pegaEnderecoDoGoogle(results[0].address_components, results[0].formatted_address);
								
								jQuery('input#localizacao, span.address > input').val(endereco.enderecoCompleto);
								jQuery('span.cidade > input').val(endereco.cidade);
								jQuery('span.uf > input').val(endereco.estado);
								// jQuery('.latlng input').val('('+results[0].geometry.location['k']+','+results[0].geometry.location['D']+')');
								// jQuery('.latlng input').val('('+results[0].geometry.location['A']+','+results[0].geometry.location['F']+')');
								jQuery('.latlng input').val('('+results[0].geometry.location.lat()+','+results[0].geometry.location.lng()+')');


							}
						} 
					});
				}
			}

		}
		
	});
}

function pegaEnderecoDoGoogle(addressComponents, formattedAddress) {
    var endereco = {};
	var i;
	for(i=0;i<addressComponents.length;i++) {
	    var types = addressComponents[i].types;
	    var j;
	    for (j=0;j<types.length;j++) {
	        if (types[j]=='route') {
	            endereco.rua = addressComponents[i].long_name;
	        } else if (types[j]=='street_number') {
	            endereco.numero = addressComponents[i].long_name;
	        } else if (types[j]=='sublocality') {
	            endereco.bairro = addressComponents[i].long_name;
	        } else if (types[j]=='locality' || types[j]=='administrative_area_level_2') {
	            endereco.cidade = addressComponents[i].long_name;
	        } else if (types[j]=='administrative_area_level_1') {
	            endereco.estado = addressComponents[i].short_name;
	        } 
	        // else if (types[j]=='postal_code') {
	        //     endereco.CEP = addressComponents[i].long_name;
	        // }
	    }
	}
	if (endereco.rua && endereco.numero) {
	    endereco.enderecoCompleto = endereco.rua+', '+endereco.numero;
	    if (endereco.bairro) {
	        endereco.enderecoCompleto += ', '+endereco.bairro;
	    }
	    // if (endereco.CEP) {
	    //     endereco.enderecoCompleto += ', CEP: '+endereco.CEP;
	    // }
	} else {
	    endereco.enderecoCompleto = formattedAddress;
	}
	return endereco;
}

var setMapAddress = function(localizacao) {

	destroyMap();

	jQuery('#map').gmap3({
		
		getlatlng:{
			address:  localizacao,
			callback: function(results){
				if ( !results ) return;

				// var latlng = '('+results[0].geometry.location['k']+','+results[0].geometry.location['D']+')';
				// var latlng = '('+results[0].geometry.location['A']+','+results[0].geometry.location['F']+')';
				var latlng = '('+results[0].geometry.location.lat()+','+results[0].geometry.location.lng()+')';
				jQuery('.latlng input').val(latlng);

				jQuery('#map').gmap3({
					map: {
						options: {
							center : results[0].geometry.location,
							zoom: 15
							
						} 
					},

					getaddress:{
						latLng: results[0].geometry.location,
						callback: function(results){
							
							
						    var endereco = pegaEnderecoDoGoogle(results[0].address_components, results[0].formatted_address);
							
							jQuery('input#localizacao, span.address > input').val(endereco.enderecoCompleto);
							jQuery('span.cidade > input').val(endereco.cidade);
							jQuery('span.uf > input').val(endereco.estado);
							// jQuery('.latlng input').val('('+results[0].geometry.location['k']+','+results[0].geometry.location['D']+')');
							// jQuery('.latlng input').val('('+results[0].geometry.location['A']+','+results[0].geometry.location['F']+')');
							jQuery('.latlng input').val('('+results[0].geometry.location.lat()+','+results[0].geometry.location.lng()+')');




						}
					}, 

					marker:{
						latLng:results[0].geometry.location,
						options:{
							draggable: true
						},	
						events:{
							dragend: function(marker) {
								jQuery(this).gmap3({
									 getaddress:{
										latLng:marker.getPosition(),
										callback: function(results){
										
										    var endereco = pegaEnderecoDoGoogle(results[0].address_components, results[0].formatted_address);
											
											jQuery('input#localizacao, span.address > input').val(endereco.enderecoCompleto);
											jQuery('span.cidade > input').val(endereco.cidade);
											jQuery('span.uf > input').val(endereco.estado);
											// jQuery('.latlng input').val('('+results[0].geometry.location['k']+','+results[0].geometry.location['D']+')');
											// jQuery('.latlng input').val('('+results[0].geometry.location['A']+','+results[0].geometry.location['F']+')');
											jQuery('.latlng input').val('('+results[0].geometry.location.lat()+','+results[0].geometry.location.lng()+')');





										}
									} 
								});
							}
						}

					}
				});
				
			}
		}
	});


 
}
