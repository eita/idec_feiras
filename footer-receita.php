    <?php get_template_part( 'structure-final' ); ?>
	<script type="text/javascript">
        var items = <?= json_encode(idec_receita_get_receitas()) ?>;
        var alimentos = <?= json_encode(idec_receita_get_non_empty('alimento')) ?>;
        var tiposDeReceita = <?= json_encode(idec_receita_get_non_empty('tipo-de-receita')) ?>;
        var generalArchiveContentHMTL = '<?= str_replace(array("\n", "  "),"",idec_get_template_model("general-archive", "content")) ?>';
        var generalArchiveItemContentHTML = '<?= str_replace(array("\n", "  "),"",idec_get_template_model("general-archive", "item-content")) ?>';
        var closeItHTML = '<?= str_replace(array("\n", "  "),"",idec_get_template_model("general-closeit", "content", array("see_all"=>"ver todas as receitas", "homeurl"=>idec_get_page_type_homeurl()))) ?>';
        
        (function( $ ) {
            $(function() {
                remove_loading();
                if(initialPageLocation && initialPageLocation.isOverlay) {
                    openItemPage(initialPageLocation.type, initialPageLocation.slug, initialPageLocation.href, initialPageLocation.isOverlay, initialPageLocation.page_type, initialPageLocation.paged);
                }
                
                // SEARCH!
                var options = {
	                source: function(request, response) {
                            
                        if (request.term==="") {
                            response([]);
                            return;
                        }
                        
                        response(search_receitas(request.term, true));
	                },
	                minLength: 2,
	                focus: function(event, ui) {
	                    event.preventDefault();
	                },
	                select: function(event, ui) {
                        event.preventDefault();
                        if (ui.item.type=='header') {
                            return false;
                        }
                        var label, isOverlay, slug;
                        if (ui.item.type=='item'){
                            label = ui.item.title;
                            isOverlay = true;
                            slug = ui.item.id;
                        } else {
                            label = ui.item.label;
                            isOverlay = false;
                            slug = ui.item.slug;
                        }
                        $('#ajax_search_value').val(label);
                        
                        openItemPage(ui.item.type, slug, ui.item.url, isOverlay, PAGE_TYPE);
			        }
                };
                $('#ajax_search_value')
                    .autocomplete(options)
                    .keypress(function(e) {
                        var s = $(this).val();
                        if(e.which == 13 && s.length>=2) {
                            $("#ajax_search_value").autocomplete( "close" );
                            var page_type = 'receita',
                                item_type = 'search',
                                id = s,
                                title = 'Busca de receitas: "'+s+'"',
                                matches = search_receitas(id, false);
                            if (typeof cachedPageLocations[getLKey(page_type,item_type,id,1)] == 'undefined') {
                                cachedPageLocations[getLKey(page_type,item_type,id,1)] = {
                                    html: build_search_results_page(matches, page_type, id, title, 1, s),
                                    targetEl: get_targetEl(false),
                                    id: id,
                                    name: s,
                                    title: title,
                                    updatedAt: new Date().getTime()
                                }
                            }
                            openItemPage(item_type, id, "", false, page_type);
                        }
                    })
                    .data("ui-autocomplete")._renderItem = function(ul, item) {
                        return build_search_result_item(item, 'm', true).appendTo(ul);
		            };
                
                function build_search_result_item(item, classPreposition, inAutocomplete) {
                    var label = item.title;
                    var $a = jQuery("<a></a>").addClass('idec-list-item-content');
                    if (item.type=='header') {
                        $h2 = jQuery("<h2></h2>").addClass('idec-search-autocomplete-section').text(label);
                        return jQuery("<li></li>").addClass('header').append($h2);
                    } else if (item.type=='item') {
                        if(!inAutocomplete) {
                            $a.addClass('ajaxifythis')
                                .attr({
                                    'href': item.url, 
                                    'data-item-type': item.type,
                                    'data-item-id': item.id,
                                    'data-page_title': item.title,
                                    'data-item-is_overlay': 1
                                });
                        }
                        var $div = jQuery("<div></div>");
                        var $title = jQuery("<h1></h1>").addClass('idec-list-item-title').text(label).appendTo($div);
                        var $description = jQuery("<p></p>").addClass('idec-list-item-address').text(get_excerpt(item.description)).appendTo($div);
                        $div.appendTo($a);
                    } else {
                        if (!inAutocomplete) {
                            $a.addClass('ajaxifythis')
                                .attr({
                                'href': item.url, 
                                'data-type': item.type,
                                'data-id': item.id,
                                'data-page_title': item.page_title,
                                'data-item-is_overlay': 0
                            });
                        }
                        var $p = jQuery("<p></p>").addClass('idec-search-autocomplete-name').html("<span class='"+classPreposition+"-label-name'>"+item.label+"</span> <span class='"+classPreposition+"-label-count'>"+item.count+"</span>");
                        $p.appendTo($a);
                    }
                    return jQuery("<li></li>").addClass(classPreposition+'-'+item.type).append($a);
                }
            });
        })(jQuery);
        
        function search_receitas(s, isAutocomplete) {
            var item, alimento, tipoDeReceita, 
                matches = [], alimentoMatches = [], tipoDeReceitaMatches = [];
                
            for  (var id in items) {
                item = items[id];
                item.type='item';
                if (matchChecker(s, item, ['title', 'description'])) {
                    matches.push(item);
                }
            }
            
            if(!isAutocomplete) {
                return matches;
            }
            
            if(matches.length>0) {
                matches.unshift({
                    'type': 'header',
                    'title': 'Receitas'
                });
            }
            
            for (var slug in alimentos) {
                alimento = alimentos[slug];
                alimento.type='alimento';
                if (matchChecker(s, alimento, ['label'])) {
                    alimentoMatches.push(alimento);
                }
            }
            if(alimentoMatches.length>0) {
                alimentoMatches.unshift({
                    'type': 'header',
                    'title': 'Alimentos'
                });
            }
            
            for (var slug in tiposDeReceita) {
                tipoDeReceita = tiposDeReceita[slug];
                tipoDeReceita.type='tipo-de-receita';
                if (matchChecker(s, tipoDeReceita, ['label'])) {
                    tipoDeReceitaMatches.push(tipoDeReceita);
                }
            }
            if(tipoDeReceitaMatches.length>0) {
                tipoDeReceitaMatches.unshift({
                    'type': 'header',
                    'title': 'Tipos de receita'
                });
            }
            var ret = matches.concat(alimentoMatches).concat(tipoDeReceitaMatches);
            
            return ret;
        }
        
	</script>


	    <?php wp_footer(); ?>
	</body>
</html>
