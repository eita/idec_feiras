<?php 

/*add_action( 'pre_get_posts', 'idec_receita_posts_per_page' );
function idec_receita_posts_per_page( $query ) {
    if ( $query->is_post_type_archive('receita') ) {
        $query->set('orderby', 'rand');
    }
}*/

function idec_receita_get_count() {
    $count = wp_count_posts('receita');
    return $count->publish;
}

// this function expects a text with line breaks and outputs a list
function idec_receita_process_list ( $list, $class= "idec-receita-list", $mode = "ul") {
    $list = strip_tags($list, "<p><u>   ");
    return $list;
}

function idec_receita_set_main_query_to_archive($paged=1) {
    global $wp_query, $receita_taxonomy_query;
    $receita_taxonomy_query = clone $wp_query;
    idec_set_main_query_to_archive('receita', $paged, get_option('posts_per_page'));
}

function idec_receita_get_taxonomy_query($paged=1) {
    global $pageLocation, $receita_taxonomy_query;

    if (!$receita_taxonomy_query) {
        $args = array(
            'post_type'=>'receita',
            'paged'=>$paged,
            'posts_per_page'=>get_option('posts_per_page'),
            'tax_query'=>array(
                array(
                    'taxonomy'=>$pageLocation->type,
                    'field'=>'slug',
                    'terms'=>$pageLocation->slug
                )
            )
        );
        $receita_taxonomy_query = new WP_Query($args);
    }
    
    return $receita_taxonomy_query;
}

function idec_receita_get_non_empty($taxonomy_slug) {
    global $receita_taxonomias;
    
    if($receita_taxonomias[$taxonomy_slug]) {
        return $receita_taxonomias[$taxonomy_slug];
    }
    
    $args = array(
        'taxonomy' =>$taxonomy_slug,
        'number'=> 0,
        'hide_empty'=>1,
        'orderby'=>'name',
        'order'=>'ASC'
    );
    $retAr = get_terms($args);

    $ret = array();
    foreach($retAr as $r) {
        $ret[$r->slug] = (object) array(
            'id'=>$r->term_id,
            'slug'=>$r->slug,
            'count'=>$r->count,
            'url'=>get_term_link($r->term_id, $taxonomy_slug),
            'label'=>$r->name,
            'description'=>$r->description,
            'page_title'=>idec_generate_page_headers('receita', $taxonomy_slug, $r->name)->title
        );
    }
    
    $receita_taxonomias[$taxonomy_slug] = (object) $ret;
    return $receita_taxonomias[$taxonomy_slug];
}

function idec_receita_get_receitas() {
    global $receita_items;
    
    if($receita_items) {
        return $receita_items;
    }
    
    $args = array(
        'post_type'=>'receita',
        'numberposts'=>-1,
    );
    
    $posts = get_posts($args);
    
    foreach ($posts as $post) {
        $receita_items[$post->ID] = array(
            'id'=>$post->ID,
            'title'=>$post->post_title,
            'description'=>$post->post_content,
            'slug'=>$post->post_name,
            'alimento'=>wp_get_post_terms($post->ID,'alimento',array("fields" => "slugs")),
            'tipo-de-receita'=>wp_get_post_terms($post->ID,'tipo-de-receita',array("fields" => "slugs")),
            'url'=>get_permalink($post->ID),
            'image_url'=>(has_post_thumbnail($post->ID))
                ? get_the_post_thumbnail_url( $post->ID, 'thumbnail' )
                : '/wp-content/themes/IDEC_feiras/images/45332.svg'
        );
    }
    
    return $receita_items;
}

function idec_receita_show_item_categories($post_id, $type, $class="") {
    $terms = get_the_terms($post_id, $type);
    $html = "";
    $class = ($class) ? " class=\"$class\"" : "";
	foreach ($terms as $term) {
		$link = get_term_link($term, $type);
		$html .= '
		    <div'.$class.'>
		        <a class="ajaxifythis" href="'.$link.'" data-item-type="'.$type.'" data-item-id="'.$term->slug.'" data-item-is_overlay=0>
		            '.$term->name.'
		        </a>
	        </div>
        ';
	}
	return $html;
}

?>
