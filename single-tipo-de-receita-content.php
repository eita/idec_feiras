<?php 
    $pL = idec_pageLocation();
    $term = get_term_by('slug', $pL->slug, $pL->type);
    $closeit = idec_get_template_model('general-closeit', 'content', array('see_all'=>'ver todas as receitas', 'homeurl'=>idec_get_page_type_homeurl()));
    $query = idec_receita_get_taxonomy_query($pL->paged);
    $content_archive = "";
    if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
        $image_url = (has_post_thumbnail())
            ? get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' )
            : '/wp-content/themes/IDEC_feiras/images/45332.svg';
        $content_archive .= idec_get_template_model('general-archive', 'item-content', array('item_id'=>get_the_ID(), 'item_url'=>get_the_permalink(), 'image_url'=>$image_url, 'image_title'=>get_the_title(), 'extra_image_class'=>'', 'title'=>get_the_title()));
    endwhile; endif;
    $count = idec_generate_count_results_text('idec-receita-count', $term->count, 'receita', 'do tipo '.$term->name, $pL->paged, $query->max_num_pages);
    $pagination = idec_custom_pagination($query->max_num_pages,$pL->paged,$pL->href);
?>

<?= idec_get_template_model('general-archive', 'content', array('closeit'=>$closeit, 'image'=>'', 'title'=>$term->name, 'intro'=>'', 'count'=>$count[0], 'whereami'=>$count[1], 'content_archive'=>$content_archive, 'pagination'=>$pagination)); ?>
