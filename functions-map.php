<?php

// Global vars:
$dias_da_semana = array(
	'segunda' => 'Segunda-feira',
	'terca' => 'Terça-feira',
	'quarta' => 'Quarta-feira',
	'quinta' => 'Quinta-feira',
	'sexta' => 'Sexta-feira',
	'sabado' => 'Sábado',
	'domingo' => 'Domingo',
);

add_action( 'default_hidden_meta_boxes', 'remove_cidades_meta_box', 10, 2 );
function remove_cidades_meta_box( $hidden, $screen ) {
	if ( 'item' == $screen->id ) {
		$hidden = array(
			'cidadediv', 'types-information-table',
    		);

	}
	return $hidden;
}

// updates the wpcf_field, which synthesizes the date and time in a single field
add_action( 'save_post_item', 'update_wpcf_horario', 10, 3 );

function update_wpcf_horario ( $id = NULL, $post = NULL, $update = NULL) {
  global $dias_da_semana;

	$args = array(
        'post_type' => array( 'item' ),
        'posts_per_page'         => '-1',
	);
	if ($id) {
		$args['p'] = $id;
	}

	// The Query
	$query = new WP_Query( $args );

	if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
		//$meta = get_post_meta ( get_the_ID() );

		$horario = NULL;

		$dia_cat = NULL;
		foreach (wp_get_object_terms( get_the_ID(), 'dia' ) as $dia)  {
			$dia_cat[] = $dia->name;
		}

		foreach (wp_get_object_terms( get_the_ID(), 'dia' ) as $dia)  {
			$diadasemana = $dia->name;

			if ($id){
				if (isset($_POST['wpcf'][ $dia->slug . '_horario'])) {
					$diadasemana .= ", " . $_POST['wpcf'][$dia->slug . '_horario'];
				}
			} else {
				if ($h = get_post_meta( get_the_ID(), 'wpcf-' . $dia->slug . '_horario', True)) {
					$diadasemana .= ", " . $h;
				}
			}

			$horario[] = $diadasemana;
		}
		$horario = idec_order_by_day(implode("<br />", $horario));

		if ($id){
			if ($_POST['wpcf']['observacoes-sobre-o-horario'] != "") {
				$horario .= "<br>" . $_POST['wpcf']['observacoes-sobre-o-horario'];
			}
		} else {
			if ($o = get_post_meta( get_the_ID(), 'wpcf-observacoes-sobre-o-horario', True)) {
				$horario .= "<br>" . $o;
			}
		}

		$ret= update_post_meta( get_the_ID(), 'wpcf-horario', $horario);
		endwhile;
	endif;
	wp_reset_postdata();
}

function idec_order_by_day($html, $sep = '<br />') {
    global $dias_da_semana;
    $horarios = explode($sep, $html);
    $ordered = array();
    foreach($horarios as $horario) {
        $i=0;
        foreach($dias_da_semana as $dia) {
            if(strpos($horario, $dia)!==false) {
                $ordered[$i] = $horario;
	    }
            $i++;
        }
    }
    ksort($ordered);
    return implode($sep, $ordered);
}

function get_markers($tipo=null, $dia=null, $periodo=null, $s="", $limit=null, $orderby=null, $cidade=null, $ids=null, $selo=null) {

	$args = array();

	$args['post_type'] = 'item';
	$args['post_status'] = 'publish';

	if($ids) {
		$args['include'] = $ids;
	}

	if($orderby) {
		$args['orderby'] = $orderby;
		$args['order'] = 'ASC';
	}

	if($limit != null && $limit != "undefined"){
		$args['posts_per_page'] = $limit;
	} else {
		$args['posts_per_page'] = -1;
	}

	if($cidade != null && $cidade != 'all'){
		$args['tax_query'] = array(
		    array(
			    'taxonomy' => 'cidade',
			    'field'    => 'slug',
			    'terms'    => $cidade
		    )
	    );
	}

	if($tipo != null && $tipo != 'all' && $tipo != "undefined"){
		$args['tipo'] = $tipo;
	}

	if($selo != null && $selo != 'all' && $selo != "undefined"){
		$args['selo'] = $selo;
	}

	if($dia != null && $dia != 'all' && $dia != "undefined"){
		$args['dia'] = $dia;
	}

	if($periodo != null && $periodo != 'all' && $periodo != "undefined"){
		$args['periodo'] = $periodo;
	}

    $locais = array();

    // SEARCH QUERY: the result goes to autocomplete:
	if($s){
        $matching_ids = array();
        $statesRaw = array();
		//$args['s'] = $s;
		$args['title_filter'] = $s;
		$isLongEnough = (strlen($s)>2);

		// Query on states shortname:
		$matching_states = get_terms(array(
		    'taxonomy' =>'uf',
		    'search'=>$s,
		    'orderby'=>$orderby,
		    'order'=>'ASC'
	    ));

	    foreach($matching_states as $state) {
	        if (!$statesRaw[$state->slug]) {
	            $longname = html_entity_decode($state->description, ENT_COMPAT, 'UTF-8');
	            //$label = $longname . " (".$state->name.")";
	            $label = "Estado ".idec_get_location_preposition($longname, false);

	            $t = array(
	                'name' => $state->name,
	                'longname' => $longname,
	                'label' => $label,
	                'value' => 'uf_'.$state->term_id,
	                'id' => $state->term_id,
	                'slug' => $state->slug,
	                'count' => count_publish ($state->term_id, 'uf' ),
	                'type' => 'uf',
	                'url' => get_term_link($state->term_id, 'uf'),
	                'title' => idec_generate_page_headers('map', 'uf', $label)->title,
	            );
	            $statesRaw[$state->slug] = (object) $t;
            }
	    }

		if ($isLongEnough) {

		    // Now the query on states fullname:
		    $matching_states = get_terms(array(
		        'taxonomy' =>'uf',
		        'description__like'=>$s,
		        'orderby' => $orderby,
		        'order' => 'ASC',
	        ));

	        foreach($matching_states as $state) {
	            $longname = html_entity_decode($state->description, ENT_COMPAT, 'UTF-8');
	            //$label = $longname . " (".$state->name.")";
	            $label = "Estado ".idec_get_location_preposition($longname, false);
	            $t = array(
	                'name' => $state->name,
	                'longname' => $longname,
	                'label' => $label,
	                'value' => "uf_".$state->term_id,
	                'id' => $state->term_id,
	                'slug' => $state->slug,
	                'count' => count_publish ($state->term_id, 'uf' ),
	                'type' => "uf",
	                'url' => get_term_link($state->term_id, 'uf'),
	                'title' => idec_generate_page_headers('map', 'uf', $label)->title,
                );
	            $statesRaw[$state->slug] = (object) $t;
	        }

	        // Query on regions:
		    $regionsRaw = array();
		    $REGIONS = idec_get_regions();
		    foreach($REGIONS as $slug=>$region) {
		        if(strpos($slug, sanitize_title($s))!==false) {
		            $t = array(
	                    'name' => $region['name'],
	                    'label' => $region['name'],
	                    'value' => 'region_'.$slug,
	                    'id' => $slug,
	                    'slug' => $slug,
	                    'type' => 'region',
	                    'url' => '',
	                    'title' => $region['page_title'],
                        'count' => ''
	                );
	                $regionsRaw[$slug] = (object) $t;
		        }
		    }

		    ksort($regionsRaw);

	        if (count($regionsRaw)>0) {
                $s = array(
                    'label' => 'Regiões',
	                'value' => 'region',
	                'type' => "header"
                );
	            $t = (object) $s;
	            array_unshift($regionsRaw,$t);
            }


	        // Now the query on cities:
		    $matching_cities = get_terms(array(
		        'taxonomy' =>'cidade',
		        'search'=>$s,
		        'number'=> ($limit) ? $limit : 0,
		        'hide_empty'=>0,
		        'orderby'=>$orderby,
		        'order'=>'ASC'
	        ));

	        if (count($matching_cities)>0) {
                $t = array(
                    'label' => 'Cidades',
        	        'value' => 'cidades',
        	        'type' => 'header'
                );
    	        $locais[] = (object) $t;
            }

	        foreach($matching_cities as $city) {
	            $label = html_entity_decode($city->name, ENT_COMPAT, 'UTF-8');
	            $cityAndState = idec_get_name_and_uf_from_label($label);
	            $t = array(
	                'label' => $label,
	                'uf' => $cityAndState->uf,
	                'slug' => $city->slug,
	                'count' => $city->count,
	                'value' => "city_".$city->term_id,
	                'id' => $city->term_id,
	                'type' => "cidade",
	                'url' => get_term_link($city->term_id, 'cidade'),
	                'title' => idec_generate_page_headers('map', 'cidade', $label)->title,
	            );
	            $locais[] = (object) $t;
	        }



		    // Now the query on items, excluding meta data:
		    $hasHeader = false;

		    // They will be queried only by title:
		    add_filter( 'posts_where', 'idec_title_filter', 10, 2 );
            $items = new WP_Query($args);
            remove_filter( 'posts_where', 'idec_title_filter', 10, 2 );

		    if ( $items->have_posts() ) : while ( $items->have_posts() ) : $items->the_post();
			    $id = get_the_ID();
			    $latlng = get_post_meta($id, 'wpcf-latlng', true );
		        $latlng = str_replace(array("(",")"," "), '', $latlng);
		        list($lat,$lng) = explode(',', $latlng);
		        $city = idec_get_city($id);

			    if($id && $lat && $lng && $city && $city->name) {
			        if (!$hasHeader) {
			            $t = array(
                            'label' => 'Iniciativas',
                	        'value' => 'iniciativas',
                	        'type' => 'header'
                        );
            	        $locais[] = (object) $t;
            	        $hasHeader = true;
		            }

			        $t = array(
                        'label' => html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'),
            	        'value' => $id,
            	        'id' => $id,
            	        'city' => $city->name,
            	        'type' => 'item',
            	        'lat' => $lat,
            	        'lng' => $lng,
            	        'url' => get_permalink($id),
            	        'title' => idec_generate_page_headers('map', 'item', $id)->title,
            	        'address' => idec_remove_city_from_address(get_post_meta($id, 'wpcf-address-custom', true), $city->name)
                    );
        	        $locais[] = (object) $t;
        	        $matching_ids[] = $id;
			    }

		    endwhile; endif;



		    // Now the query on items on metadata (address):
		    //unset($args['s']);
		    unset($args['title_filter']);
		    if(count($matching_ids)) {
		        $args['post__not_in'] = $matching_ids;
		    }
		    $args['meta_query'] = array(
        		array(
		        	'key'     => 'wpcf-address-custom',
		        	'value'   => $s,
		        	'compare' => 'LIKE',
		        )
	        );

		    $items = new WP_Query($args);

		    if ( $items->have_posts() ) : while ( $items->have_posts() ) : $items->the_post();

			    $id = get_the_ID();
			    $latlng = get_post_meta($id, 'wpcf-latlng', true );
		        $latlng = str_replace(array("(",")"," "), '', $latlng);
		        list($lat,$lng) = explode(',', $latlng);
		        $city = idec_get_city($id);

			    if($id && $lat && $lng && $city && $city->name) {
			        if (!$hasHeader) {
			            $t = array(
                            'label' => 'Iniciativas',
                	        'value' => 'iniciativas',
                	        'type' => 'header'
                        );
            	        $locais[] = (object) $t;
            	        $hasHeader = true;
		            }

			        $t = array(
                        'label' => html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'),
            	        'value' => $id,
            	        'id' => $id,
            	        'city' => $city->name,
            	        'type' => 'item',
            	        'lat' => $lat,
            	        'lng' => $lng,
            	        'url' => get_permalink($id),
            	        'title' => idec_generate_page_headers('map', 'item', $id)->title,
            	        'address' => idec_remove_city_from_address(get_post_meta($id, 'wpcf-address-custom', true), $city->name)
                    );
        	        $locais[] = (object) $t;
			    }

		    endwhile; endif;

        }

	    ksort($statesRaw);

	    if (count($statesRaw)>0) {
            $s = array(
                'label' => 'Estados',
	            'value' => 'estados',
	            'type' => "header"
            );
	        $t = (object) $s;
	        array_unshift($statesRaw,$t);
        }

	    return array_merge(array_values($regionsRaw), array_values($statesRaw), $locais);


	} else {
	    // This is for populating all markers (markersRaw) upon initialization:
		$items = get_posts( $args );
		foreach ($items as $key => $item) {
		    $city = idec_get_city($item->ID);
			$uf = idec_get_uf($item->ID);
			if($item->ID && $city && $city->slug && $uf && $uf->slug) {
			    $latlng = get_post_meta($item->ID, 'wpcf-latlng', true );
			    $latlng = str_replace(array("(",")"," "), '', $latlng);
			    list($lat,$lng) = explode(',', $latlng);
				$type = idec_get_type($item->ID);
				$selos = idec_get_selos($item->ID);
				$t =array (
					'id' 	=> $item->ID,
					'title' => $item->post_title,
					'lat' 	=> $lat,
					'lng' 	=> $lng,
					'type'	=> $type,
					'types_labels' => idec_get_types_labels($item->ID),
					'selos'	=> $selos,
					'link' 	=> get_permalink($item->ID),
					'address' => idec_remove_city_from_address(get_post_meta($item->ID, 'wpcf-address-custom', true), $city->name),
					'city_slug' 	=> $city->slug,
					'uf_slug' 	=> $uf->slug,
					'region_slug' => idec_get_region_from_uf($uf->slug),
					'datetime' => get_post_meta($item->ID, 'wpcf-horario', true ),
				);
				$locais[] = (object) $t;
			}
		}
	}

	return $locais;

}
function idec_get_all_non_empty($taxonomy_slug, $region_slug=false, $hide_empty=false) {
    $REGIONS = idec_get_regions();
    if($taxonomy_slug=='region' && $region_slug && isset($REGIONS[$region_slug])) {
        $include = $REGIONS[$region_slug]['states_ids'];
        $taxonomy_slug = 'uf';
    }
    $args = array(
        'taxonomy' =>$taxonomy_slug,
        'number'=> 0,
        'hide_empty'=>$hide_empty,
        'orderby'=>'name',
        'order'=>'ASC'
    );
    if(isset($include)) {
        $args['include'] = $include;
    }
    $retAr = get_terms($args);

    $ret = array();
    foreach($retAr as $r) {
        $ret[$r->slug] = array(
            'id'=>$r->term_id,
            'slug'=>$r->slug,
            'count'=>$r->count,
            'url'=>get_term_link($r->term_id, $taxonomy_slug)
        );
        $bounds = json_decode(get_term_meta($r->term_id, 'bounds')[0]);
        if ($bounds) {
            $ret[$r->slug]['bounds'] = $bounds;
        }
        if($taxonomy_slug=='uf') {
            $label = $r->description;
            $ret[$r->slug]['label'] = $label;
            $ret[$r->slug]['page_title'] = idec_generate_page_headers('map', 'uf', $r->slug)->title;
            $ret[$r->slug]['name'] = $r->name;
            $ret[$r->slug]['region_slug'] = idec_get_region_from_uf($r->slug);
            $ret[$r->slug]['src'] = idec_get_local_geojson_dir().'estados/geojson/'.$r->name.'.json';
            $ret[$r->slug]['address'] = "Estado ".idec_get_location_preposition($label, false).", BR";
        } else {
            $ret[$r->slug]['label'] = $r->name;
            $ret[$r->slug]['page_title'] = idec_generate_page_headers('map', 'cidade', $r->slug)->title;
            $cityAndState = idec_get_name_and_uf_from_label($r->name);
            $ret[$r->slug]['name'] = $cityAndState->name;
            $ret[$r->slug]['uf_slug'] = strtolower($cityAndState->uf);
            $ret[$r->slug]['src'] = idec_get_local_geojson_dir()."municipios/".$cityAndState->uf."/geojson/".strtoupper(str_replace('-','_',sanitize_title($cityAndState->name))).'.json';
            $ret[$r->slug]['address'] = $cityAndState->name.", ".$cityAndState->uf.", BR";
        }
    }
    return $ret;
}

function idec_get_type($id, $slugOnly=true) {
	$types = get_the_terms($id, 'tipo');
	$types = array_values($types);
	$type = ($slugOnly) ? $types[0]->slug : $types[0];
	return $type;
}
function idec_get_all_types() {
    $types = get_terms( array(
        'taxonomy' => 'tipo',
        'hide_empty' => true,
        'orderby' => 'count',
        'order' => 'DESC',
        'exclude_tree' => array (950),
    ));
	return $types;
}
function idec_get_selos($id, $slugOnly=true) {
	$selos = get_the_terms($id, 'selo');
	if (!is_wp_error($selos)) {
		$selos = array_values($selos);
		if ($slugOnly) {
			$selos_arr = NULL;
			foreach ($selos as $selo) {
				$selos_arr[] = $selo->slug;
			}
			$selos = implode("#", $selos_arr);
		}
	} else {
		$selos = NULL;
	}
	
	return $selos;

}
function idec_get_all_selos() {
    $selos = get_terms( array(
        'taxonomy' => 'selo',
        'hide_empty' => true,
        'orderby' => 'count',
        'order' => 'DESC',
        // 'exclude_tree' => array (950),
    ));
	return $selos;
}
function idec_get_types_labels($id) {
	$types = get_the_terms($id, 'tipo');
	$ret = array();
	foreach($types as $t) {
	    $ret[] = $t->name;
	}
	return implode(', ', $ret);
}
function idec_get_uf($id) {
	$types = get_the_terms($id, 'uf');
	if(!$types) {
	    return false;
	}
	$types = array_values($types);
	$uf = $types[0];
	return $uf;
}
function idec_get_city($id) {
	$cities = get_the_terms($id, 'cidade');
	$city = $cities[0];
	return $city;
}
function idec_remove_city_from_address($address, $city) {
    // TODO: Remove cities from addresses in database so that we
    // don't need to do this dirty job below!
    $addressWithoutCity = str_replace(array($city, "Brasil"), "", $address);
    $addressWithoutCity = trim(str_replace(array(", ,","  "),array(","," "),$addressWithoutCity));
    while(strpos($addressWithoutCity,",,")!==false) {
        $addressWithoutCity = str_replace(array(",, ",",,"),array(", ",","),$addressWithoutCity);
    }
    if(substr($addressWithoutCity,-1)==',') {
        $addressWithoutCity = substr($addressWithoutCity,0,-1);
    }
    return $addressWithoutCity;
}
function idec_get_days($id) {
	$types = get_the_terms($id, 'dia');
	$types = array_values($types);
	return $types;
}
function idec_get_periodos($id) {
	$types = get_the_terms($id, 'periodo');
	$types = array_values($types);
	return $types;
}
function idec_get_datetime($id) {
	$days = idec_get_days($id);
	$r = NULL;
	foreach($days as $day){
		$r[] = $day->name;
	}
	return implode(", ", $r);
}
function idec_get_produtos($id) {
	$types = get_the_terms($id, 'produto');
	$types = array_values($types);
	return $types;
}
function idec_get_local_geojson_dir() {
    return BASE_URI."/includes/kml-brasil-master/lib/2010/";
}
function idec_get_this_locality_from_term($type) {
    if (!in_array($type, array('cidade', 'uf'))) {
        return false;
    }
    $term = get_term_by('slug', get_query_var('term'), $type);
    return idec_get_locality_from_term($type, $term);
}
function idec_get_locality_from_term($type, $term) {
    if (!in_array($type, array('cidade', 'uf')) || !$term) {
        return false;
    }
    switch($type) {
        case 'cidade':
            list($cidade, $uf) = explode(' - ', $term->name);
            $cityAndState = idec_get_name_and_uf_from_label($term->name);
            $localityData = array(
                'src'=>idec_get_local_geojson_dir()."municipios/".$cityAndState->uf."/geojson/".strtoupper(str_replace('-','_',sanitize_title($cityAndState->name))).'.json',
                'address'=>$cityAndState->name.", ".$cityAndState->uf.", BR",
                'label'=>$term->name,
                'id'=>$term->term_id,
                'slug'=>$term->slug,
            );
            break;
        case 'uf':
            $localityData = array(
                'src'=>idec_get_local_geojson_dir().'estados/geojson/'.$term->name.'.json',
                'address'=>"Estado ".idec_get_location_preposition($term->description, false).", BR",
                'label'=>$term->description,
                'id'=>$term->term_id,
                'slug'=>$term->slug,
            );
            break;
    }
    return (object) $localityData;
}
function idec_get_item_html($showCity=true, $id="{id}", $url="{url}", $type="{type}", $title="{title}", $city="{city}", $address="{address}") {
    $itemHTML = "
        <li data-item-id=\"$id\">
            <a class=\"idec-list-item-content ajaxifythis\" data-item-type=\"item\" data-item-id=\"$id\" href=\"$url\" data-item-is_overlay=1>
                <div>
                    <div class=\"idec-list-item-icon\">
                        <i class=\"mapafeiras_icon-tipo_$type\"></i>
                    </div>
                    <h1 class=\"idec-list-item-title\">$title</h1>
    ";
    if($showCity) {
        $itemHTML .= "
                    <p class=\"idec-list-item-address\">$city</p>
        ";
    } else {
        $itemHTML .= "
        		    <p class=\"idec-list-item-address\">$address</p>
        ";
    }
    $itemHTML .= "
                </div>
            </a>
        </li>
    ";
    return str_replace(array("\n", "  "),"",$itemHTML);
}

function idec_get_location_html($taxonomy_slug="{type}", $url="{url}", $slug="{slug}", $page_title="{page_title}", $src="{src}", $address="{address}", $name="{name}", $count="{count}") {
    $html = "
        <li class=\"with-separator\">
            <a class=\"idec-list-item-content\" href=\"$url\" onclick=\"event.preventDefault();goToThisLocation(this);\" data-slug=\"$slug\" data-type=\"$taxonomy_slug\" data-page_title=\"$page_title\">
                <div>
                    <div class=\"idec-list-item-icon\">$count</div>
                    <h1 class=\"idec-list-item-title\">$name</h1>
                </div>
            </a>
        </li>
    ";
    return str_replace(array("\n", "  "),"",$html);
}

function ajax_get_markers() {
	$tipo = $_POST['tipo'];
	$dia = $_POST['dia'];
	$periodo = $_POST['periodo'];
	$s = $_POST['s'];
	$cidade = $_POST['cidade'];
	$ids = $_POST['ids'];
	$orderby = isset($_POST['orderby'])?$_POST['orderby']:'post_title';
	$limit = $_POST['limit'];
	$locais = get_markers($tipo, $dia, $periodo, $s, $limit, $orderby, $cidade, $ids);

	echo json_encode($locais);
    wp_die();
}

function fn_opening_days($atts, $contents) {
	return "<h2>Dias e Horários</h2><div class='opening_days'>".do_shortcode($contents)."</div>";
}

function fn_opening_day($atts, $contents) {
	$disabled = ($atts['disabled']==true) ? 'disabled' : '';
	ob_start();
	?>
	<div class="day <?php echo $disabled; ?>"><?php echo $atts['text']; ?></div><div class="day-infos"><?php echo $contents; ?></div>
	<?php
	$contents = ob_get_contents();
	ob_end_clean();

	return $contents;
}


function idec_comments_form_defaults($default) {
	unset($default['comment_notes_after']);
	unset($default['title_reply']);
	return $default;
}


function idec_comments_form_fields($fields){

	$fields['url'] = '';

	$fields['author'] = str_replace(
        '<input',
        '<input placeholder="'
        /* Replace 'theme_text_domain' with your theme’s text domain.
         * I use _x() here to make your translators life easier. :)
         * See http://codex.wordpress.org/Function_Reference/_x
         */
            . _x(
                'Nome',
                'comment form placeholder',
                'idec'
                )
            . '"',
        $fields['author']
    );

    $fields['email'] = str_replace(
        '<input id="email" name="email" type="text"',
        /* We use a proper type attribute to make use of the browser’s
         * validation, and to get the matching keyboard on smartphones.
         */
        '<input type="email" placeholder="Email"  id="email" name="email"',
        $fields['email']
    );

	return $fields;
}


function season_content($season, $regiao) {

	//define as páginas com conteudos de cada estacao
	$seasons = array(
	'primavera' => array(
		'norte' 		=> 49,
		'nordeste' 		=> 65,
		'centro-oeste' 	=> 73,
		'sudeste' 		=> 39,
		'sul' 			=> 57
		),
	'verao' => array(
		'norte' 		=> 51,
		'nordeste' 		=> 67,
		'centro-oeste' 	=> 76,
		'sudeste' 		=> 42,
		'sul' 			=> 59
		),
	'outono' => array(
		'norte' 		=> 53,
		'nordeste' 		=> 69,
		'centro-oeste' 	=> 78,
		'sudeste' 		=> 45,
		'sul' 			=> 61
		),
	'inverno' => array(
		'norte' 		=> 55,
		'nordeste' 		=> 71,
		'centro-oeste' 	=> 80,
		'sudeste' 		=> 47,
		'sul' 			=> 63
		)
	);

	$post = get_post( $seasons[$season][$regiao] );
	if($post) {

		echo apply_filters('the_content', $post->post_content);
	} else {
		echo '';
	}

}

//ADD SHORTCODES SUPPORT TO CONTACT FORM 7
function mycustom_wpcf7_form_elements( $form ) {
	$form = do_shortcode( $form );

	return $form;
}
//END ADD SHORTCODES SUPPORT TO CONTACT FORM 7


function fn_select_states() {
	global $wpdb;
	$results = $wpdb->get_results( 'SELECT nome, sigla FROM estados', OBJECT );
	ob_start();
	?>
	<select name="uf" class="uf-select" id="estado">
		<?php foreach ($results as $key => $estado) {
			?><option value="<?php echo $estado->sigla; ?>"><?php echo $estado->nome; ?></option><?php
		} ?>

	</select>
	<?php
	$contents = ob_get_contents();
	ob_end_clean();

	return $contents;
}

function fn_checkbox_produtos() {
	$produtos = array(
		"cereais"=>"Cereais",
		"frutas-legumes-e-verduras"=>"Frutas, Legumes e Verduras",
		"laticinios"=>"Laticínios",
		"leguminosas"=>"Leguminosas (feijões, lentilha, grão de bico, ervilha, etc.)",
		"ovos"=>"Ovos",
		"panificados"=>"Panificados (pães, bolos, tortas, etc.)",
		"outros"=>"Outros",
		);

	ob_start();

	foreach ($produtos as $key => $prod) {
		// $nome = $prod->name;
		// $slug = $prod->slug;
		$nome = $prod;
		$slug = $key;
		?><div class="block-item"><label><input type="checkbox" name="produtos[]" value="<?php echo $slug; ?>"> <?php echo $nome; ?></label></div><?php
	}
	?>

	<?php
	$contents = ob_get_contents();
	ob_end_clean();

	return $contents;
}

function fn_radio_faixaetaria() {

	$options = array(
		 "Até 19 anos",
		 "De 20 a 29 anos",
		 "De 30 a 39 anos",
		 "De 40 a 49 anos",
		 "De 50 a 59 anos",
		 "Acima de 60 anos",
	);

	return idec_print_radio ( 'faixa-etaria' , $options );
}

function fn_radio_regiao() {

	$options = array(
		 "Norte",
		 "Nordeste",
		 "Centro-Oeste",
		 "Sudeste",
		 "Sul",
	);

	return idec_print_radio ( 'regiao' , $options );
}

function fn_radio_sexo() {

	$options = array(
		"Outro",
		"Masculino",
		"Feminino",
	);

	return idec_print_radio ( 'sexo' , $options );
}

function fn_radio_escolaridade() {

	$options = array(
		"Até o Ensino Médio incompleto",
		"Ensino Médio completo",
		"Superior completo",
		"Pós-graduado"
	);

	return idec_print_radio ( 'escolaridade' , $options );
}

function fn_radio_participacao_na_feira() {

	$options = array(
		"Consumidor",
		"Feirante ou gestor da feira",
	);

	return idec_print_radio ( 'participacao-na-feira' , $options );
}

function fn_radio_onde_ocorre_a_feira() {

	$options = array(
		"Rua ou avenida",
		"Praça",
		"Parque",
		"Espaços educacionais (universidade, escola e outros)",
		"Outros espaços. Qual?"
	);

	$other = array ('outros-espacos', 'Outros espaços. Qual?');

	return idec_print_radio ( 'onde-ocorre-a-feira' , $options , NULL , $other);
}

function idec_print_radio ( $name, $options , $style = 'style="display: inline-block; margin-right: 10px"' , $other = NULL) {

	ob_start();
	$first = true;
	foreach ($options as $option) {	?>
		<div class="block-item" <?= $style ?>><label><input <?= ($first)?'checked="checked"':''; ?> type="radio" name="<?= $name ?>" value="<?= $option; ?>"> <?= $option; ?></label></div>
	<?php
		$first = false;
	}

	if ( $other ) { ?>
		<div class="form-item form-item-wide"><span class="wpcf7-form-control-wrap <?= $other[0]; ?>"><input name="<?= $other[0]; ?>" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" type="text"></span></div>
	<?php }

		$contents = ob_get_contents();
		ob_end_clean();

	return $contents;
}


function fn_get_brazil_cities() {
	$uf= $_POST['uf'];
	global $wpdb;
	$results = $wpdb->get_results( 'SELECT c.nome, e.sigla FROM estados as e, cidades as c WHERE c.estado_id = e.id AND e.sigla = "'.$uf.'"', OBJECT );
	$cities = array();
	foreach ($results as $key => $city) {
		$cities[] = $city->nome;
	}
	echo json_encode($cities);
	die();
}

function fn_get_edit_id($atts, $contents){
	$edit_id = $_GET['edit_id'];
	return $edit_id;
}

function fn_get_url($atts, $contents){
	global $post;
	// $url = get_permalink($post->ID);
	$url = $_GET['url'];
	return $url;
}


function show_item_data_in_form() {
	$edit_id = $_POST['edit_id'];

	$titulo = get_the_title($edit_id);
	$endereco_custom = get_post_meta($edit_id, 'wpcf-address-custom', true );
	$endereco = get_post_meta($edit_id, 'wpcf-address', true );
	$latlng = get_post_meta($edit_id, 'wpcf-latlng', true );
	$uf = idec_get_uf($edit_id);
	$post_object = get_post( $edit_id );
	$content = $post_object->post_content;

	$values = array(
		'titulo' => $titulo,
		'address-custom' => $endereco_custom,
		'address' => $endereco,
		'latlng' => $latlng,
		'tipo' => idec_get_type($edit_id),
		'uf' => $uf->name,
		'cidade' => idec_get_city($edit_id),
		'dias' => idec_get_days($edit_id),
		'horario' => get_post_meta($edit_id, 'wpcf-horario', true ),
		'periodos' => idec_get_periodos($edit_id),
		'produtos' => idec_get_produtos($edit_id),
		'referencia' => get_post_meta($edit_id, 'wpcf-referencia', true ),
		'descricao' => $content,
		'cafe' => get_post_meta($edit_id, 'wpcf-cafe-da-manha', true ),
		'contato' => get_post_meta($edit_id, 'wpcf-contato', true )
		);

	echo json_encode($values);
    wp_die();
}

/*
TODO: Change the way users inform about errors or updates of items!
add_action('transition_post_status','idec_publish_item',10,3);
*/
function idec_publish_item($new_status,$old_status,$post){
	if ($post->post_type == 'revisao' && $new_status == 'publish'){

		$edit_id = get_post_meta($post->ID, '_wpcf_belongs_item_id', true );
		$revision_id = $post->ID;

		//metas
		$metas_updates = array(
			'wpcf-nome',
			'wpcf-email',
			'wpcf-telefone',
			'wpcf-associado-idec',
			'wpcf-address-custom',
			'wpcf-horario',
			'wpcf-cafe-da-manha',
			'wpcf-referencia',
			'wpcf-contato',
			'wpcf-address',
			'wpcf-latlng'
			);

		foreach ($metas_updates as $key => $m_value) {
			update_post_meta( $edit_id, $m_value, get_post_meta( $revision_id, $m_value, true) );
		}

		//taxonomies
		$dias = wp_get_object_terms($revision_id, 'dia', array('fields' => 'slugs'));
		$periodo = wp_get_object_terms($revision_id, 'periodo', array('fields' => 'slugs'));
		$cidade = wp_get_object_terms($revision_id, 'cidade', array('fields' => 'slugs'));
		$uf = wp_get_object_terms($revision_id, 'uf', array('fields' => 'slugs'));
		$produtos = wp_get_object_terms($revision_id, 'produto', array('fields' => 'slugs'));
		$regiao = wp_get_object_terms($revision_id, 'regiao', array('fields' => 'slugs'));


	    wp_set_object_terms( $edit_id, idec_get_type($revision_id), 'tipo', false);
	    wp_set_object_terms( $edit_id, $uf, 'uf', false);
	    wp_set_object_terms( $edit_id, $cidade, 'cidade', false);
	    wp_set_object_terms( $edit_id, $dias, 'dia', false);
	    wp_set_object_terms( $edit_id, $periodo, 'periodo', false);
	    wp_set_object_terms( $edit_id, $produtos, 'produto', false);
	    wp_set_object_terms( $edit_id, $regiao, 'regiao', false);


	    $revision = get_post( $revision_id );

	    $post_args = array(
	    	'ID' => $edit_id,
	    	'post_content' => $revision->post_content,
	    	'post_title' => $revision->post_title
	    	);
	    wp_update_post($post_args );

	 	if(get_post_thumbnail_id($revision_id)) {

	    	update_post_meta($edit_id, '_thumbnail_id', get_post_thumbnail_id($revision_id) );
	 	}
	}
 }

function idec_get_comments_images($post_id){

	$comments_query = new WP_Comment_Query;
	$comments = $comments_query->query( array(
		'post_id' => $post_id,
		'status' => 'approve',
		'meta_query' => array(array(
			'key' => 'attachmentId'
			)),
		'fields' => 'ids'
		) );

	$images_url = array();
	$thumb_url = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
	if($thumb_url) { $images_url[] = $thumb_url; }

	foreach ($comments as $key => $comment_id) {

		$attachmentId = get_comment_meta( $comment_id, 'attachmentId', true );
		$attachmentUrl = wp_get_attachment_url( $attachmentId );
		$images_url[] = $attachmentUrl;

	}


	return $images_url;

}

function idec_get_location_preposition($locationLabel, $em=true) {
    $cityAndState = idec_get_name_and_uf_from_label($locationLabel);
    $location = $cityAndState->name;
    $uf = $cityAndState->uf;
    $locationSlug = sanitize_title($location);
    $locationDefinitePrepositions = array(
        'rio-de-janeiro'=>'o',
        'recife'=>'o',
        'acre'=>'o',
        'amapa'=>'o',
        'amazonas'=>'o',
        'bahia'=>'a',
        'ceara'=>'o',
        'espirito-santo'=>'o',
        'maranhao'=>'o',
        'para'=>'o',
        'paraiba'=>'a',
        'parana'=>'o',
        'piaui'=>'o',
        'rio-grande-do-norte'=>'o',
        'rio-grande-do-sul'=>'o',
        'tocantins'=>'o',
        'distrito-federal'=>'o'
    );
    $a = ($em) ? array("n", "em") : array("d", "de");
    $ret = (isset($locationDefinitePrepositions[$locationSlug]))
        ? $a[0].$locationDefinitePrepositions[$locationSlug]." $location"
        : $a[1]." $location";
    return $ret;
}

function idec_get_location_stats_html() {
    $html = '
            <div class="locations_stats">
                <div class="locations_stats-header"></div>
                <div class="locations_stats-subheader"></div>
                <div class="locations_stats-body">
                    <ul id="locationStatsList" class="items_list">
                    </ul>
                </div>
            </div>
    ';
    return str_replace(array("\n", "  "),"",$html);
}

function idec_get_stats_from_uf($term_id) {
    $args = array(
        'post_type'=>'item',
        'posts_per_page'=>-1,
        'tax_query' => array(
		    array(
			    'taxonomy' => 'uf',
			    'field'    => 'term_id',
			    'terms'    => $term_id
		    )
	    )
    );

    $items = new WP_Query($args);
    $stats = array(
        "count"=>$items->post_count
    );
    $cities = array();
    $count = array();

    if ( $items->have_posts() ) : while ( $items->have_posts() ) : $items->the_post();

	    $id = get_the_ID();

	    if($id) {
	        $include[] = $id;
	        $label = html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8');
	        $city = idec_get_city($id);
	        if($city && $city->term_id) {
	            $localityData = idec_get_locality_from_term('cidade', $city);
	            $cityAndState = idec_get_name_and_uf_from_label($city->name);
	            if (!isset($cities[$city->slug])) {
	                $cities[$city->slug] = array(
	                    'slug'=>$city->slug,
	                    'label'=>$city->name,
	                    'name'=>$cityAndState->name,
	                    'id'=>$city->term_id,
	                    'src'=>$localityData->src,
	                    'address'=>$localityData->address,
	                    'count'=>$city->count,
	                    'url'=>get_term_link($city->term_id,'cidade')
	                );
	                $count[$city->slug] = $city->count;
	            }
	        }
	    }

    endwhile; endif;

    $ret = (object) $stats;
    $ret->cities = array();
    arsort($count);
    foreach($count as $slug=>$c) {
        $ret->cities[] = (object) $cities[$slug];
    }
    return $ret;
}

function idec_get_stats_from_region($region_slug) {
    $statesAr = idec_get_all_non_empty('region', $region_slug);

    $stats = array(
        "count"=>0
    );

    $states = array();
    foreach($statesAr as $slug=>$state) {
        $stats['count'] += $state['count'];
        $states[] = (object) $state;
    }
    $stats['states'] = $states;

    return (object) $stats;
}

function idec_get_regions() {
    return array(
        'norte'=>array('name'=>'Norte', 'page_title'=>'Região Norte do Brasil', 'address'=>'Região Norte', 'slug'=>'norte', 'alias'=>'N', 'states_ids'=>array("52","80","50","46","100","54","646"), 'states_slugs'=>array('ro','ac','am','rr','pa','ap','to')),
        'nordeste'=>array('name'=>'Nordeste', 'page_title'=>'Região Nordeste do Brasil', 'address'=>'Região Nordeste', 'slug'=>'nordeste', 'alias'=>'NE', 'states_ids'=>array("86","82","76","48","66","84","78","98","7"), 'states_slugs'=>array('ma','pi','ce','rn','pb','pe','al','se','ba')),
        'centro-oeste'=>array('name'=>'Centro-Oeste', 'page_title'=>'Região Centro-Oeste do Brasil', 'address'=>'Região Centro-Oeste', 'slug'=>'centro-oeste', 'alias'=>'CO', 'states_ids'=>array("59","40","42","595"), 'states_slugs'=>array('ms','mt','go','df')),
        'sudeste'=>array('name'=>'Sudeste', 'page_title'=>'Região Sudeste do Brasil', 'address'=>'Região Sudeste', 'slug'=>'sudeste', 'alias'=>'SE', 'states_ids'=>array("62","9","91","74"), 'states_slugs'=>array('mg','es','rj','sp')),
        'sul'=>array('name'=>'Sul', 'page_title'=>'Região Sul do Brasil', 'address'=>'Região Sul', 'slug'=>'sul', 'alias'=>'S', 'states_ids'=>array("69","44","71"), 'states_slugs'=>array('pr','sc','rs'))
    );
}

function idec_get_region_from_uf($uf) {
    $ufs = array('ro'=>'norte', 'ac'=>'norte', 'am'=>'norte', 'rr'=>'norte', 'pa'=>'norte', 'ap'=>'norte', 'to'=>'norte', 'ma'=>'nordeste', 'pi'=>'nordeste', 'ce'=>'nordeste', 'rn'=>'nordeste', 'pb'=>'nordeste', 'pe'=>'nordeste', 'al'=>'nordeste', 'se'=>'nordeste', 'ba'=>'nordeste', 'ms'=>'centro-oeste', 'mt'=>'centro-oeste', 'go'=>'centro-oeste', 'df'=>'centro-oeste', 'mg'=>'sudeste', 'es'=>'sudeste', 'rj'=>'sudeste', 'sp'=>'sudeste', 'pr'=>'sul', 'sc'=>'sul', 'rs'=>'sul');
    return $ufs[$uf];
}

function idec_get_name_and_uf_from_label($label) {
    $tmp = explode(' - ', $label);
    $ret = array(
        'name'=>$tmp[0],
        'uf'=>(isset($tmp[1])) ? $tmp[1] : ""
    );
    return (object) $ret;
}


function ajaxify_comments_jaya($comment_ID, $comment_status) {
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        //If AJAX Request Then
        switch ($comment_status) {
            case '0':
                //notify moderator of unapproved comment
                wp_notify_moderator($comment_ID);
            case '1': //Approved comment
                echo "success";
                $commentdata = &get_comment($comment_ID, ARRAY_A);
                //print_r( $commentdata);
                $permaurl = get_permalink( $post->ID );
                $url = str_replace('http://', '/', $permaurl);

                if($commentdata['comment_parent'] == 0){
                    $output = '
                        <li class="comment byuser comment-author-admin bypostauthor odd alt thread-odd thread-alt depth-1" id="comment-' . $commentdata['comment_ID'] . '">
                            <div id="div-comment-'.$commentdata['comment_ID'].'" class="comment-body">
                                <div class="comment-author vcard">
                                    '.get_avatar($commentdata['comment_author_email']).'<cite class="fn">'.$commentdata['comment_author'].'</cite> <span class="says">says:</span>
                                </div>
                            <div class="comment-meta commentmetadata">
                                <a href="http://localhost/WordPress_Code/?p=1#comment-'.$commentdata['comment_ID'] .'">
                                    '.get_comment_date( 'F j, Y \a\t g:i a', $commentdata['comment_ID']) .'
                                </a>&nbsp;&nbsp;
                    ';
                    if ( is_user_logged_in() ){
                        $output .= '
                                <a class="comment-edit-link" href="'.home_url().'wp-admin/comment.php?action=editcomment&amp;c='. $commentdata['comment_ID'] .'">
                                    (Edit)
                                </a>
                        ';
                    }
                    $output .= '
                            </div>
                            <p>'.$commentdata['comment_content'].'</p>
                            <div class="reply">
                                <a class="comment-reply-link" href="'.$url. '&amp;replytocom='.$commentdata['comment_ID'].'#respond" onclick="return addComment.moveForm(&quot;div-comment-'.$commentdata['comment_ID'].'&quot;, &quot;'.$commentdata['comment_ID'].'&quot;, &quot;respond&quot;, &quot;1&quot;)">
                                    Reply
                                </a>
                            </div>
                        </div>
                    </li>
                    ' ;
                    echo $output;
                }
                else{
                $output = '<
                    ul class="children">
                        <li class="comment byuser comment-author-admin bypostauthor even depth-2" id="comment-'.$commentdata['comment_ID'].'">
                            <div id="div-comment-'.$commentdata['comment_ID'].'" class="comment-body">
                                <div class="comment-author vcard">
                                    '.get_avatar($commentdata['comment_author_email']).'<cite class="fn">' . $commentdata['comment_author']. '</cite> <span class="says">says:</span>
                                </div>
                                <div class="comment-meta commentmetadata">
                                    <a href="http://localhost/WordPress_Code/?p=1#comment-'.$commentdata['comment_ID'].'">
                                        '.get_comment_date( 'F j, Y \a\t g:i a', $commentdata['comment_ID']).'
                                    </a>&nbsp;&nbsp;
                ';
                if ( is_user_logged_in() ){
                    $output .= '
                                    <a class="comment-edit-link" href="'.home_url().'wp-admin/comment.php?action=editcomment&amp;c='.$commentdata['comment_ID'] .'">
                                        (Edit)
                                    </a>
                    ';
                }
                $output .= '
                                </div>
                                <p>'.$commentdata['comment_content'].'</p>
                                <div class="reply">
                                    <a class="comment-reply-link" href="'.$url.'&amp;replytocom='.$commentdata['comment_ID'].'#respond" onclick="return addComment.moveForm(&quot;div-comment-'.$commentdata['comment_ID'].'&quot;, &quot;'.$commentdata['comment_ID'].'&quot;, &quot;respond&quot;, &quot;1&quot;)">
                                        Reply
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                ';
                echo $output;
                }
                $post = &get_post($commentdata['comment_post_ID']);
                wp_notify_postauthor($comment_ID, $commentdata['comment_type']);
                break;
            default:
                echo "error";
        }
        exit;
    }
}

if (!is_admin()) {
	add_action('comment_post', 'ajaxify_comments_jaya', 25, 2);
}

/* configure API */
function idec_rest_prepare_item( $response, $post = null ) {
	$city = idec_get_city($post->ID);
	$cityAndState = idec_get_name_and_uf_from_label($city->name);
	$response->data['cidade'] = $cityAndState->name;
	$response->data['uf'] = $cityAndState->uf;
	$response->data['address'] = idec_remove_city_from_address(get_post_meta($post->ID, 'wpcf-address', true), $cityAndState->name);
	$response->data['horario'] = idec_order_by_day(get_post_meta($post->ID, 'wpcf-horario', true ));
	$avatar = idec_get_post_image($post->ID);
	if ($avatar[0] && $response->data['featured_media']!=0) {
		$response->data['avatar'] = $avatar[0];
	}
	$type = idec_get_type($post->ID);
	$response->data['tipo'] = $type;
	$latlng = get_post_meta($post->ID, 'wpcf-latlng', true );
	$latlng = str_replace(array("(",")"," "), '', $latlng);
	list($response->data['lat'],$response->data['lng']) = explode(',', $latlng);
	unset($response->data['content'], $response->data['dia'], $response->data['produto'], $response->data['regiao'], $response->data['template'], $response->data['ping_status'], $response->data['comment_status'], $response->data['status'], $response->data['type']);
	return $response;
}
add_filter( "rest_prepare_item", "idec_rest_prepare_item", 10, 3);

/**
 * Add REST API support to an already registered taxonomy.
 */
function idec_taxonomy_rest_support() {
  global $wp_taxonomies;
  $taxonomy_names = array ( 'cidade' , 'tipo' , 'regiao' , 'produto' , 'dia');
  foreach ($taxonomy_names as $taxonomy_name) {
	  if ( isset( $wp_taxonomies[ $taxonomy_name ] ) ) {
	    $wp_taxonomies[ $taxonomy_name ]->show_in_rest = true;
	  }
  }
}
add_action( 'init', 'idec_taxonomy_rest_support', 25 );

function count_publish ( $id , $tax ) {

    $args = array(
        'post_type' => 'item',
        'posts_per_page'=> -1,
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => $tax,
                'field' => 'id',
                'terms' => $id
            )
        )
    );
    $state_publish = new WP_Query($args);
    return $state_publish->post_count;
}

// Adicionar Local: If GCR, opening days are not mandatory
add_filter( 'wpcf7_validate_checkbox',
	'idec_conditional_required_checkbox', 99, 2 );
add_filter( 'wpcf7_validate_checkbox*',
	'idec_conditional_required_checkbox', 99, 2 );
add_filter( 'wpcf7_validate_radio',
	'wpcf7_checkbox_validation_filter', 99, 2 );

function idec_conditional_required_checkbox($result, $tag) {

		$tag = new WPCF7_Shortcode( $tag );
		$name = $tag->name;
		$value = isset( $_POST[$name] ) ? (array) $_POST[$name] : array();

		if ( ("dias-funcionamento" == $name) && ('Grupo de Consumo Responsável' != $_POST['tipo']) ) :
				if ( empty($value)  ) :
						$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
				endif;
		endif;

		return $result;
}

// Adicionar Local: If GCR, opening days are not mandatory
add_filter( 'wpcf7_validate_text',
	'idec_conditional_required_text', 99, 2 );
add_filter( 'wpcf7_validate_text*',
	'idec_conditional_required_text', 99, 2 );

function idec_conditional_required_text($result, $tag) {
			$tag = new WPCF7_Shortcode( $tag );
			$name = $tag->name;
			$value = isset( $_POST[$name] )
					? trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) )
					: '';
			if ( "contato" == $name && "Feira Orgânica ou Agroecológica" != $_POST['tipo'] ) :
					if ( '' == $value  ) :
							$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
					endif;
			endif;
			return $result;
	}
