<?php $alimentos = idec_receita_get_non_empty('alimento'); ?>
<div id="categoriesControl-wrapper">
    <div class="categoriesControl-buttons">
        <a href="#" id="categoriesControl-menu" alt="Escolha um alimento" title="Escolha um alimento"><span class="mapafeiras_icon-filtro"></span></a>
    </div>
    <div id="filters">
      <form id="categoriesControl-form">
          <h1>Filtrar por alimento</h1>
          <ul id="categoriesControl-form-options" class="ui-menu">
              <?php foreach ($alimentos as $slug=>$cat) { ?>
                  <li class='ui-menu-item'>
                      <a class="idec-list-item-content ajaxifythis" href="<?= $cat->url ?>" data-item-type='alimento' data-item-id='<?= $slug ?>' data-item-is_overlay=0>
                          <p class="idec-search-autocomplete-name">
                              <span class="m-label-name"><?= $cat->label ?></span>
                              <span class="m-label-count"><?= $cat->count ?></span>
                          </p>
                      </a>
                  </li>
              <?php } ?>
          </ul>
      </form>
    </div>
</div>
