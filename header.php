<?php $pL = idec_pageLocation(); ?>
<?php $this_page_headers = idec_generate_this_page_headers(); ?>
<?php $image = idec_get_post_image($post->ID); ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $this_page_headers->title ?></title>
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link rel="shortcut icon" href="<?php echo BASE_URI; ?>/images/ico_feiras.ico" />

	<link rel="stylesheet" type="text/css" href="<?php echo BASE_URI.'/css/reset.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_URI.'/js/fancybox/jquery.fancybox.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>">
	<?php header('X-Frame-Options: GOFORIT');   ?>

	<meta property="og:locale" content="pt_BR"/>
	<meta property="og:site_name" content="<?php bloginfo('title'); ?>"/>

	<meta property="og:title" content="<?= $this_page_headers->title ?>"/>
	<meta itemprop="name" content="<?= $this_page_headers->title ?>"/>
	<meta property="og:url" content="<?= $pL->href ?>"/>
	<meta property="og:type" content="<?= (PAGE_TYPE=='map' && $pL->type=='global') ? 'website' : 'article'?>"/>
	<meta property="og:description" content="<?= $this_page_headers->description ?>"/>
	<meta itemprop="description" content="<?= $this_page_headers->description ?>"/>
	<meta property="og:image" content="<?= $image[0] ?>"/>
    <meta property="og:image:secure_url" itemprop="image" content="<?= $image[0] ?>"/>
	<meta property="og:image:width" content="<?= $image[1] ?>"/>
	<meta property="og:image:height" content="<?= $image[2] ?>"/>
	<meta itemprop="image" content="<?= $image[0] ?>"/>
	<meta name="description" content="<?= $this_page_headers->description ?>"/>


	<script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-24212310-2', 'auto');

        ga('send', 'pageview');
    </script>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php /* TODO: find a way to cache and reduce CSS and js loading */ ?>
<div id='waitCSSLoad' style="position:absolute;width:100%;height:1200%;background:#fff;z-index:200000"></div>
<script>jQuery(window).bind("load",function(){jQuery('#waitCSSLoad').hide();});</script>

<?php /*
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=231071397085577";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
*/ ?>
<script>
    // Global constants:
    var ADD_PLACE_SLUG = '<?= ADD_PLACE_SLUG ?>',
        ABOUT_SLUG = '<?= ABOUT_SLUG ?>',
        CONTACT_SLUG = '<?= CONTACT_SLUG ?>',
		COMIDADEVERDADE_SLUG = '<?= COMIDADEVERDADE_SLUG ?>',
        PAGE_TYPE = '<?= PAGE_TYPE ?>',
        DAYS_CACHED = <?= (DAYS_CACHED) ? DAYS_CACHED : 'false' ?>,
        ARCHIVE_DAYS_CACHED = <?= (ARCHIVE_DAYS_CACHED) ? ARCHIVE_DAYS_CACHED : 'false' ?>,
        TARGET_EL = {'embedded': 'idec-content-wrapper', 'overlay': 'item-ajax-content'},
        socialMediaSharerUrls = [
            <?php $socialmedia = idec_get_social_media(); ?>
            <?php foreach ($socialmedia as $s) { ?>
            {'class': '<?= $s["class"] ?>', 'url': '<?= $s["url"] ?>'},
            <?php } ?>
        ],
        ajaxurl = '<?= admin_url("admin-ajax.php") ?>',
        base_dir = '<?= BASE_URI ?>',
        home_url = '<?= idec_get_page_type_homeurl() ?>',
        home_title = '<?= idec_get_page_type_hometitle() ?>',
        loadingHTML = '<img class="loading" src="'+base_dir+'/images/logo_loading_em_verde_200x85.gif" />',
        maxCachedPageLocationHistory = 50,
    <?php if ($pL->type && $pL->type!='global' && (PAGE_TYPE=='map' || $pL->is_overlay)) { ?>
        initialPageLocation = {
        'type': '<?= $pL->type ?>',
        'slug': '<?= $pL->slug ?>',
        'href': '<?= $pL->href ?>',
        'title': '<?= $pL->title ?>',
        'name': '<?= $pL->name ?>',
        'isOverlay': <?= $pL->is_overlay ?>,
        'page_type': '<?= $pL->page_type ?>',
        'paged': <?= $pL->paged ?>
    };
    <?php } else { ?>
        initialPageLocation = false;
    <?php } ?>

    // Global variables:
    WINDOW_CHANGE_EVENT = ('onorientationchange' in window) ? 'orientationchange':'resize';
    var isSearchSelected = false,
        menu,
        <?php if($pL->page_type!='map') { ?>

        <?php if(!$pL->type || $pL->type=='item') { ?>
            <?php $pL = idec_pageLocation('archive', '', idec_get_page_type_homeurl(), idec_get_page_type_hometitle(), idec_get_page_type_hometitle(),"0",1); ?>
        <?php } ?>

        pageLocation = {
            'type': '<?= $pL->type ?>',
            'slug': '<?= $pL->slug ?>',
            'href': '<?= $pL->href ?>',
            'title': '<?= $pL->title ?>',
            'name': '<?= $pL->name ?>',
            'isOverlay': <?= $pL->is_overlay ?>,
            'page_type': '<?= $pL->page_type ?>',
            'paged': <?= $pL->paged ?>
        };

        <?php } else { ?>

        pageLocation = {
            'type': '',
            'slug': '',
            'href': '',
            'title': '',
            'name': '',
            'isOverlay': 0,
            'page_type': PAGE_TYPE,
            'paged': 1
        },
        <?php } ?>
        pageLocationHistory = [],
        cachedPageLocations = {};
    if (typeof localStorage !== "undefined" && localStorage.getItem('cachedPageLocations')) {
        cachedPageLocations = JSON.parse(localStorage.getItem('cachedPageLocations'));
    }
</script>

<?php get_template_part( 'structure-initial' ); ?>
