<?php $placeholderText = 'Buscar '.idec_get_page_type_label('n').'...'; ?>
<div id="ajax_search" class="idec-search idec-search-sm">
    <span class="mapafeiras_icon-buscar idec-search-button idec-search-button-sm"></span>
    <input type="text" id="ajax_search_value" placeholder="<?= $placeholderText ?>" class="removeFilterOnClick idec-search-input idec-search-input-sm"/>
    <span id="ajax_search_loading"><img src="<?= BASE_URI ?>/images/loader3.gif"></span>
    <?php get_template_part('categories_control', PAGE_TYPE); ?>
</div>
