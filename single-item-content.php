<?php
    $city 		= idec_get_city($post->ID);
    $endereco 	= idec_remove_city_from_address(get_post_meta($post->ID, 'wpcf-address-custom', true), $city->name);
    $horario	= get_post_meta($post->ID, 'wpcf-horario', true );
    $observacoes_horario = get_post_meta($post->ID, 'wpcf-observacoes-sobre-o-horario', true );
    $referencia	= get_post_meta($post->ID, 'wpcf-referencia', true );
    $contato	= get_post_meta($post->ID, 'wpcf-contato', true );
    $cafe		= get_post_meta($post->ID, 'wpcf-cafe-da-manha', true );
    $regiao 	= strip_tags(get_the_term_list($post->ID, 'regiao', '', ', ', '' ));
    $produtosRaw   = get_the_terms($post, 'produto');
    $tipo = idec_get_type($post->ID, false);
    $selos = get_the_terms($post->ID, 'selo');
    $produtos_da_sociobio = get_post_meta($post->ID, 'wpcf-produtos-da-sociobiodiversidade', true );

    /* get facebook posts, if available */
    $facebook = NULL;
    // Facebook disabled until token is solved
    // if (strstr($referencia, 'facebook')){
    //   $facebook_id = trim(parse_url($referencia, PHP_URL_PATH), "/");
    //   $output_array = NULL;
    //   preg_match('/[0-9]{15}/', $facebook_id, $output_array);
    //   if($output_array){
    //     $facebook_id = $output_array[0];
    //   }
    // }
    // $facebook = do_shortcode("[fts_facebook type=page id='$facebook_id' posts=6 description=no posts_displayed=page_only]");
    //
    // if (!strstr($facebook, 'fts-jal-fb-header')) {
    //   $facebook = NULL;
    // }


    if ($produtosRaw) {
        $produtos = array();
        $outros = false;
        foreach ($produtosRaw as $produto) {
            if ($produto->name == 'Outros') {
                $outros = true;
            } else {
                $produtos[] = $produto->name;
            }
        }
        asort($produtos);
        if ($outros) {
            $produtos[] = 'Outros';
        }
        $produtos = implode('; ', $produtos);
    } else {
        $produtos = '';
    }
?>

<div class="idec-content">
    <div class="idec-content-item">
	    <div class="idec-content-head idec-content-head-sm">
		    <div class="idec-content-head-category">
		        <span class="mapafeiras_icon-tipo_<?= $tipo->slug ?>" aria-hidden="true"></span> <?= $tipo->name ?>
		    </div>
		    <div class="idec-content-head-back">
			    <?php if(!is_single()): ?>
				    <a href="#" id="closeit">
					    Voltar ao Mapa
					    <i class="mapafeiras_icon-fechar" aria-hidden="true"></i>
				    </a>
			    <?php else: ?>
				    <a href="<?php echo home_url( ); ?>">
					    Ver no Mapa
					    <i class="mapafeiras_icon-fechar" aria-hidden="true"></i>
				    </a>
			    <?php endif ?>

		    </div>
	    </div>
	    <div class="idec-ficha">
	    <div class="idec-receita-image idec-receita-image-sm">
		    <?php

			    $images = NULL;
			    $attachments = get_children( array( 'post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image','post_status' => 'inherit') );

			    if ( has_post_thumbnail ( get_the_ID() ) ) {
				    $images[] = get_the_post_thumbnail_url( get_the_ID() );
			    }
			    if ( $attachments ) {
				    foreach ($attachments as $img) {
					    $images[] = wp_get_attachment_url( $img->ID);
				    }
			    }

			    if ( $images ): ?>
			    <div class="cycle-slideshow idec-content-item-slideshow idec-content-item-slideshow-sm">
			        <div class="cycle-prev"><span class="mapafeiras_icon-seta_esq"></span></div>
                    <div class="cycle-next"><span class="mapafeiras_icon-seta_dir"></span></div>
			    <?php foreach ($images as $image) { ?>
				    <div class="idec-content-item-image-slide image-slide" style="background-image:url(<?= $image ?>)"></div>
			    <?php } ?>
			    </div>
			    <?php else: ?>
			        <div class="idec-content-item-image-slide image-slide">
			            <i class="mapafeiras_icon-tipo_<?= $tipo->slug ?>" style="width:100%"></i>
		            </div>
			    <?php endif ?>
	    </div>

	    <div class="idec-content-title-row idec-content-title-row-sm">
		    <?php the_title( '<h1>' , '</h1>' ); ?>
	    </div>
	    <div class="idec-content-item-address-wrapper-sm">
		    <div class="idec-content-item-address-sm">
			    <p><?= $endereco ?></p>
			    <p><?= $city->name ?></p>
			</div>

	    <?php if ($referencia): ?>
	    <div class="idec-content-item-url idec-content-item-url-sm"><p><a target="_blank" href="<?= $referencia ?>"><i class="mapafeiras_icon-pag_link" aria-hidden="true"></i>Acesse o site</a></p></div>
		<?php endif ?>
		</div>
	    <div class="pure-g idec-content-item-infotable-sm">
			    <div class="pure-u-1 idec-content-interaction-wrapper">
            <div class="pure-u-1-1 pure-u-sm-1-3">
              <?= do_shortcode("[crowdbutton id=2]") ?>
				    </div>
				    <!-- <div class="pure-u-1-1 pure-u-sm-1-3">
					    <a href="#" onClick="event.preventDefault();jQuery('html, body').animate({scrollTop: jQuery('#comentarios').offset().top}, 500);">
					    	<div class="idec-content-item-infotable-button-wrapper">
							    <div class="idec-content-item-infotable-button-icon"><i class="mapafeiras_icon-comente" aria-hidden="true"></i></div>
							    <div class="idec-content-item-infotable-button-text">Comente</div>
						    </div>
					    </a>
				    </div> -->
            <div class="pure-u-1-1 pure-u-sm-1-3">
				    	<div class="idec-content-item-infotable-button-wrapper">
					    <?= do_shortcode('
                            [fu-upload-form class="idec-form-send-photo" suppress_default_fields="true" form_layout="media" title=""]
                            [input type="file" name="photo" id="input-idec-form-send-photo" class="required" description="<div class=\'idec-content-item-infotable-button-icon\'><i class=\'mapafeiras_icon-pag_camera\' aria-hidden=\'true\'></i></div><div class=\'idec-content-item-infotable-button-text\'>Envie Fotos</div> <span id=\'count-idec-form-send-photo\'></id>" multiple="multiple"]
                            [recaptcha]
                            [input type="submit" id="button-idec-form-send-photo" class="btn" value="Enviar"]
                            [/fu-upload-form]
				        '); ?>
				        </div>
				    </div>
            <div class="pure-u-1-1 pure-u-sm-1-3">
              <?= do_shortcode("[crowdbutton id=1]") ?>
            </div>
			    </div>
			    <?php
			    	if (parse_url($_SERVER['HTTP_REFERER'])['query'] == 'response=fu-sent'): ?>
			    		<div class="pure-u-1 idec-image-sent">
							<p>Muito obrigada, a imagem foi recebida. Nossa equipe irá analisá-la, e em breve ela estará visível aqui!</p>
			    		</div>
			    <?php endif ?>
          <?php if ($selos): ?>
          <div class="pure-u-1">
				    <div class="pure-u-1 pure-u-sm-1-4">
					    <h2 class="idec-content-subtitle">Selos</h2>
				    </div>
				    <div class="pure-u-1 pure-u-sm-3-4">
              <div class="idec-selos">
                <?php foreach ($selos as $selo): ?>
                  <?php
                    $image_id = get_term_meta( $selo->term_id, 'image', true );
                    $image = wp_get_attachment_image_src( $image_id, 'full', false );
                  ?>
                  <div class="tooltip"><img src="<?= $image[0] ?>" title="<?= $selo->description ?>" /><span class="tooltiptext"><?= $selo->description ?></span></div>
                <?php endforeach; ?>
              </div>
				    </div>
			    </div>
          <?php endif ?>
			    <div class="pure-u-1">
				    <div class="pure-u-1 pure-u-sm-1-4">
					    <h2 class="idec-content-subtitle">Quando</h2>
				    </div>
				    <div class="pure-u-1 pure-u-sm-3-4">
					    <p><?= $horario ?></p>
					<!--<br>-->
					    <p><?php //$observacoes_horario ?></p>
				    </div>
			    </div>
			    <?php if ($produtos): ?>
		    	<div class="pure-u-1">
				    <div class="pure-u-1 pure-u-sm-1-4">
					    <h2 class="idec-content-subtitle">Produtos</h2>
				    </div>
				    <div class="pure-u-1 pure-u-sm-3-4">
					    <p><?= $produtos ?></p>
				    </div>
				</div>
				<?php endif ?>
        <?php if ($produtos_da_sociobio): ?>
        <div class="pure-u-1">
          <div class="pure-u-1 pure-u-sm-1-4">
            <h2 class="idec-content-subtitle">Produtos da Sociobiodiversidade</h2>
          </div>
          <div class="pure-u-1 pure-u-sm-3-4">
            <p><?= $produtos_da_sociobio ?></p>
          </div>
      </div>
      <?php endif ?>
			    <?php if (($contato) AND (!$contato)): //TODO: solve what to make with this ?>
			    <div class="pure-u-1">
				    <div class="pure-u-1 pure-u-sm-1-4">
					    <h2 class="idec-content-subtitle">Contato</h2>
				    </div>
				    <div class="pure-u-1 pure-u-sm-3-4">
					    <p><?= $contato ?></p>
				    </div>
				  </div>
			    <?php endif ?>
			    <?php if ($cafe == 's'): ?>
			    <div class="pure-u-1">
				    <div class="pure-u-1 pure-u-sm-1-4">
					    <h2 class="idec-content-subtitle">Outras informações</h2>
				    </div>
				    <div class="pure-u-1 pure-u-sm-3-4">
					    <p>Café da manhã no local</p>
				    </div>
				  </div>
			    <?php endif ?>
          <div class="pure-u-1">
				    <div class="pure-u-1 pure-u-sm-1-4">
					    <h2 class="idec-content-subtitle">Atualizado em</h2>
				    </div>
				    <div class="pure-u-1 pure-u-sm-3-4">
					    <p>
                <?php
                  setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                  date_default_timezone_set('America/Sao_Paulo');
                  echo strftime('%d de %B de %Y', strtotime($post->post_modified));
                ?>
              </p>
				    </div>
				  </div>

          <?php if ($facebook): ?>
          <div class="pure-u-1">
            <h2 class="idec-content-subtitle">Do Facebook</h2>
          </div>
			    <div class="pure-u-1">
              <?= $facebook ?>
			    </div>
          <?php endif ?>

			    <div class="pure-u-1" style="margin-bottom: 0px;">
				    <h2 id="comentarios" class="idec-content-subtitle">Comentários</h2>
			    </div>
			    <div class="pure-u-1">
			    <?php
				    global $withcomments;
				    $withcomments = "1";
				    comments_template();
			    ?>
			    </div>
	    </div>
	    </div>
    </div>
</div>
