    <?php get_template_part( 'structure-final' ); ?>
	<script type="text/javascript">
        var basicLocationStatsHTML = '<?= idec_get_location_stats_html("","",true) ?>';
        var items = <?= json_encode(idec_biblioteca_get_items()) ?>,
            collections = <?= json_encode(idec_biblioteca_get_collections()) ?>,
            item_types = <?= json_encode(idec_biblioteca_get_item_types()) ?>,
            generalArchiveContentHMTL = '<?= str_replace(array("\n", "  "),"",idec_get_template_model("general-archive", "content")) ?>',
            generalArchiveItemContentHTML = '<?= str_replace(array("\n", "  "),"",idec_get_template_model("general-archive", "item-content", array("extra_image_class"=>" item_thumbnail"))) ?>',
            closeItHTML = '<?= str_replace(array("\n", "  "),"",idec_get_template_model("general-closeit", "content", array("see_all"=>"ver todos os conteúdos", "homeurl"=>idec_get_page_type_homeurl()))) ?>',
            item_images = {};
        if (typeof localStorage !== "undefined" && localStorage.getItem('biblioteca_item_images')) {
            item_images = JSON.parse(localStorage.getItem('biblioteca_item_images'));
        }

        (function( $ ) {
            $(function() {
                remove_loading();
                if(initialPageLocation) {
                    openItemPage(initialPageLocation.type, initialPageLocation.slug, initialPageLocation.href, initialPageLocation.isOverlay, initialPageLocation.page_type, initialPageLocation.paged);
                } else {
                    updateSidebar();
                }
                load_items_thumbs();
                // SEARCH!
                var options = {
                    source: function(request, response) {
                        if (request.term==="") {
                            response([]);
                            return;
                        }
                        
                        response(search_biblioteca(request.term, true));
	                },
	                minLength: 2,
	                focus: function(event, ui) {
	                    event.preventDefault();
	                },
	                select: function(event, ui) {
                        event.preventDefault();
                        if (ui.item.type=='header') {
                            return false;
                        } else {
                            $('#ajax_search_value').val(ui.item.title);
                            var isOverlay = (ui.item.type=='item');
                            openItemPage(ui.item.type, ui.item.id, ui.item.local_url, isOverlay, PAGE_TYPE);
                        }
			        }
                };
                $('#ajax_search_value')
                    .autocomplete(options)
                    .keypress(function(e) {
                        var s = $(this).val();
                        if(e.which == 13 && s.length>=2) {
                            $("#ajax_search_value").autocomplete( "close" );
                            var page_type = 'biblioteca',
                                item_type = 'search',
                                id = s,
                                title = 'Busca na biblioteca: "'+s+'"',
                                matches = search_biblioteca(id, false);
                            if (typeof cachedPageLocations[getLKey(page_type,item_type,id,1)] == 'undefined') {
                                cachedPageLocations[getLKey(page_type,item_type,id,1)] = {
                                    html: build_search_results_page(matches, page_type, id, title, 1, s),
                                    targetEl: get_targetEl(false),
                                    id: id,
                                    name: s,
                                    title: title,
                                    updatedAt: new Date().getTime()
                                }
                            }
                            openItemPage(item_type, id, "", false, page_type);
                        }
                    })
                    .data("ui-autocomplete")._renderItem = function(ul, item) {
                        return build_search_result_item(item, 'm', true).appendTo(ul);
		            };
                
                function build_search_result_item(item, classPreposition, inAutocomplete) {
                    var label = item.title;
                    var $a = jQuery("<a></a>").addClass('idec-list-item-content');
                    if (item.type=='header') {
                        $h2 = jQuery("<h2></h2>").addClass('idec-search-autocomplete-section').text(label);
                        return jQuery("<li></li>").addClass('header').append($h2);
                    } else if (item.type=='item') {
                        if(!inAutocomplete) {
                            $a.addClass('ajaxifythis')
                                .attr({
                                    'href': item.local_url, 
                                    'data-item-type': 'item', 
                                    'data-item-id': item.id,
                                    'data-page_title': item.title,
                                    'data-item-is_overlay': 1
                                });
                        }
                        var $div = jQuery("<div></div>");
                        var $title = jQuery("<h1></h1>").addClass('idec-list-item-title').text(label).appendTo($div);
                        var $description = jQuery("<p></p>").addClass('idec-list-item-address').text(get_excerpt(item.description)).appendTo($div);
                        $div.appendTo($a);
                    } else if (item.type=='collection') {
                        if (!inAutocomplete) {
                            $a.addClass('ajaxifythis')
                                .attr({
                                'href': item.local_url, 
                                'data-type': 'collection',
                                'data-id': item.id,
                                'data-page_title': item.title,
                                'data-item-is_overlay': 0
                            });
                        }
                        var $p = jQuery("<p></p>").addClass('idec-search-autocomplete-name').html("<span class='"+classPreposition+"-label-name'>"+label+"</span> <span class='"+classPreposition+"-label-count'>"+item.items_count+"</span>");
                        $p.appendTo($a);
                    }
                    return jQuery("<li></li>").addClass(classPreposition+'-'+item.type).append($a);
                }
            });
        })(jQuery);
        
        function load_items_thumbs() {
            if (Object.keys(item_images).length > 0) {
                jQuery('img.item_thumbnail').each(function() {
                    var $img = jQuery(this);
                    if (typeof item_images[$img.attr('data-item-id')] != 'undefined' 
                        && item_images[$img.attr('data-item-id')] 
                        && DAYS_CACHED 
                        && daydiff(new Date().getTime(), item_images[$img.attr('data-item-id')].updatedAt)<=DAYS_CACHED
                    ) {
                        var data = item_images[$img.attr('data-item-id')];
                        var src = typeof($img.attr('data-item-size'))!='undefined' 
                            ? data[$img.attr('data-item-size')] 
                            : data['thumbnail'];
                        $img.attr({
                            src: src,
                            title: $img.attr('data-item-title'),
                            alt: $img.attr('data-item-title')
                        });
                    }
                });
            }
            
            // Try to get other images from ajax:
            jQuery('img.item_thumbnail').each(function() {
                var $img = jQuery(this);
                if ( typeof item_images[$img.attr('data-item-id')]=='undefined' 
                    || !DAYS_CACHED 
                    || daydiff(new Date().getTime(), item_images[$img.attr('data-item-id')].updatedAt)>DAYS_CACHED ) {
                    var params = {
                        'action': 'idec_biblioteca_ajax_get_item_thumbnail',
                        'id': $img.attr('data-item-id')
                    };
                    jQuery.post(ajaxurl, params, function(data) {
                        item_images[$img.attr('data-item-id')] = data;
                        item_images[$img.attr('data-item-id')].updatedAt = new Date().getTime();
                        if (typeof localStorage !== "undefined") {
                            localStorage.setItem("biblioteca_item_images", JSON.stringify(item_images));
                        }
                        var src = typeof($img.attr('data-item-size'))!='undefined' 
                            ? data[$img.attr('data-item-size')] 
                            : data['thumbnail'];
                        $img.attr({
                            src: src,
                            title: $img.attr('data-item-title'),
                            alt: $img.attr('data-item-title')
                        });
                    }, "json")
                        .fail(function() {
                            $img.css('visibility','hidden');
                            item_images[$img.attr('data-item-id')] = false;
                            item_images[$img.attr('data-item-id')].updatedAt = new Date().getTime();
                            if (typeof localStorage !== "undefined") {
                                localStorage.setItem("biblioteca_item_images", JSON.stringify(item_images));
                            }
                        });
                }
            });
        }
        
        function search_biblioteca(s, isAutocomplete) {
            var i, l, item, collection, matches = [], collectionMatches = [];

            for  (i = 0, l = items.data.length; i<l; i++) {
                item = items.data[i];
                item.type='item';
                if (matchChecker(s, item, ['title', 'description'])) {
                    matches.push(item);
                }
            }
            
            if(!isAutocomplete) {
                return matches;
            }
            
            if(matches.length>0) {
                matches.unshift({
                    'type': 'header',
                    'title': 'Conteúdos'
                });
            }
            
            for (var collId in collections) {
                collection = collections[collId];
                collection.type='collection';
                if (matchChecker(s, collection, ['title'])) {
                    collectionMatches.push(collection);
                }
            }
            if(collectionMatches.length>0) {
                collectionMatches.unshift({
                    'type': 'header',
                    'title': 'Temas'
                });
            }
            
            return matches.concat(collectionMatches);
        }
        
	</script>

	    <?php wp_footer(); ?>
	</body>
</html>
