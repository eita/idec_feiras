<article id="item-{item_id}" class="idec-grid-item pure-u-1-2 pure-u-sm-12-24 pure-u-md-8-24 pure-u-lg-6-24">
    <a href="{item_url}" class="ajaxifythis image" data-item-type="item" data-item-id="{item_id}" data-item-is_overlay=1>
        <img src="{image_url}" class="pure-image{extra_image_class}" data-item-title="{title}" data-item-id="{item_id}" alt="{image_title}" title="{image_title}">
        <div class="idec-grid-item-title">
            {title}
        </div>
    </a>
</article>
