<div class="idec-content-full-height">
    <div class="idec-content searchbox">
        <div class="idec-content-receita">
            {closeit}
            <div class="idec-receita-image">
                {image}
            </div>
            <div class="idec-content-title-row pure-hide idec-content-title-row-sm">
                <h1>{title}</h1>
            </div>
            <div>
                <p>{intro}</p>
                <p class="idec-content-archive-count">{count}</p>
                <p class="idec-content-archive-page">{whereami}</p>
                <div class="pure-g">
                    {content_archive}
                </div>
            </div>
            {pagination}
        </div>
    </div>
</div>
