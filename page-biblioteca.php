<?php

// Default location and target:
idec_pageLocation('archive', '', idec_get_page_type_homeurl(), idec_get_page_type_hometitle(), idec_get_page_type_label(), "0", 'biblioteca', idec_biblioteca_get_page_from_url());
$template_target = array('archive', 'biblioteca-content');

// Conditional location and target:
if(isset($_REQUEST['id']) || isset($_REQUEST['mixed_archive']) || isset($_REQUEST['collection']) || isset($_REQUEST['item_type'])) {
    $item = idec_biblioteca_get_single_item();
    if($item && get_object_vars($item)) {
        // Single item:
        idec_pageLocation('item', $item->id, $item->local_url, $item->title, $item->title, "1", 'biblioteca');
    } else {
        // It's an archive, be it a collection, be it an item type, be it a mixed_archive
        $mixed_archive = idec_biblioteca_get_single_mixed_archive();
        if ($mixed_archive && get_object_vars($mixed_archive)) {
            idec_pageLocation('mixed_archive', $mixed_archive->id, $mixed_archive->local_url, $mixed_archive->title, $mixed_archive->title, "0", 'biblioteca', idec_biblioteca_get_page_from_url());
            $template_target = array('single', 'mixed_archive-content');
        } else {
            $collection = idec_biblioteca_get_single_collection();
            if ($collection && get_object_vars($collection)) {
                idec_pageLocation('collection', $collection->id, $collection->local_url, $collection->title, $collection->title, "0", 'biblioteca', idec_biblioteca_get_page_from_url());
                $template_target = array('single', 'collection-content');
            } else {
                $item_type = idec_biblioteca_get_single_item_type();
                if ($item_type && get_object_vars($item_type)) {
                    idec_pageLocation('item_type', $item_type->id, $item_type->local_url, $item_type->title, $item_type->title, "0", 'biblioteca', idec_biblioteca_get_page_from_url());
                    $template_target = array('single', 'item_type-content');
                }
            }
            
        }
    }
}
?>
<?php get_header(); ?>

    <?php get_template_part($template_target[0], $template_target[1]); ?>

<?php get_footer('biblioteca'); ?>
