<?php $categories = idec_get_all_types(); ?>
<?php $selos = idec_get_all_selos(); ?>
<div id="categoriesControl-wrapper">
    <div class="categoriesControl-buttons">
        <a href="#" id="categoriesControl-menu" alt="filtrar tipo" title="filtrar tipo"><span class="mapafeiras_icon-filtro"></span></a>
	<?php if (wp_is_mobile()): ?>
	        <a href="#" id="categoriesControl-myPosition" alt="minha localização" title="minha localização"><span class="mapafeiras_icon-localize"></span></a>
	<?php endif ?>
    </div>
    <div id="filters">
      <form id="categoriesControl-form">
          <h1>Filtrar categorias</h1>
          <?php foreach ($categories as $cat) { ?>
              <label>
                  <input id="cat_<?= $cat->slug ?>" type="checkbox" checked="checked">&nbsp;
                  <img class="categoryIcon" src="<?= BASE_URI.'/images/icones_tipos_sombra_'.$cat->slug ?>.png">&nbsp;
                  <?php
                      $args = array(
                          'post_type' => 'item',
                          'posts_per_page'=> -1,
                          'post_status' => 'publish',
                          'tax_query' => array(
                              array(
                                  'taxonomy' => 'tipo',
                                  'field' => 'slug',
                                  'terms' => $cat->slug
                              )
                          )
                      );
                      $postsPerTerm = new WP_Query($args);
                  ?>
                  <?= $cat->name ?> <i>(<?= $postsPerTerm->post_count ?>)</i>
              </label>
          <?php } ?>
      </form>
      <!--
      <form id="selosControl-form">
          <h1 style="margin-bottom: 13px">Filtrar por selos</h1>
          <?php foreach ($selos as $selo) { ?>
              <?php
              $image_id = get_term_meta( $selo->term_id, 'image', true );
              $image = wp_get_attachment_image_src( $image_id, 'full', false );
              ?>
              <label>
                  <input id="cat_<?= $selo->slug ?>" type="checkbox">&nbsp;
                  <img style="width:50px; vertical-align: bottom;" class="categoryIcon" src="<?= $image[0] ?>">&nbsp;
                  <?php
                      $args = array(
                          'post_type' => 'item',
                          'posts_per_page'=> -1,
                          'post_status' => 'publish',
                          'tax_query' => array(
                              array(
                                  'taxonomy' => 'selo',
                                  'field' => 'slug',
                                  'terms' => $selo->slug
                              )
                          )
                      );
                      $postsPerTerm = new WP_Query($args);
                  ?>
                  <?= $selo->name ?> <i>(<?= $postsPerTerm->post_count ?>)</i>
              </label>
          <?php } ?>
      </form>
      -->
    </div>

</div>
