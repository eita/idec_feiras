	<div class="idec-content-head idec-content-head-sm">
		<div class="idec-content-head-category">
		    <!-- <span class="fa fa-cutlery" aria-hidden="true"></span> -->
		    <?php //idec_receita_show_item_categories(get_the_ID(), 'tipo-de-receita', 'idec-receita-tipo-de-receita-tag'); ?>
		</div>
		<div class="idec-content-head-back">
			<a id="closeit" href="<?= idec_get_page_type_homeurl() ?>">
			    Voltar
			    <i class="mapafeiras_icon-fechar" aria-hidden="true"></i>
		    </a>
		</div>
	</div>
	<div class="idec-ficha idec-ficha-receita-sm">
		<?php if (has_post_thumbnail ()): ?>
		<div class="idec-receita-single-image idec-receita-single-image-sm" style="background-image: url(<?php the_post_thumbnail_url( array (567,567) ); ?>);">			
		</div>
		<?php endif ?>

		<div class="idec-ficha-receita-inner-sm">

		<div class="idec-content-title-row idec-content-title-row-sm">
			<?php the_title( '<h1>' , '</h1>' ); ?>
			<!-- <i class="mapafeiras_icon-espalhe" aria-hidden="true"></i> -->
		</div>

		<?php $ingredientes = get_post_meta( get_the_ID(), 'wpcf-ingredientes', true); ?> 
		<?php $mododepreparo = get_post_meta( get_the_ID(), 'wpcf-modo-de-preparo', true); ?> 
		<?php $rendimento = get_post_meta( get_the_ID(), 'wpcf-rendimento', true); ?> 
		<?php $fonte = get_post_meta( get_the_ID(), 'wpcf-fonte', true); ?> 
		<?php $link = get_post_meta( get_the_ID(), 'wpcf-link', true); ?> 

		<div class="pure-g idec-content-item-infotable-sm">
			<div class="pure-u-1">
				<div class="pure-u-1 pure-u-sm-1-4">
					<h2 class="idec-content-subtitle">Ingredientes:</h2>
				</div>
				<div class="pure-u-1 pure-u-sm-3-4">
					<?= wpautop(strip_tags($ingredientes)); ?>
				</div>
			</div>
			<div class="pure-u-1">
				<div class="pure-u-1 pure-u-sm-1-4">
					<h2 class="idec-content-subtitle">Modo de Preparo:</h2>
				</div>
				<div class="pure-u-1 pure-u-sm-3-4">
					<?= wpautop(strip_tags($mododepreparo)); ?>
				</div>
			</div>
<?php if ($rendimento): ?>
			<div class="pure-u-1">
				<div class="pure-u-1 pure-u-sm-1-4">
					<h2 class="idec-content-subtitle">Rendimento:</h2>
				</div>
				<div class="pure-u-1 pure-u-sm-3-4">
					<p><?= $rendimento ?></p>
				</div>
			</div>
<?php endif ?>
<?php if ($fonte): ?>	
			<div class="pure-u-1">
				<div class="pure-u-1-2 pure-u-sm-1-4">
					<h2 class="idec-content-subtitle">Fonte:</h2>
				</div>
				<div class="pure-u-1">
					<div class="pure-u-1 pure-u-sm-3-4">
						<p>							
							<?= $link?"<a href=" . $link . ">":"" ?>
							<?= str_replace("Fonte: ", "", $fonte) ?>
							<?= $link?"</a>":"" ?>							
						</p>
					</div>
				</div>
			</div>
<?php endif ?>
			<div class="pure-u-1">
				<div class="pure-u-1-2 pure-u-sm-1-4">
					<h2 class="idec-content-subtitle">Mais Receitas:</h2>
				</div>
				<div class="pure-u-1">
					<div class="pure-u-1 pure-u-sm-3-4">
	                    <?= idec_receita_show_item_categories(get_the_ID(), 'alimento', 'idec-receita-alimento-tag'); ?>
	                    <?= idec_receita_show_item_categories(get_the_ID(), 'tipo-de-receita', 'idec-receita-alimento-tag'); ?>			
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
