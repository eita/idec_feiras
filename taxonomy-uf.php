<?php $term = idec_get_this_locality_from_term('uf'); ?>
<?php $headers = idec_generate_page_headers('map', 'uf', $term->slug); ?>
<?php idec_pageLocation('uf', $term->slug, get_term_link($term->slug, 'uf'), $headers->title, $term->label, '0', 'map'); ?>
<?php get_header(); ?>

    <div id="main_map" class="removeFilterOnClick"></div>
        
<?php get_footer('map'); ?>
