<?php $pL = idec_pageLocation(); ?>
<?php $template_parts = (!$pL->type || $pL->type=='archive') ? array('archive', 'receita-content') : array('single', $pL->type.'-content'); ?>

<?php get_header(); ?>

<?php get_template_part($template_parts[0], $template_parts[1]); ?>

<?php get_footer('receita'); ?>
