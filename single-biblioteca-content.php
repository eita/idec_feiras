<?php $item = idec_biblioteca_get_single_item(false, true); ?>
<div class="idec-content-full-height">
	<div class="idec-content">
		<div class="idec-content-biblioteca">
			<div class="idec-content-head idec-content-head-sm">
				<div class="idec-content-head-category">
				</div>
				<div class="idec-content-head-back">
					<a href="#" id="closeit">
					    Voltar
					    <i class="mapafeiras_icon-fechar" aria-hidden="true"></i>
				    </a>
				</div>
			</div>
			<div class="idec-ficha">
	        <?php if (!$item->video) { ?>
	        	<div class="idec-receita-image idec-receita-image-sm idec-single-biblioteca-image">
	            	<img src="<?= BASE_URI ?>/images/loader3.gif" class="pure-image item_thumbnail" data-item-title="<?= $item->title ?>" data-item-id="<?= $item->id ?>" data-item-size="size800" alt="loading" title="loading">
				</div>            	
	        <?php } ?>		
			<?php if ($item->video) { ?>
	            <div class="biblioteca-video"><?= $item->video ?></div>
	        <?php } ?>         
			<div class="idec-content-title-row idec-content-title-row-sm">
				<h1><?= $item->title ?></h1>
			</div>
			<div class="pure-g idec-content-item-infotable-sm">
			    <?php if ($item->item_type==11) { ?>
	            <div class="pure-u-1">
					<div class="pure-u-1 pure-u-sm-1-4">
						<h2 class="idec-content-subtitle">Link</h2>
					</div>
					<div class="pure-u-1 pure-u-sm-3-4">
						<p><a href="<?= $item->link ?>" target="_blank"><?= $item->link ?></a></p>
					</div>
				</div>
	            <?php } else { ?>
				<div class="pure-u-1" style="margin-bottom: 20px">
				    <?php $i=0; ?>
			        <?php foreach($item->files as $file) { ?>
			            <?php if (isset($file->thumb_only) && $file->thumb_only) { ?>
			                <?php continue; ?>
		                <?php } ?>
			            <span class="value">
			            	<a href="<?= $file->file_urls->original ?>" target="_blank" style="margin-right: 10px">
				            	Arquivo <?php echo ($i+1) . " (" . pathinfo($file->file_urls->original)['extension'] . ")"; ?>
				            	<i class="mapafeiras_icon-pag_download" aria-hidden="true"></i>
			            	</a>
			            </span>
			            <?php $i++; ?>
			        <?php } ?>
			    </div>
			    <?php } ?>
				<div class="pure-u-1">
					<p><?= $item->description ?></p>
				</div>
	            <?php if ($item->creator) { ?>
	            <div class="pure-u-1">
					<div class="pure-u-1 pure-u-sm-1-4">
						<h2 class="idec-content-subtitle">Autoria</h2>
					</div>
					<div class="pure-u-1 pure-u-sm-3-4">
						<p><?= $item->creator ?></p>
					</div>
				</div>
	            <?php } ?>				
	            <?php if ($item->date) { ?>
	            <div class="pure-u-1">
					<div class="pure-u-1 pure-u-sm-1-4">
						<h2 class="idec-content-subtitle">Ano</h2>
					</div>
					<div class="pure-u-1 pure-u-sm-3-4">
						<p><?= $item->date ?></p>
					</div>
				</div>
	            <?php } ?>
	            <?php if ($item->source) { ?>
	            <div class="pure-u-1">
					<div class="pure-u-1 pure-u-sm-1-4">
						<h2 class="idec-content-subtitle">Fonte</h2>
					</div>
					<div class="pure-u-1 pure-u-sm-3-4">
						<p class="idec-content-metadata-item"><?= make_clickable($item->source) ?></p>
					</div>
				</div>
	            <?php } ?>
				<div class="pure-u-1">
					<div class="pure-u-1-2 pure-u-sm-1-4">
						<h2 class="idec-content-subtitle">Mais Publicações:</h2>
					</div>
					<div class="pure-u-1">
						<div class="pure-u-1 pure-u-sm-3-4">
		                    <?= idec_biblioteca_show_item_categories($item, 'collection', 'idec-receita-alimento-tag'); ?>
		                    <?= idec_biblioteca_show_item_categories($item, 'item_type', 'idec-receita-alimento-tag'); ?>
						</div>
					</div>
				</div>		            
			</div>  
			</div>       
		</div>
	</div>
</div>
