<?php

updateCityCount();

function updateCityCount () {

	$mysqli = new mysqli("localhost", "root", "lamp123", "feirasorg");
	if ($mysqli->connect_errno) {
	    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	$query = "SELECT rel.term_taxonomy_id, count(po.ID) FROM wp_term_relationships rel 
    INNER JOIN wp_posts po ON (po.ID = rel.object_id) 
    INNER JOIN wp_term_taxonomy tax ON (rel.term_taxonomy_id = tax.term_id)
    WHERE tax.taxonomy = 'tipo'
 	GROUP BY rel.term_taxonomy_id";

	if (!$res = $mysqli->query($query)) {
	    echo "Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	while ($row = $res->fetch_assoc()) {
		echo "UPDATE wp_term_taxonomy SET count = " . $row['count(po.ID)'] . " WHERE term_taxonomy_id = " . $row['term_taxonomy_id'] . ";";
		echo "<br>";
	}

}

function migrateOldCities () {
	$mysqli = new mysqli("localhost", "root", "lamp123", "feirasorg");
	if ($mysqli->connect_errno) {
	    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	$query = "SELECT * FROM `wp_posts` WHERE `post_type` = 'item' AND (`post_status` = 'publish' OR `post_status` = 'draft')";
	if (!$res = $mysqli->query($query)) {
	    echo "Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	while ($row = $res->fetch_assoc()) {
		// para um post, pega os termos de taxonomia de cidade e estado
		$query2 = "SELECT `wp_terms`.`name` FROM `wp_term_relationships` INNER JOIN `wp_term_taxonomy` ON 
		`wp_term_relationships`.`term_taxonomy_id` = `wp_term_taxonomy`.`term_id` 
		INNER JOIN `wp_terms` ON `wp_term_taxonomy`.`term_id` = `wp_terms`.`term_id`
		WHERE `wp_term_relationships`.`object_id` = " . $row['ID'] . " AND (`wp_term_taxonomy`.`taxonomy` = 'city' OR `wp_term_taxonomy`.`taxonomy` = 'UF')";

		if (!$res2 = $mysqli->query($query2)) {
		    echo "Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
		}

		// se estado e cidades estão preenchidos
		if ($res2->num_rows == 2) {
			$l = NULL;
			while ($row2 = $res2->fetch_assoc()) {
				$l[] = $row2['name'];	
			}	
			if (strlen($l[1]) == 2 ) {
				$city = $l[0] . "-" . $l[1]; 
			} else {
				$city = $l[1] . "-" . $l[0]; 
			}
			$city = createSlug(utf8_encode($city));
			// echo $city;
			$query3 = "SELECT * FROM `wp_terms` WHERE `slug` = '$city'";
			if (!$res3 = $mysqli->query($query3)) {
			    echo "Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
			}

			if ($res3->num_rows == 1){
				// echo "----> success<br/>";			
				$row3 = $res3->fetch_assoc();				
				echo "INSERT INTO `wp_term_relationships` SET `term_taxonomy_id` = " . $row3['term_id'] . ", `object_id` = " . $row['ID'] . ", 	`term_order` = 0;";
				echo "<br>";
			} else {
				echo "----> fail<br/>";				
			}
		} else {
			$query3 = "SELECT * FROM `wp_postmeta` WHERE `wp_postmeta`.`post_id` = " . $row['ID'];
			if (!$res3 = $mysqli->query($query3)) {
			    echo "Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
			}
			while ($row3 = $res3->fetch_assoc()) {
				if ($row3['meta_key'] == 'wpcf-address'){
					echo $row['ID']. "  - Address: " . $row3['meta_value'] . "<br>";
				}
			}
		}

	}
}


function modifySlugs () {
	$mysqli = new mysqli("localhost", "root", "lamp123", "feirasorg");
	if ($mysqli->connect_errno) {
	    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	$query = "SELECT `wp_terms`.`name`, `wp_terms`.`term_id` FROM `wp_term_taxonomy` INNER JOIN `wp_terms`
	ON `wp_term_taxonomy`.`term_id` = `wp_terms`.`term_id`
	WHERE `wp_term_taxonomy`.`taxonomy`= 'City'";

	$res = $mysqli->query($query);

	if (!$res){
	    echo "Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}

	while ($row = $res->fetch_assoc()) {
		$query2 = "SELECT `cidades`.`nome`, `estados`.`sigla` FROM `cidades` INNER JOIN `estados` ON `cidades`.`estado_id` = `estados`.`id` WHERE `cidades`.`nome` = '". $row['name'] . "'";

		if ($res2 = $mysqli->query($query2))
		    echo utf8_encode($row['name']) . " - " . $res2->num_rows . "<br>";
		else {			
		    echo "Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
		}

		if ($res2->num_rows > 1) {

			$query3 = "SELECT `object_id` FROM `wp_term_relationships` WHERE `term_taxonomy_id` = " . $row['term_id'];

			$res3 = $mysqli->query($query3);
			if (!$res3){
			    echo "Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
			}
			while ($row3 = $res3->fetch_assoc()) {
				$query4 = "SELECT `wp_term_taxonomy`.`term_id` FROM `wp_term_relationships`
							INNER JOIN `wp_term_taxonomy` 
							ON `wp_term_taxonomy`.`term_id` = `wp_term_relationships`.`term_taxonomy_id` 
							WHERE `wp_term_relationships`.`object_id` = " . $row3['object_id'] . "							
							AND `wp_term_taxonomy`.`taxonomy`= 'UF'";

				$res4 = $mysqli->query($query4);

				if (!$res4){
				    echo "Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
				}

				while ($row4 = $res4->fetch_assoc()) {
					echo "    " . $row4['term_id'] . "<br>";
				}
			}
		}
	}

}

function genTerms () {
	$mysqli = new mysqli("localhost", "root", "lamp123", "feirasorg");
	if ($mysqli->connect_errno) {
	    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	$query = "SELECT `cidades`.`nome`, `estados`.`sigla` FROM `cidades` INNER JOIN `estados` ON `cidades`.`estado_id` = `estados`.`id`";

	$res = $mysqli->query($query);

	while ($row = $res->fetch_assoc()) {
	    echo utf8_encode($row['nome']) . " - " . utf8_encode($row['sigla']) . "<br>";
	}
}

function createSlug($string) {

    $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-', '\'' => ''
    );

    // -- Remove duplicated spaces
    $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);

    // -- Returns the slug
    return strtolower(strtr($string, $table));


}


