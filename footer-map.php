  <?php get_template_part( 'structure-final' ); ?>
  <script type="text/javascript">

    // Global constants:
    const COMIDADEVERDADE = <?= (COMIDADEVERDADE) ? 'true' : 'false' ?>;
    var localGeojsonDir = '<?= idec_get_local_geojson_dir(); ?>';
    var baseIconDir = base_dir+'/images/icones_tipos_sombra_';
    var limitesBrasilRaw = [[-33.751944444444, -73.990555555556],[5.2719444444444, -34.792777777778]];
    var zoomMinimo = 4;
    var maxSidebarItems = 30;
    var maxSidebarCities = 20;
    var maxSidebarStates = 20;
    var itemHTML = '<?= idec_get_item_html(); ?>';
    var locationHTML = '<?= idec_get_location_html(); ?>';
    var basicLocationStatsHTML = '<?= idec_get_location_stats_html("","",true) ?>';
    var OpenStreetMapProvider;
    var geoProvider;
    var geoSearchControl;

    // Global variables:
		var map;
		var $main_map = jQuery('#main_map');
		var markerClusterer;
		var infowindow;
		var markers = [];
		var isAutoZooming = false;
		var markersRaw = <?= json_encode(get_markers()) ?>;
		var cities = <?= json_encode(idec_get_all_non_empty('cidade', false, true)) ?>;
		var states = <?= json_encode(idec_get_all_non_empty('uf')) ?>;
		var regions = <?= json_encode(idec_get_regions()) ?>;
		var locationStatsHTML = {};
		var limitesBrasil;
    var ultimaPosicaoValida;
    var searchResults = {
      "s": "",
      "items": []
    };
    var globalBounds;
    var locationPolygon;
    var iconOptions = {
      iconSize: [48, 56],
      iconAnchor: [24, 56]
    };

    if(initialPageLocation && ['cidade', 'uf'].indexOf(initialPageLocation.type)==-1) {
      (function( $ ) {
        $(function() {
          openItemPage(initialPageLocation.type, initialPageLocation.slug, initialPageLocation.href, initialPageLocation.isOverlay, initialPageLocation.page_type);
        });
      })(jQuery);
    }

    (function( $ ) {
      $(function() {
        OpenStreetMapProvider = window.GeoSearch.OpenStreetMapProvider;
        GeoSearchControl = window.GeoSearch.GeoSearchControl;
        geoProvider = new OpenStreetMapProvider({
          params: {
            country: "Brazil",
            countrycodes: "br"
          }
        });
        if(!initialPageLocation || ['cidade', 'uf'].indexOf(initialPageLocation.type)>-1) {
          // Let the map rock!
          runMap();
        }
      });
    })(jQuery);


    function runMap() {
        if (markers.length>0) {
            return;
        }


        map = L.map('main_map', {
          center: {lat: -14.2400732, lng: -53.1805017}, // Brasil
          zoom: zoomMinimo,
          zoomControl: false
        });
        map.getPane('mapPane').style.zIndex = 90;
        mapTileLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {foo: 'bar'});
        mapTileLayer.addTo(map);

        if (COMIDADEVERDADE) {
            var aboutControl = L.control({
              position: "topright"
            });
            aboutControl.onAdd = function (map) {
              var div = L.DomUtil.create("div", "leaflet-control leaflet-bar leaflet-control-about");
              div.innerHTML = "<a title='Especial COVID-19: Encontre Comida de Verdade durante a pandemia' class='leaflet-control-part aboutControl' href='https://feirasorganicas.org.br/comidadeverdade'><div id='aboutControlTextTitle'> Especial COVID-19</div><div id='aboutControlText'>Encontre Comida de Verdade<br />durante a pandemia</div></a>";
              return div;
            };
            map.addControl(aboutControl);
        }
        new L.Control.Zoom({ position: 'topleft' }).addTo(map);

        /*map = new google.maps.Map(document.getElementById('main_map'), {
            center: {lat: -14.2400732, lng: -53.1805017}, // Brasil
            zoom: zoomMinimo,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false,
            panControl: false,
            scrollwheel: true,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            styles: [ { "featureType": "landscape.man_made", "elementType": "geometry.fill", "stylers": [ { "color": "#f9f7ef" } ] }, { "featureType": "poi", "elementType": "labels.icon", "stylers": [ { "saturation": -50 } ] }, { "featureType": "poi.business", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.government", "elementType": "geometry.fill", "stylers": [ { "color": "#f1d6db" } ] }, { "featureType": "poi.medical", "elementType": "geometry.fill", "stylers": [ { "color": "#f8f9e1" } ] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [ { "color": "#b7dbaf" } ] }, { "featureType": "poi.place_of_worship", "elementType": "geometry.fill", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.school", "elementType": "geometry.fill", "stylers": [ { "color": "#c7cadc" } ] }, { "featureType": "poi.sports_complex", "elementType": "geometry.fill", "stylers": [ { "color": "#b7dbaf" } ] }, { "featureType": "road", "stylers": [ { "visibility": "simplified" } ] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [ { "color": "#fdfcf8" } ] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [ { "color": "#eaebd8" } ] }, { "featureType": "road.highway", "stylers": [ { "visibility": "simplified" } ] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [ { "color": "#f8c967" } ] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "color": "#fee5b3" } ] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "color": "#e7b95e" } ] }, { "featureType": "road.local", "elementType": "geometry.fill", "stylers": [ { "color": "#f0ede6" } ] }, { "featureType": "transit", "stylers": [ { "visibility": "simplified" } ] }, { "featureType": "transit", "elementType": "geometry.fill", "stylers": [ { "color": "#f6f3ec" } ] }, { "featureType": "water", "stylers": [ { "color": "#86d2e1" } ] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [ { "color": "#a8d9de" } ] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [ { "color": "#92998d" } ] }]
        });
        */

        ultimaPosicaoValida = map.getCenter();
        limitesBrasil = L.latLngBounds(
            L.latLng(limitesBrasilRaw[0][0], limitesBrasilRaw[0][1]),
            L.latLng(limitesBrasilRaw[1][0], limitesBrasilRaw[1][1])
        );

        infowindow = L.popup({maxWidth: 300, offset: L.point(0,-60)});

        // Adds markers and populates global bounds:
        globalBounds = L.latLngBounds();
        for(var i=0;i<markersRaw.length;i++) {
            iconOptions.iconUrl = baseIconDir+markersRaw[i].type+'.png';
            var marker = L.marker(L.latLng(markersRaw[i].lat, markersRaw[i].lng), {
                id: markersRaw[i].id,
                icon: L.icon(iconOptions),
                type: markersRaw[i].type,
                selos: markersRaw[i].selos,
                title: markersRaw[i].title,
                mRawPos: i
            });
            markers.push(marker);
            globalBounds.extend(marker.getLatLng());
            marker.on('click', (function(marker, i) {
                return function() {
                    openInfoWindow(marker);
                }
            })(marker, i));
        }

        (function( $ ) {
            $(function() {
                if (initialPageLocation && ['cidade', 'uf'].indexOf(initialPageLocation.type)>-1) {
                    // If we landed in a city or a state directly, map should zoom to this place.
                    remove_loading();
                    goToLocation(window.location.href, initialPageLocation.title, initialPageLocation.name, initialPageLocation.type, initialPageLocation.slug, initialPageLocation.name);
                } else {
                    // If not, then the search input must be empty (even after a page reload)
                    jQuery('#ajax_search_value').val("");
                    updateSidebar();
                }

                markerCluster = L.markerClusterGroup({
                    disableClusteringAtZoom: 10,
                    spiderfyOnMaxZoom: false,
                    iconCreateFunction: function (cluster) {
                  		var childCount = cluster.getChildCount();
                  		var c = ' marker-cluster-';
                      var size;
                  		if (childCount < 10) {
                  			c += 'small';
                        size = 50;
                  		} else if (childCount < 100) {
                  			c += 'medium';
                        size = 90;
                  		} else {
                  			c += 'large';
                        size = 130;
                  		}
                  		return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'marker-cluster' + c, iconSize: new L.Point(size, size) });
                  	},
                    /*styles: [
                        {
                            "height":50,
                            "width":50,
                            "url":base_dir+'/images/clusters/f1.png'
                        },
                        {
                            "height":70,
                            "width":70,
                            "url":base_dir+'/images/clusters/f2.png'
                        },
                        {
                            "height":90,
                            "width":90,
                            "url":base_dir+'/images/clusters/f3.png'
                        },
                        {
                            "height":110,
                            "width":110,
                            "url":base_dir+'/images/clusters/f4.png'
                        },
                        {
                            "height":130,
                            "width":130,
                            "url":base_dir+'/images/clusters/f5.png'
                        }
                    ]*/
                })
                  .addLayers(markers)
                  .addTo(map);
            });

          // SEARCH!
            var options = {
              source: function(request, response) {
                  $('#ajax_search_loading').show();
                  var params = {
                    'action': 'get_markers',
                    'limit': 5,
                    's': request.term
                  };
                  $.post(ajaxurl, params, function(data) {
                      $('#ajax_search_loading').hide();
                      if(pageLocation.type=='search' && pageLocation.slug == request.term) {
                          response(null);
                      }
                      /*
                        ** I had to comment out search results caching, since the autocomplete has a limit of 5 results and the sidebar must have no limits
                        searchResults = {
                          's': request.term,
                            'items': data
                      };*/
                        response(data);
                    }, "json");
              },
              minLength: 2,
              focus: function(event, ui) {
                  event.preventDefault();
              },
              select: function(event, ui) {
                    event.preventDefault();
                    if (ui.item.type=='header') {
                        return false;
                    } else if (ui.item.type=='item') {
                        clearLocalityBoundaries();
                        $('#ajax_search_value').val(ui.item.label);
                        openItemPage('item', ui.item.value, ui.item.url, 1, 'map');
                    } else {
                        goToLocation(ui.item.url, ui.item.title, ui.item.label, ui.item.type, ui.item.slug, ui.item.label);
                    }
	        }
            };
            $('#ajax_search_value')
                .autocomplete(options)
                .keypress(function(e) {
                    var s = $(this).val();
                    if(e.which == 13 && s.length>=2) {
                        $("#ajax_search_value").autocomplete( "close" );
                        if(searchResults.s==s) {
                            updateSidebarWithSearchResults(searchResults.items, s);
                        } else {
                            toggle_sidebar_loading('show');
                            var params = {
                            'action': 'get_markers',
                            'limit': -1,
                            's': s
                          };
                          $.post(ajaxurl, params, function(items) {
                              searchResults = {
                                  's': s,
                                    'items': items
                              };
                              toggle_sidebar_loading('hide');
                              updateSidebarWithSearchResults(items, s);
                            }, "json");
                        }
                    }
                })
                .data("ui-autocomplete")._renderItem = function(ul, item) {
                    /*
                    // If highlight is wanted, this regexp is a good start.
                    // Problem: accented letters!
                    var label = String(item.label).replace(
                        new RegExp(this.term, "gi"),
                        "<span class='ui-state-highlight'>$&</span>"
                    );
                    */
                    return build_search_result_item(item, 'm', true).appendTo(ul);
            };

        })(jQuery);

        // Adds map listeners:
        map.on('click', function() {
          map.closePopup();
          toggleLocalityBoundary();
        });
        map.on('zoomend', function() {
          //console.log('entered zoomend');console.log(isAutoZooming);console.log('--------');
          var zoom = map.getZoom();
          if (zoom<zoomMinimo) {
            map.setZoom(zoomMinimo);
            return;
          }
          if (!isAutoZooming) {
            toggleLocalityBoundary();
          } else {
            isAutoZooming = false;
          }
        });
        map.on('moveend', function() {
          if (limitesBrasil.contains(map.getCenter())) {
            ultimaPosicaoValida = map.getCenter();
            updateSidebar();
            return;
          }
          map.panTo(ultimaPosicaoValida);
        });

        // This listener is only used once
        var tilesLoadedListener = mapTileLayer.on('load', function() {
            adjust_elements_to_height();
            if (!initialPageLocation) {
                remove_loading(pageLocation.isOverlay);
            }
            mapTileLayer.off('load', tilesLoadedListener);
        });
    }

    // CATEGORY FILTER
		jQuery('#categoriesControl-form input').on('click', function(e){
			var checkedCategories = getCheckedCategories();
			var visibleMarkers = [];
			for(var i=0;i<markers.length;i++) {
			    if(checkedCategories.indexOf(markers[i].options.type)>-1) {
			        //markers[i].addTo(map);
			        visibleMarkers.push(markers[i]);
			    } else {
			        //markers[i].remove(map);
			    }
			}
			updateSidebar();
			markerCluster.clearLayers();
			markerCluster.addLayers(visibleMarkers);
		});
		//END FILTER

    // SELOS FILTER
		jQuery('#selosControl-form input').on('click', function(e){
			var checkedSelos = getCheckedSelos();
      var visibleMarkers = [];
      if (checkedSelos.length > 0) {
  			for(var i=0;i<markers.length;i++) {
            if(markers[i].options.selos == null) continue;
            var selos = markers[i].options.selos.split("#");
            for(var j=0;j<selos.length;j++) {

    			    if(checkedSelos.indexOf(selos[j])>-1) {
    			        visibleMarkers.push(markers[i]);
    			    }
            }
  			}
      } else {
        visibleMarkers = markers;
      }
			updateSidebar();
			markerCluster.clearLayers();
			markerCluster.addLayers(visibleMarkers);
		});
		//END FILTER

        //GET MY LOCATION
        jQuery('#my-location, #categoriesControl-myPosition').click(function(){
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    map.panTo(pos);
                    map.setZoom(16);
                    var marker = L.marker(pos, {
                        icon: L.icon({iconUrl: baseIconDir+'usuario.png'}),
			                  title: 'Sua posição'
                    }).addTo(map);
                    markers.push(marker);
                    marker.on('click', (function(pos) {
                        return function() {
                            map.panTo(pos);
                            map.setZoom(20);
                        }
                    })(pos));
                    updateSidebar();
                }, function() {
                    // Person didn't allow the browser to access the location
                });
            } else {
                // Browser doesn't support Geolocation
            }
		 });

  </script>

  <?php wp_footer(); ?>

</body></html>
