<br><br><br>
<h2>2.Informações sobre o local</h2>
<br><br>
<div class="form-item">[text titulo placeholder "Nome da feira"]</div>
<div class="form-item">[text endereco id:localizacao placeholder "Endereço Completo"]</div>
<div class="form-item form-item-wide" id="map">Mapa</div>
<div class="form-item"><label>Estado:</label> <br>[select_states]</div>
<div class="form-item"><label>Cidade:</label> <br>[select cidade id:cidade]</div>
<div class="form-item"><label>Dias de funcionamento: </label><br>[checkbox dias-funcionamento "Segunda|seg" "Terça|ter" "Quarta|qua" "Quinta|qui" "Sexta|sex" "Sábado|Sáb" "Domingo|dom"]</div>
<div class="form-item"><label>Períodos de funcionamento:</label><br>[checkbox periodo-funcionamento "Manhã|manha" "Tarde|tarde" "Noite|noite"]</div>
<div class="form-item">[text horario placeholder "Horário de funcionamento"]</div><div class="form-item"></div>
<div class="clearfix"></div>
<div class="form-item form-item-wide"><label>Quais Produtos estão disponíveis?</label><br>[checkbox_produtos]</div>
<div class="form-item form-item-wide"><br>[checkbox cafe label_first "Há venda de café da manhã para consumo no local?|s"]</div>
<div class="form-item">[text referencia placeholder "Site de referência"]</div>
<div class="form-item form-item-wide"><label>Foto</label><br>[file foto limit:1048576 filetypes:jpg|png]</div>