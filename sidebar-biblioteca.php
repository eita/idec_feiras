<?php $collections = idec_biblioteca_get_collections('name'); ?>
        <div class="locations_stats">
            <div class="locations_stats-body">
                <div id='idec-remove-filter'>
                    <span class='pure-button pure-button-error'><i class='mapafeiras_icon-fechar'></i> remover filtro <span id='idec-remove-filter-name'></span></span>
                </div>
                <ul id="locationStatsList" class="items_list">
                <?php foreach($collections as $id=>$collection) { ?>
                    <li class="with-separator">
                        <a class="ajaxifythis idec-list-item-content" href="<?= $collection->local_url ?>" data-item-id="<?= $id ?>" collection-id="<?= $id ?>" data-item-type="collection" data-page_title="<?= $collection->page_title ?>" data-item-is_overlay=0>
                            <div>
                                <div class="idec-list-item-icon"></div>
                                <h1 class="idec-list-item-title"><?= $collection->title ?></h1>
                            </div>
                        </a>
                    </li>
                <?php } ?>
                </ul>
            </div>
        </div>
